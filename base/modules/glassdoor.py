from constance import config
import lxml.html as html
import time
from selenium.common.exceptions import NoSuchElementException
from base.models import Task
from base.modules.extracktor import extract
from base.modules.captcha_processor import resolve_captcha
import logging
logger = logging.getLogger()


def csv_adapt(value):
    if value.find('\"') != -1 or value.find(',') != -1 or value.find('\n') != -1:
        return ''.join(['"', value.replace('"', '""'), '"'])
    return value


def check_login(driver, url):
    try:
        driver.find_element_by_name("username").send_keys(config.G_LOGIN)
        driver.find_element_by_css_selector(
            "span.block-signin > div.value.passWrapper > input[name=\"password\"]").send_keys(config.G_PASS)
        time.sleep(15)
        driver.find_element_by_id("signInBtn").click()
        time.sleep(10)
        driver.get(url)
    except NoSuchElementException:
        pass


def process_review(review):
    page = review

    def get_value_in_review_lxml(classname, maybe_original=False):
        val = ''
        try:
            if maybe_original:
                val_count = len(page.find_class(classname))
                if val_count > 1:
                    return page.cssselect('.' + classname + '.original')[0].text_content().encode('ascii',
                                                                                                  'ignore').strip()
            val = page.find_class(classname)[0].text_content().encode('ascii', 'ignore').strip()
        except :
            pass
        return val

    # dict key, specific element class for key, can be two values with different languages
    fields = [('summary', 'summary', False),
              ('pros', 'pros', True),
              ('cons', 'cons', True),
    ]

    if len(page.find_class("adviceMgmt")) != 0:  # for best performance. Selenium slow process NoSuchElementException
        fields.append(('advice', "adviceMgmt", True))

    res = {key: get_value_in_review_lxml(cl, original) for (key, cl, original) in fields}
    stars = None
    try:
        stars = float(page.find_class('value-title')[0].attrib['title'])
    except :
        pass
    if 'advice' not in res:
        res['advice'] = ''

    date_container = page.cssselect('.date.subtle')
    if len(date_container):
        res['time'] = date_container[0].text_content().strip() #Featured Review
    else:
        res['time'] = 'Featured Review'

    return res, stars


def glassdoor_parser(driver, page_main_url, output_file_path, pre_output_file_path, the_topics, su, file_logger, company_name=None):

    max_page = page_main_url.replace(".htm", "_P10000.htm")
    driver.get(max_page)
    try:
        max_page_num = int(driver.find_element_by_css_selector('li.page.last').text)
    except NoSuchElementException:
        max_page_num = 1
    big_blob = ''
    company_review_count = 0
    total_rating = 0

    pre_res_file = open(pre_output_file_path, 'a+b')
    for i in xrange(1, max_page_num + 1):
        page_url = page_main_url.replace(".htm", "_P%s.htm" % i)
        if not max_page_num == 1:
            driver.get(page_url)
        su.set_proc_page_status(i, max_page_num)
        html_tree = html.fromstring(driver.find_element_by_css_selector('body').get_attribute('innerHTML'))

        reviews = html_tree.find_class('hreview')
        if not len(reviews):
            # probably CAPTCHA - wait and try again
            su.set_proc_text_status('Probably CAPTCHA. Now wait to check.')
            time.sleep(20)
            try:
                if driver.find_element_by_css_selector('#recaptcha_response_field').is_displayed():
                    su.set_proc_text_status('CAPTCHA detected. Try resolve.')
                    if not resolve_captcha(driver, file_logger):
                        su.set_proc_text_status(
                            'Can\'t CAPTCHA resolve. Page may be don\'t processed')
                        file_logger.write(
                            '%s Can\'t CAPTCHA resolve. Page may be don\'t processed.' % page_url)
                        logger.warning('%s Can\'t CAPTCHA resolve. Page may be don\'t processed.' % page_url)
                    else:
                        su.set_proc_text_status('CAPTCHA resolved.')
            except:  # no captcha field
                pass

            check_login(driver, page_url)
            html_tree = html.fromstring(driver.find_element_by_css_selector('body').get_attribute('innerHTML'))
            reviews = html_tree.find_class('hreview')

        if not company_name:
            try:
                company_name = html_tree.cssselect('tt.i-emp')[0].text_content().encode('ascii', 'ignore')
            except Exception as e:
                logger.error('%s Cant\' recognize company name. page will be not processed' % page_url, exc_info=True)
                continue

        for review in reviews:
            res, stars = process_review(review)
            separator = '. '
            big_blob = separator.join([big_blob, separator.join(res.itervalues())])
            pre_res_file.write(",".join(
                [csv_adapt(company_name), page_url, res['time'], csv_adapt(res['summary']), csv_adapt(res['pros']), csv_adapt(res['cons']), csv_adapt(res['advice']), '\r\n']))
            if stars:
                total_rating += stars
                company_review_count += 1
    pre_res_file.close()

    if company_review_count:
        company_avg_rating = total_rating / company_review_count
    else:
        company_avg_rating = 0

    if len(big_blob) > 10:  # at less few words
        try:
            keywords_list = extract(big_blob, the_topics)
        except Exception as e:
            logger.error('Keywords extraction error', exc_info=True)
            raise e

        out_file = open(output_file_path, 'a+b')
        out_file.write(','.join(
            [csv_adapt(company_name), page_main_url, str(company_avg_rating), ','.join(keywords_list)]) + '\r\n')
        out_file.close()
    else:
        su.set_proc_text_status('!!! Small blob. No sense to analise.')
        logger.warning('%s !!! Small blob. No sense to analise.' % page_main_url)
        file_logger.write('%s !!! Small blob. No sense to analise.' % page_main_url)