from base.modules.extraction.alchemy_interface import Extraction_Suite_Interface
from base.modules.extraction.similarity_interface import Similarity_Interface

__author__ = 'orion'


def extract(big_blob, the_topics):
    EX = Extraction_Suite_Interface()
    SIM = Similarity_Interface()
    keywords = EX.blob2keyword_tuples(big_blob)
    product_sum = {}
    keywords_list = []
    print 's extract'
    for kk in keywords:
        term = kk['text']
        relevance = kk['relevance']
        sentiment = kk['sentiment']['type']
        if sentiment == 'neutral':
            sent_score = 0.00000001
        else:
            sent_score = kk['sentiment']['score']

        # Adjust keywords
        if sentiment == 'neutral':
            adj_relevance = 0
        else:
            adj_relevance = sent_score

        for topic in the_topics:
            sim = SIM.max_phrase_similarity(term, topic)
            if topic not in product_sum:
                product_sum[topic] = 0
            product_sum[topic] += sim * float(adj_relevance)
    for topic in the_topics:
        keywords_list.append(topic)
        keywords_list.append(str(product_sum[topic]))
    return keywords_list