__author__ = 'orion'
from base.models import Task


class StatusUpdater():
    def __init__(self, task_id):
        self.tak_id = task_id
        self.task = Task.objects.get(pk=task_id)
        self.current_link_num = None
        self.total_links_count = None
        self.current_url = None
        self.current_page_num = None
        self.total_pages_count = None


    def set_text_status(self, message):
        self.task.status = message
        self.task.save()

    def _write_proc_status(self, message=None):
        if self.current_page_num:
            self.task.status = 'Processing (%s / %s): %s Page %s/%s%s' % (
                self.current_link_num, self.total_links_count, self.current_url, self.current_page_num,
                self.total_pages_count, (' !!! '+message) if message else '')
        else:
            self.task.status = 'Processing (%s / %s): %s%s' % (
                self.current_link_num, self.total_links_count, self.current_url, (' !!! ' + message) if message else '')
        self.task.save()

    def set_proc_status(self, current, total, url):
        self.current_link_num = current
        self.total_links_count = total
        self.current_url = url
        self.current_page_num = None
        self.total_pages_count = None
        self.task.c_link = current
        self.task.c_page = None
        self._write_proc_status()

    def set_proc_page_status(self, current_page, total_pages):
        self.current_page_num = current_page
        self.total_pages_count = total_pages
        self.task.c_page = current_page
        self._write_proc_status()

    def set_proc_text_status(self, message):
        self._write_proc_status(message)