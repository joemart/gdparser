__author__ = 'orion'
import lxml.html as html
from extracktor import extract
import logging
logger = logging.getLogger()


def universal_parser(page_main_url, output_file_path, pre_output_file_path, selector, the_topics, su, file_logger,
                     company_name=None, mode='xpath'):
    if not company_name:
        company_name = 'unknown company'
    page = html.parse(page_main_url)
    big_blob = ''
    if mode == 'css':
        elems = page.getroot().cssselect(selector)
    elif mode == 'xpath':
        elems = page.getroot().xpath(selector)
    else:
        raise Exception('unknown selector type')
    pre_res_file = open(pre_output_file_path, 'a+b')
    for elem in elems:
        txt = elem.text_content().encode('ascii', 'ignore')
        big_blob += txt
        if txt.find('\"') != -1 or txt.find(',') != -1 or txt.find('\n') != -1:
            txt = ''.join(['"', txt.replace('"', '""'), '"'])
        pre_res_file.write(','.join([company_name, page_main_url, txt, '\r\n']))
    pre_res_file.close()
    if len(big_blob) > 10:
        try:
            keywords_list = extract(big_blob, the_topics)
        except Exception as e:
            logger.error('Keywords extraction error', exc_info=True)
            raise e
        out_file = open(output_file_path, 'a+b')
        out_file.write(','.join([company_name, page_main_url, ','.join(keywords_list)]) + '\r\n')
        out_file.close()
    else:
        su.set_proc_text_status('!!! Empty or small blob. No sense to analise. Probably incorrect selector.')
        file_logger.write('%s !!! Empty or small blob. No sense to analise. Probably incorrect selector.' % page_main_url)
        logger.error('%s !!! Empty or small blob. No sense to analise. Probably incorrect selector.' % page_main_url)

