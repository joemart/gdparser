import random
from PIL import Image
import time
from selenium.common.exceptions import NoSuchElementException
from deathbycaptcha import SocketClient
from constance import config
from django.conf import settings
from selenium.webdriver.common.keys import Keys
import os
import logging
logger = logging.getLogger()


def resolve_captcha(driver, file_logger=None, time_sleep=15, captcha_selector='#recaptcha_area', captcha_input_selector="#recaptcha_response_field", try_num=1):

    name = ''.join(
        [random.choice('1234567890qwertyuiopasdfghjklzxcvbnm_') for i in range(15)]) + '.png'
    full_path = os.path.join(settings.MEDIA_ROOT, name)
    driver.save_screenshot(full_path)
    img = Image.open(full_path)
    el = driver.find_element_by_css_selector(captcha_selector)
    x = el.location['x']
    y = el.location['y']
    img.crop((x, y, x+320, y+70)).save(full_path)
    output = open(full_path)
    client = SocketClient(config.CAPTCHA_LOGIN, config.CAPTCHA_PASS)
    client.is_verbose = False
    try:
        captcha = client.decode(output)
        output.close()
        # os.remove(full_path)
    except Exception, e:
        output.close()
        # os.remove(full_path)
        captcha = None
        raise e

    if captcha:
        el = driver.find_element_by_css_selector(captcha_input_selector)
        el.send_keys(captcha['text'])
        el.send_keys(Keys.RETURN)
        time.sleep(time_sleep)

        try:
            if driver.find_element_by_css_selector(captcha_input_selector).is_displayed():  # check
                if try_num < config.CAPTCHA_TRY_COUNT:
                    time.sleep(20)
                    return resolve_captcha(driver, file_logger, try_num+1)
                else:
                    logger.error('Captcha resolved incorrect %s %s ' % (full_path, captcha['text']))
                    if file_logger:
                        file_logger.write('Captcha resolved incorrect %s %s ' % (full_path, captcha['text']))
                    return False
            else:
                return True
        except NoSuchElementException:
            return True
    else:
        if try_num < config.CAPTCHA_TRY_COUNT:
                time.sleep(20)
                return resolve_captcha(driver, file_logger, try_num+1)
        else:
            logger.error('Captcha not resolved %s ' % full_path)
            if file_logger:
                file_logger.write('Captcha not resolved %s ' % full_path)
            return False
