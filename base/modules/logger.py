from base.models import Task

__author__ = 'orion'


class Logger():
    def __init__(self, task_id):
        self.tak_id = task_id
        self.task = Task.objects.get(pk=task_id)

    def write(self, message):
        f = open(self.task.log_file.path, 'a+b')
        f.write(message + '\r\n')
        f.close()