import re
import logging
from django.shortcuts import render_to_response
from djangotoolbox import fields #ListField

from collections import defaultdict
#from power.simplenlp.euro import EuroNL

#nope#  from simplenlp.euro import EuroNL
#nope#  from power.simplenlp.euro import EuroNL
#from power.simplenlp import * #ok
#nope from power.simplenlp.euro import *
#from power.simplenlp.euro import * #ok

from djangoappengine.utils import on_production_server
from django.conf import settings # Fixes "please run connect first"

#simplenlp
#https://web.archive.org/web/20130927203518/http://csc.media.mit.edu/docs/conceptnet/nl.html
from extraction.simplenlp import get_nl
en_nl=get_nl('en')




    
class Power_Interface(object):
    # Creation as clean interface to conceptnet analysis
    #0v1 JC Jan 20, 2015
    # -see d4d too
    
    def __init__(self):
        return
    
    def load_assets(self):
        oa=[]
        assets=['simplenlp','montylingua']
        for a in assets:
            oa.append("Asset: "+a)

        oa.append("Asset: pcfgpattern sentence parser with nltk and corpos (see pattern_parse)")
        return oa
    
    def raw2concepts(self,sentence):
        #Go through standard structural extractors
        
        #simplenlp.euro class EuroNL(NLTools) - uses pickle i believe
        cons=en_nl.extract_concepts(sentence,max_words=1,check_conceptnet=True)
        for c in cons:
            logging.info("BREAK: concepts "+str(c))


        return
    
    def pattern_parse(self,sentence):
        # Uses pcfg pattern, nltk and some relation extraction options
        
        #**need better import here as loads every time.
        from power.pcfg_pattern import pattern_parse
        bestprob, bestframe, bestrel, bestmatches=pattern_parse(sentence)
        return
    
    
    def sentence_similarity(self):
#        >>> from csc.nl import get_nl
#        >>> en_nl = get_nl('en')
#        
#        # This constructs a multidimentional analogyspace from conceptnet:
#        >>> from csc.conceptnet4.analogyspace import conceptnet_2d_from_db
#        >>> cnet = conceptnet_2d_from_db('en')
#        >>> analogyspace = cnet.svd(k=50)
#        <A printout of a bunch of data>
#        
#        # With this, we can represent a concept as a vector.
#        >>> vector_from_word = lambda word: analogyspace.weighted_u[word,:]
#        
#        >>> add_vectors = lambda v1,v2 : v1.weighted_add(v2,1)
#        >>> first = en_nl.extract_concepts('Mary had a little lamb.',max_words=2,check_conceptnet=True)
#        >>> print first
#        [u'mary',u'little',u'lamb']
#        # these are the concepts that conceptnet recognizes within the sentence.
#    
#        >>> second = en_nl.extract_concepts('Lions and Tigers and Bears, Oh My!',max_words=2,check_conceptnet=True)
#        >>> print second
#        [u'lion',u'tiger',u'bear',u'oh']
#    
#        # Turn every word into a vector in analogyspace,
#        # then find the sum of those vectors to get a vector for the whole sentence
#        >>> firstvector = reduce(add_vectors,map(vector_from_word,first))
#        >>> secondvector = reduce(add_vectors,map(vector_from_word,second))
#        
#        # and now we compute the similarity by taking the dot product 
#        # of the normalized vectors.
#        >>> similarity = firstvector.hat().dot(secondvector.hat()
        return
                                           
    

        
def main(request):
    #logging.info("GOT: "+str(T))
    
    P=Power_Interface()
    P.load_assets()
    
    #Ok basic
#    P.raw2concepts('The quick brown fox jumps over the lazy dog.')
    sentence="If you want to impanel a jury then you should ask questions."
    P.raw2concepts(sentence)
#works#nltk    P.pattern_parse(sentence)


    #Import d4d->divisi2->svd
    logging.info("Note:  Can't load divisi2 svd sparse matrix calcs on gae")
    #Also seems to import a conceptnet version
    from power.d4d import d4d

    
    return render_to_response('tadmin_templates/single_form_helper.html', locals())

"""

en_nl:
at euro.py
- blacklist, stopword, frequencies, swapdict, autocorrect, tokenize, untokenize, canonicalize, is_stopword
- get_windows, get_words, get_frequency...


http://csc.media.mit.edu/docs/conceptnet/nl.html
en_nl.is_stopword("the") => True


The "frequency" of an assertion declares how often it is supposedly true ie
'always'=10, usually=8 unspecified=5 never=-10.
"score" means confidence in the assertion, ie how many users beielve it to be true minus the
number of users that believe its false.
In an analogy space matrix, the "value" ie the cell refered to by a concept and a feature,
is a floating point number than combines the fequencey and score into a number generally
between -1 and 1.:w


examples from: http://csc.media.mit.edu/docs/conceptnet/nl.html?highlight=nltools#simplenlp.NLTools
en_nl.tokenize("dogs are located at houses -2.3").split(" ") # vector of strings.
en_nl.normalize("the Running dogs") => "run dog"  ie remove "the", lowercase and stem "Running", depluralize "dogs"

3rd source...csc.corpus.parse
#pcfg pattern.py

MontyNLGenerator()
    def generate_summary(self,vsoos):
    def generate_sentence(self,vsoo,sentence_type='declaration',tense='past',s_dtnum=('',1),o1_dtnum=('',1),o2_dtnum=('',1),o3_dtnum=('',1)):
    def determine_prepphrase(self,prepphrase,det='',number=1):
    def determine_nounphrase(self,nounphrase,det='',number=1):
    def determine_verb(self,verb,tense,subject_number=1,ego_p=0):
    def get_features(self,feature_string):
    def build_morph_dict(self):
    def reformulate_lifenet(self):
    def all_egocentric_declarations(self,simple_vp):
    def morph_noun(self,noun_lemma,number=1):
    def conjugate_verb(self,verb_lemma,mode):
    def get_morph_complex(self,tagged_lemma,desired_features):
    def output_morph_dict(self):
    def load_xtag_morph(self):
    def setitem(self,dict,key,value)
"""

