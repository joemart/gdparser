import os
import sys
import re
import logging
import random
import networkx as nx
import urllib2
from django.http import HttpResponse
import simplejson as json

from ether.ccm import CCM_Official_Interface as CCM_Object
from ether.ether import CCM_Factory


class NestedDict(dict):
    def __getitem__(self, key):
        if key in self: return self.get(key)
        return self.setdefault(key, NestedDict())


class API_Structure(object):
    def __init__(self):
        self.out_dict=NestedDict()
        self.endpoint=""
        return
    
    def set_endpoint(self,e):
        self.endpoint=e
        return

    def set(self,key,value):
        self.out_dict[key]=value
        return
    
    def do_post(self):
        req = urllib2.Request(self.endpoint)
        req.add_header('Content-Type', 'application/json')
        response = urllib2.urlopen(req, json.dumps([self.out_dict]),timeout=60)
        return response.read()

class Remote_Crawler_Interface(object):
    def __init__(self):
        self.endpoint="http://127.0.0.1:8000/api"
        self.active_endpoint=''
        self.alive=False
        self.API=API_Structure()
        self.API.set_endpoint(self.endpoint)
        self.API.set('action','fetch_cache')
        self.API.set('verbose','True')
        self.API.set('api_key','SecretEncryptionKey')

        return
    
    def crawl(self,url):
        api_response=""
        return api_response
    
    def find_remote_endpoint(self):
        self.endpoint_potentials=[self.endpoint]
        self.endpoint_potentials+=['http://netcloudapp.cloudapp.net:8000/api']
        self.endpoint_potentials+=['http://38.108.104.150:8000/api']
        for end in self.endpoint_potentials:
            if self.ping(end):
                self.active_endpoint=end
                self.API.set_endpoint(end)
                break
        return self.active_endpoint
    
    def ping(self,end=None):
        if not end: end=self.endpoint
        #Ping crawler
        try:
            response=urllib2.urlopen(end,timeout=30).read()
            self.alive=True
        except:
            self.alive=False
        
        if not self.alive:
            logging.error("Could not connect to crawler")
        return self.alive
    
    def analyze_url(self,url):
        #Do json post
        self.API.set('url',url)
        response=self.API.do_post()
        
        #json response to dict
        rdict = json.loads(response)[0]
        for g in rdict: #g: statistics, people, categories
            logging.info(str(g))
        return rdict


    
class CCM_Markup_Class(object):
    def __init__(self):
        return
    
    def get_topics(self):
        return ["toronto"]


def crawler_interface_main():
    #Bridge gaps
    url='http://www.huffingtonpost.com/divorced-moms/cheating-3-real-couples-w_b_6555390.html'
    
    #General activities
    Crawler=Remote_Crawler_Interface()
    Markup=CCM_Markup_Class()
    
    Crawler.ping()
    
#    api_dict=Crawler.analyze_url(url)
#    logging.info("GO: "+str(api_dict))
    
    
    #Jons' design notes
    # - ccm_assembly has assertions logic


    #Get basic CCM
    Factory=CCM_Factory()
    CCM=Factory.create_ccm()

    topics=Markup.get_topics()
    tdict=NestedDict()

    for topic in topics:
        if topic:
            url='http://www.huffingtonpost.com/divorced-moms/cheating-3-real-couples-w_b_6555390.html'
            tdict[topic]['api_response']=Crawler.analyze_url(url)
            
            CCM.store_structured('api_response','1',topic,tdict[topic]['api_response'])
            
            
            
            
    return



def crawler_main(request):
    crawler_interface_main()
    return HttpResponse("end of crawler_interface")


if __name__ == '__main__':            
    crawler_interface_main()
    

