import re
import logging
import datetime

from django import forms
from html5.forms import *
from djangotoolbox import fields
from djangotoolbox import patches
from djangotoolbox import widgets #maybe

class ControlForm(forms.Form):
    
    CHOICES=[]
    EXTRA_CHOICES = [
       ('keyone', 'keyone text'),
    ]
    CHOICES.extend(EXTRA_CHOICES)
    theoptions=forms.ChoiceField(choices=CHOICES)
    
class DirectForm2(forms.Form):
    
    def __init__(self, *args, **kwargs):
        
        self.user = kwargs.pop('user', None) # pop removes from dict, so we don't pass to the parent
        self.tindex = kwargs.pop('tindex', None)
        self.id = kwargs.pop('id',"")
        super(DirectForm2, self).__init__(*args, **kwargs) # call the parent constructor to finish __init__            
        
        DATABASE="" # to trick IDE
        mydatabase="topicdb"
        exec "from topic.models import %s as DATABASE" % mydatabase

        #Note:  On POST, must replay the form fields initial definition (so they can be cleaned)
        if self.id:
            result=DATABASE.objects.filter(id=self.id)
        else:
            self.tindex=4   # Assumes exists
            result=DATABASE.objects.filter(tindex=str(self.tindex))
            
        for entity in result:
            for field in entity._meta.fields: # for each field of database
                if re.search('(.*_list)', field.name): #Check if listfield regex: *_list
                    self_field="self.fields['"+str(field.name)+"']"
                    exec "%s = %s" % (self_field, "fields.ListField.formfield(field,initial=getattr(entity,field.name))")
                elif re.search('(.*_fk)', field.name):
                    fk_name=str(getattr(entity,str(field.name)+"_id")) #Generic to all models - works with formed line
                    prepopulate="forms.CharField(initial=\""+fk_name+"\")"
                    self_field="self.fields['FK']"
                    exec "%s = %s" % (self_field, prepopulate)
                elif re.search('(.*_pfk)', field.name):
                    True # don't output primary fk to form
#allow id field                elif field.name!="id": 
                elif re.search('(.*_model)', field.name):
                    True # don't output embedded model data to form
                else:
                    #Normal text
                    prepopulate="forms.CharField(initial=\""+str(getattr(entity,field.name))+"\")"
                    self_field="self.fields['"+str(field.name)+"']"
                    exec "%s = %s" % (self_field, prepopulate)
#                else:
#                    logging.info("DID not create form for field: "+str(field.name))

    # Clean functions are run at 'if valid' input.  First field specific, then form specific
    def clean_message(self):
        type = self.cleaned_data['type'] #cleaned_data from individual field cleaning
        num_words = len(type.split())
        if num_words < 4:
            raise forms.ValidationError("Not enough words!")
        return type
