import os
import sys
import re
import logging
import random


import networkx as nx
import urllib2
from django.http import HttpResponse
import simplejson as json

#from ccm import CCM_Official_Interface as CCM_Object
#from ether import CCM_Factory
from crawler_interface import Remote_Crawler_Interface




class Extraction_Interface(object):
    def __init__(self):
        self.hierarchy=['Remote_Crawler_Interface']
        self.initialize()
        return
    
    def initialize(self):
        self.Remote=Remote_Crawler_Interface()
        self.Remote.find_remote_endpoint()
        return

    def describe(self):
        return str(self.hierarchy)
    
    
    
#OTHERS TO ADD:
#- applied science
#- power
#- big bear
#- j4j
#- assertion extraction 
#- alchemy api




