import os
import sys
import re
import logging
import random
import urllib2
import datetime
sys.path.append('../') #relative imports hack
sys.path.append('.') #relative imports hack
#import modules.alchemyapi.alchemyapi as alchemyapi 
import alchemyapi as alchemyapi
from base.models import AlchemyApiKey

#from django.http import HttpResponse
#from django.utils import simplejson as json


class Extraction_Suite_Interface(object):
    #Object level

    def __init__(self):
        self.Al=AlchemyAPI()
        return
    
    def blob2keyword_tuples(self,blob):
        #Do alchemy lookup
        #http://www.alchemyapi.com/products/demo/alchemylanguage/
        #http://access.alchemyapi.com/calls/text/TextGetRankedKeywords
        #API DOC:  http://www.alchemyapi.com/api/keyword/textc.html
        keywords=self.Al.get_keywords(blob)
        
        #works:
        #for rel in keywords:
        #    term=rel['text']
        #    relevance=rel['relevance']
        #    sentiment=rel['sentiment']['type']
        #    sent_score=rel['sentiment']['score']
        #    print "GOT: "+term+" sent score; "+sent_score
    
        return keywords


class AlchemyAPI(object):
    # 0v1 July 4, 2014  Creation
    # - Concept to support alchemy state limit usage
    #My internal wrap for alchemyapi
    def __init__(self):
        print "alhemy init"
        try:
            key = AlchemyApiKey.objects.get(in_use=True)
        except:
            if AlchemyApiKey.objects.count():
                c_key = AlchemyApiKey.objects.all().order_by('pk')[0]
                c_key.in_use=True
                c_key.save()
                key = c_key
            else:
                raise Exception('Set Alchemy Api key before parsing')
        self.secret_key = key.value

        self.db_memory=False #ConceptDB

        if self.db_memory:
            from interfaces.osh import Concept 
            self._init_concept() #State support
        
        self.Al=alchemyapi.AlchemyAPI(self.secret_key)
        
        
        self.daily_limit=0
        self.concurrent_limit=0
        self.flag_run=False
        
        if self.db_memory:
            self._check_limits()
        return

    def disconnect(self):
        # Touch save
        # Expect to be called when done with api!
        if self.flag_run and self.db_memory:
            self.the_Concept.record.myfield=self.daily_limit
            self.the_Concept.save()
        return
    
    def _check_limits(self):
        print 'start limit check'
        #Max 5 concurrent
        #Max 1000 per day
        last_exe=str(self.the_Concept.date_executed)
        last_exe=re.sub(' .*','',last_exe)
        now=str(datetime.datetime.now())
        now=re.sub(' .*','',now)

        if last_exe!=now:
            #if self.the_Concept.days_since_touched>0:
            self.daily_limit=1000
        else:
            self.daily_limit=self.the_Concept.myfield
            if self.daily_limit=="":self.daily_limit=0
        print "limit-->",self.daily_limit
        
        logging.info("BREAK: Remaining daily limits: "+str(self.daily_limit))
            
        return
    
    def get_resources(self):
        return self.daily_limit
    
    def _init_concept(self):
        # Concept to support concept of alchemyapi (ie state)
        self.concept_id="alchemyapi_state"
        self.the_Concept=Concept(self.concept_id) #bad but works
        if not self.the_Concept.exists:
            description="Auto concept creation"
            button_name=self.concept_id
            type="api_state"
            category1="api"
            category2="concept"
            response=self.the_Concept.update_concept(concept_id=self.concept_id,type=type,category1=category1,category2=category2,button_name=button_name,description=description)
        return
    
    def get_keywords(self,blob):
        flavor='text'
        data=blob
        options={}
        options['sentiment']=1
        options['maxRetrieve']=50 #default 50
        resp =self.Al.keywords(flavor, data, options)
        if resp['status']=="OK":
            keywords=resp['keywords']
        else:
            print "BAD response: "+str(resp)
            if resp['statusInfo'] == 'unsupported-text-language':
                raise Exception(
                    'AlchemyAPI: unsupported-text-language Probably you have problems with your account. Check it.')
            print('Now try other api key')
            c_key = AlchemyApiKey.objects.get(in_use=True)
            c_pk = c_key.pk
            # find next api key
            if AlchemyApiKey.objects.filter(pk__gt=c_pk).count():
                new_key = AlchemyApiKey.objects.filter(pk__gt=c_pk)[0] #next in list with pk greater then current
            else:
                new_key = AlchemyApiKey.objects.all().order_by('pk')[0]  # go to first Api key
                if new_key.pk == c_pk:
                    raise Exception("BAD response. Looks like this api key limit expired. Add another api keys or wait a day. Info: %s"%resp['statusInfo'])
            # Now try new key
            self.Al=alchemyapi.AlchemyAPI(new_key.value)
            resp = self.Al.keywords(flavor, data, options)
            if resp['status']=="OK":  #try success
                AlchemyApiKey.objects.all().update(in_use=False)
                new_key.in_use = True
                new_key.save()
                return resp['keywords']
            else:
                raise Exception('BAD response. Looks like all api keys limit expired. Add another api keys or wait a day. Info: %s'%resp['statusInfo'])
            # keywords=[]
        return keywords

    def get_sentiment(self,blob):
        resp = self.Al.sentiment('text', blob)
        if resp['status']=="OK":
            return resp["docSentiment"]
        else:
            print "BAD response: "+str(resp)+','+str(resp['statusInfo'])

            if resp['statusInfo'] == 'unsupported-text-language':
                raise Exception(
                    'AlchemyAPI: unsupported-text-language Probably you have problems with your account. Check it.')
            elif 'not available' in resp['statusInfo']:
                return {'type': '-', 'score': 0}
            print('Now try other api key')
            c_key = AlchemyApiKey.objects.get(in_use=True)
            c_pk = c_key.pk
            # find next api key
            if AlchemyApiKey.objects.filter(pk__gt=c_pk).count():
                new_key = AlchemyApiKey.objects.filter(pk__gt=c_pk)[0] #next in list with pk greater then current
            else:
                new_key = AlchemyApiKey.objects.all().order_by('pk')[0]  # go to first Api key
                if new_key.pk == c_pk:
                    raise Exception("BAD response. Looks like this api key limit expired. Add another api keys or wait a day. Info: %s"%resp['statusInfo'])
            # Now try new key
            self.Al=alchemyapi.AlchemyAPI(new_key.value)
            resp = self.Al.sentiment('text', blob)
            if resp['status']=="OK":  #try success
                AlchemyApiKey.objects.all().update(in_use=False)
                new_key.in_use = True
                new_key.save()
                return resp['docSentiment']
            else:
                if 'not available' in resp['statusInfo']:
                    return {'type': '-', 'score': 0}
                raise Exception('BAD response. Looks like all api keys limit expired. Add another api keys or wait a day. Info: %s'%resp['statusInfo'])



    def get_entities(self,flavor,the_data):
        #flavor:  text,url,html
        self.flag_run=True
        ent=self.Al.entities(flavor,the_data)
        #D# logging.info("BREAK:  Grabbing entities from: "+the_data)
        print ('ent',ent)
        if ent['status']=="OK":
            the_entities=ent['entities']
        else:
            logging.error("Bad response from AlchemyAPI likely hit limit")
            the_entities=[]
        self.daily_limit=int(self.daily_limit)-1
        return the_entities
    

    def test(self):
        the_url="http://www.programmableweb.com/api/ny-senate"
        ent=self.Al.entities("url",the_url)
        logging.info("BIG DUMP: "+str(ent))
        #[INFO] odos.py.95]     BIG DUMP: {u'status': u'OK', u'usage': u'By accessing AlchemyAPI or using information generated by AlchemyAPI, you are agreeing to be bound by the AlchemyAPI Terms of Use: http://www.alchemyapi.com/company/terms.html', u'url': u'http://www.programmableweb.com/api/ny-senate', u'language': u'english', u'entities': [{u'relevance': u'0.855935', u'count': u'2', u'type': u'Technology', u'text': u'API'}, {u'relevance': u'0.664987', u'count': u'6', u'type': u'FieldTerminology', u'text': u'Source Code'}, {u'relevance': u'0.508498', u'count': u'4', u'type': u'Company', u'disambiguated': {u'website': u'http://www.linkedin.com/', u'yago': u'http://yago-knowledge.org/resource/LinkedIn', u'name': u'LinkedIn', u'freebase': u'http://rdf.freebase.com/ns/m.03vgrr', u'subType': [u'Website', u'VentureFundedCompany'], u'crunchbase': u'http://www.crunchbase.com/company/linkedin', u'dbpedia': u'http://dbpedia.org/resource/LinkedIn'}, u'text': u'Linkedin'}, {u'relevance': u'0.483365', u'count': u'4', u'type': u'Company', u'disambiguated': {u'website': u'http://www.facebook.com/', u'yago': u'http://yago-knowledge.org/resource/Facebook', u'name': u'Facebook', u'freebase': u'http://rdf.freebase.com/ns/m.02y1vz', u'subType': [u'Website', u'VentureFundedCompany'], u'crunchbase': u'http://www.crunchbase.com/company/facebook', u'dbpedia': u'http://dbpedia.org/resource/Facebook'}, u'text': u'Facebook'}, {u'relevance': u'0.478915', u'count': u'4', u'type': u'Company', u'disambiguated': {u'website': u'http://twitter.com/', u'name': u'Twitter', u'freebase': u'http://rdf.freebase.com/ns/m.0289n8t', u'subType': [u'Website', u'VentureFundedCompany'], u'crunchbase': u'http://www.crunchbase.com/company/twitter', u'dbpedia': u'http://dbpedia.org/resource/Twitter'}, u'text': u'Twitter'}, {u'relevance': u'0.46687', u'count': u'7', u'type': u'Company', u'text': u'ProgrammableWeb'}, {u'relevance': u'0.431326', u'count': u'5', u'type': u'Technology', u'text': u'SDK'}, {u'relevance': u'0.325685', u'count': u'2', u'type': u'JobTitle', u'text': u'Developer'}, {u'relevance': u'0.324419', u'count': u'2', u'type': u'Company', u'disambiguated': {u'website': u'http://www.google.com/', u'yago': u'http://yago-knowledge.org/resource/Google', u'name': u'Google', u'freebase': u'http://rdf.freebase.com/ns/m.045c7b', u'subType': [u'AcademicInstitution', u'AwardPresentingOrganization', u'OperatingSystemDeveloper', u'ProgrammingLanguageDeveloper', u'SoftwareDeveloper', u'VentureFundedCompany'], u'crunchbase': u'http://www.crunchbase.com/company/google', u'dbpedia': u'http://dbpedia.org/resource/Google'}, u'text': u'Google'}, {u'relevance': u'0.321693', u'count': u'2', u'type': u'FieldTerminology', u'text': u'Press Release'}, {u'relevance': u'0.305444', u'count': u'1', u'type': u'Company', u'disambiguated': {u'yago': u'http://yago-knowledge.org/resource/Reddit', u'name': u'Reddit', u'freebase': u'http://rdf.freebase.com/ns/m.0b2334', u'subType': [u'Website'], u'crunchbase': u'http://www.crunchbase.com/company/reddit', u'dbpedia': u'http://dbpedia.org/resource/Reddit'}, u'text': u'Reddit'}, {u'relevance': u'0.294632', u'count': u'2', u'type': u'Company', u'disambiguated': {u'yago': u'http://yago-knowledge.org/resource/GuideStar', u'freebase': u'http://rdf.freebase.com/ns/m.09457b', u'dbpedia': u'http://dbpedia.org/resource/GuideStar', u'name': u'GuideStar'}, u'text': u'GuideStar'}, {u'relevance': u'0.290722', u'count': u'2', u'type': u'Organization', u'disambiguated': {u'website': u'http://www.fema.gov', u'yago': u'http://yago-knowledge.org/resource/Federal_Emergency_Management_Agency', u'name': u'Federal Emergency Management Agency', u'freebase': u'http://rdf.freebase.com/ns/m.0js8z', u'opencyc': u'http://sw.opencyc.org/concept/Mx4rwTk1upwpEbGdrcN5Y29ycA', u'subType': [u'GovernmentAgency'], u'dbpedia': u'http://dbpedia.org/resource/Federal_Emergency_Management_Agency'}, u'text': u'FEMA'}, {u'relevance': u'0.286289', u'count': u'1', u'type': u'Organization', u'text': u'Primary Category Government'}, {u'relevance': u'0.272319', u'count': u'2', u'type': u'FieldTerminology', u'text': u'Terms of Service'}, {u'relevance': u'0.268808', u'count': u'1', u'type': u'Organization', u'text': u'Senate'}, {u'relevance': u'0.264011', u'count': u'1', u'type': u'FieldTerminology', u'text': u'Search Terms'}, {u'relevance': u'0.263837', u'count': u'2', u'type': u'Country', u'disambiguated': {u'website': u'http://www.usa.gov/', u'yago': u'http://yago-knowledge.org/resource/United_States', u'name': u'United States', u'freebase': u'http://rdf.freebase.com/ns/m.09c7w0', u'opencyc': u'http://sw.opencyc.org/concept/Mx4rvVikKpwpEbGdrcN5Y29ycA', u'subType': [u'Location', u'Region', u'AdministrativeDivision', u'GovernmentalJurisdiction', u'FilmEditor'], u'dbpedia': u'http://dbpedia.org/resource/United_States', u'ciaFactbook': u'http://www4.wiwiss.fu-berlin.de/factbook/resource/United_States'}, u'text': u'U.S.'}, {u'relevance': u'0.258721', u'count': u'1', u'type': u'Country', u'disambiguated': {u'website': u'http://www.visitkorea.or.kr/intro.html', u'yago': u'http://yago-knowledge.org/resource/South_Korea', u'name': u'South Korea', u'freebase': u'http://rdf.freebase.com/ns/m.06qd3', u'opencyc': u'http://sw.opencyc.org/concept/Mx4rvVib25wpEbGdrcN5Y29ycA', u'subType': [u'Location', u'GovernmentalJurisdiction'], u'dbpedia': u'http://dbpedia.org/resource/South_Korea', u'ciaFactbook': u'http://www4.wiwiss.fu-berlin.de/factbook/resource/Korea,_South'}, u'text': u'South Korea'}, {u'relevance': u'0.258009', u'count': u'1', u'type': u'Company', u'text': u'POPONG POODL'}, {u'relevance': u'0.253245', u'count': u'1', u'type': u'Organization', u'text': u'Open Government Initiative'}, {u'relevance': u'0.251708', u'count': u'1', u'type': u'Technology', u'text': u'Reddit'}, {u'relevance': u'0.248902', u'count': u'1', u'type': u'City', u'text': u'New York'}, {u'relevance': u'0.247875', u'count': u'1', u'type': u'Organization', u'text': u'PW Research Center'}, {u'relevance': u'0.247059', u'count': u'1', u'type': u'Company', u'disambiguated': {u'website': u'http://www.autodesk.com/', u'yago': u'http://yago-knowledge.org/resource/Autodesk', u'name': u'Autodesk', u'freebase': u'http://rdf.freebase.com/ns/m.018nm3', u'opencyc': u'http://sw.opencyc.org/concept/Mx4rvgU245wpEbGdrcN5Y29ycA', u'subType': [u'SoftwareDeveloper'], u'crunchbase': u'http://www.crunchbase.com/company/autodesk', u'dbpedia': u'http://dbpedia.org/resource/Autodesk'}, u'text': u'Autodesk'}, {u'relevance': u'0.246204', u'count': u'1', u'type': u'Organization', u'disambiguated': {u'website': u'http://korea.na.go.kr/', u'yago': u'http://yago-knowledge.org/resource/National_Assembly_of_South_Korea', u'name': u'National Assembly of South Korea', u'freebase': u'http://rdf.freebase.com/ns/m.02v9pw', u'subType': [u'GovernmentalBody', u'Legislature'], u'dbpedia': u'http://dbpedia.org/resource/National_Assembly_of_South_Korea', u'geo': u'37.52848 126.91744'}, u'text': u'National Assembly'}, {u'relevance': u'0.243889', u'count': u'1', u'type': u'PrintMedia', u'text': u'Daily Newsletter'}, {u'relevance': u'0.238127', u'count': u'1', u'type': u'Facility', u'text': u'USF Map'}, {u'relevance': u'0.22777', u'count': u'1', u'type': u'Country', u'text': u'Korea'}, {u'relevance': u'0.226424', u'count': u'1', u'type': u'Organization', u'text': u'United Nations Office of Humanitarian Affairs'}, {u'relevance': u'0.225262', u'count': u'1', u'type': u'PrintMedia', u'text': u'Particle Reviews'}, {u'relevance': u'0.205292', u'count': u'1', u'type': u'FieldTerminology', u'text': u'mobile applications'}, {u'relevance': u'0.20165', u'count': u'1', u'type': u'Organization', u'text': u'OCHA'}]}
        #[INFO] odos.py.98]     GOT: status
        #[INFO] odos.py.98]     GOT: usage
        #[INFO] odos.py.98]     GOT: url
        #[INFO] odos.py.98]     GOT: language
        #[INFO] odos.py.98]     GOT: entities
        the_status=ent['status'] #Ok
        for item in ent:
            logging.info("GOT: "+str(item))
        return
    
    def get_blob(self):

        return



def main():
    Al=AlchemyAPI()
    Al.test()
    Al.disconnect()
    
    return


if __name__ == '__main__':            
    main()
    


    
