import os
import sys
import re
import logging
import random
from django.conf import settings
sys.path.append('../') #relative imports hack
from peak.util.imports import lazyModule
from peak.util.imports import importString,importObject
import nltk


nltk.data.path.append(os.path.join(settings.BASE_DIR, 'base', 'modules', 'extraction', 'nlt'))
# nltk.data.path.append('/media/DATA/Programming/python/projects/scraper/base/modules/extraction/nlt')

from nltk.corpus import wordnet as wn


class Similarity_Interface(object):
    #ie/ wordnet
    def __init__(self):
        return
    
    def syn_match(self,term):
        obj=wn.synsets(term)
        syn_match=False
        for syn in obj:
            try:
                #er?# print "syn name: "+syn.name()
                syn_name=syn.name().split('.')[0]
                #Logic: for now, choose first match, but have lost sense of word
                if term==syn_name:
                    syn_match=syn
                    break
            except:
                logging.info("Using default synset:")
                syn_match=syn
        return syn_match


    def max_phrase_similarity(self,terms1,terms2): 

        terms1=terms1.lower()
        terms2=terms2.lower()

        terms1=re.sub(r'\W',' ',terms1)
        terms2=re.sub(r'\W',' ',terms2)
    
        term1_list=terms1.split(r' +')
        term2_list=terms2.split(r' +')
        
        syn1_list=[]
        syn2_list=[]
        for t in term1_list: syn1_list.append(self.syn_match(t))
        for t in term2_list: syn2_list.append(self.syn_match(t))
        
        maxx=float(0.000001)
        for ss1 in syn1_list:
            for ss2 in syn2_list:
#D#                print "For "+str(ss1)+" and "+str(ss2)
                if ss1 and ss2:
                    sim=wn.path_similarity(ss1,ss2,False,True)
                    if sim:
                        sim=float(sim)
                        if sim>maxx: maxx=sim
                    else:
                        logging.info("Bad sim type")
                else:
                    logging.info("Could not find a syn")
        return maxx
    