import re
import logging
from django.shortcuts import render_to_response
from djangotoolbox import fields #ListField

from collections import defaultdict
#from power.simplenlp.euro import EuroNL

#nope#  from simplenlp.euro import EuroNL
#nope#  from power.simplenlp.euro import EuroNL
#from power.simplenlp import * #ok
#nope from power.simplenlp.euro import *
#from power.simplenlp.euro import * #ok

from djangoappengine.utils import on_production_server
from django.conf import settings # Fixes "please run connect first"

#simplenlp
#https://web.archive.org/web/20130927203518/http://csc.media.mit.edu/docs/conceptnet/nl.html
from extraction.simplenlp import get_nl
en_nl=get_nl('en')


"""
RAW GRAB FROM SOURCES
"""
import nltk
from nltk.corpus.reader import BracketParseCorpusReader
from nltk.corpus.util import LazyCorpusLoader
import string


#####################
# JC Notes:
# - experimental parser from pcfgpattern
# - uses nltk, divisi, and bunch of other conceptnet dependencies
#####################

patterns = [
(1.0, 'HasFirstSubevent', 'the first thing you do when you {VP:1} is {VP:2}'),
(1.0, 'HasLastSubevent', 'the last thing you do when you {VP:1} is {VP:2}'),
(1.0, 'HasPrerequisite', 'something you need to do before you {VP:1} is {VP:2}'),
(1.0, 'MadeOf', '{NP:1} {BE} {ADVP:a} made of {NP:2}'),
(1.0, 'IsA', '{NP:1} {BE} a kind of {NP:2} {POST:0}'),
(1.0, 'IsA', '{NP:1} {BE} a sort of {NP:2} {POST:0}'),
(1.0, 'IsA', '{NP:1} {BE} a type of {NP:2} {POST:0}'),
(1.0, 'AtLocation', 'somewhere {NP:1} can be is {P} {NP:2}'),
(1.0, 'AtLocation', 'somewhere {NP:1} can be is {NP:2}'),
(1.0, 'AtLocation', 'you are likely to find {NP:1} {P} {NP:2}'),
(0.1, 'AtLocation', '{NP:1} can be {P} {NP:2}'),
(1.0, 'UsedFor', '{NP:1} {BE} used for {NP:2}'),
(1.0, 'UsedFor', '{NP:1} {BE} used to {VP:2}'),
(1.0, 'CapableOf', '{NP:1} {BE} capable of {NP:2}'),
(1.0, 'CapableOf', 'an activity {NP:1} can do is {VP:2}'),
(1.0, 'CapableOf', 'an activity {NP:1} can do is {NP:2}'),
(1.0, 'MotivatedByGoal', 'you would {VP:1} because you want to {VP:2}'),
(1.0, 'MotivatedByGoal', 'you would {VP:1} because you want {NP:2}'),
(1.0, 'MotivatedByGoal', 'you would {VP:1} because {S:2}'),
(1.0, 'Desires', '{NP:1} {ADVP:a} wants to {VP:2}'),
(1.0, 'Desires', '{NP:1} {ADVP:a} wants {NP:2}'),
(1.0, 'Desires', '{NP:1} {ADVP:a} want to {VP:2}'),
(1.0, 'Desires', '{NP:1} {ADVP:a} want {NP:2}'),
(1.0, 'Desires', '{NP:1} {ADVP:a} likes to {VP:2}'),
(1.0, 'Desires', '{NP:1} {ADVP:a} like to {VP:2}'),
(1.0, 'DefinedAs', '{NP:1} {BE} defined as {NP:2}'),
(0.1, 'DefinedAs', '{NP:1} {BE} the {NP:2}'),
(0.1, 'DefinedAs', '{NP:2} {BE} called {NP:1}'),
(1.0, 'DefinedAs', 'the common name for {NP:2} is {NP:1}'),
(1.0, 'SymbolOf', '{NP:1} {BE} {DT} symbol of {NP:2}'),
(1.0, 'CausesDesire', '{NP:1} would make you want to {VP:2}'),
(1.0, 'Causes', 'the effect of {XP:1} is that {S:2}'),
(1.0, 'Causes', 'the effect of {XP:1} is {NP:2}'),
(1.0, 'Causes', 'the consequence of {XP:1} is that {XP:2}'),
(1.0, 'Causes', 'something that might happen as a consequence of {XP:1} is that {XP:2}'),
(1.0, 'Causes', 'something that might happen as a consequence of {XP:1} is {XP:2}'),
(1.0, 'Causes', '{ADVP:a} {NP:1} causes you to {VP:2}'),
(1.0, 'Causes', '{ADVP:a} {NP:1} causes {NP:2}'),
(1.0, 'HasSubevent', 'one of the things you do when you {VP:1} is {XP:2}'),
(1.0, 'HasSubevent', 'something that might happen when you {VP:1} is {XP:2}'),
(1.0, 'HasSubevent', 'something that might happen while {XP:1} is {XP:2}'),
(1.0, 'HasSubevent', 'something you might do while {XP:1} is {XP:2}'),
(1.0, 'HasSubevent', 'to {VP:1} you must {VP:2}'),
(0.8, 'HasSubevent', 'when you {VP:1} you {VP:2}'),
(1.0, 'HasSubevent', 'when you {VP:1} , you {VP:2}'),
(0.5, 'HasSubevent', 'when {S:1} , {S:2}'),
(0.1, 'ReceivesAction', '{NP:1} {BE} {PASV:2}'),
(1.0, 'PartOf', '{NP:1} {BE} part of {NP:2}'),
(1.0, 'CreatedBy', 'you make {NP:1} by {NP:2}'),
(1.0, 'CreatedBy', '{NP:1} {BE} created by {NP:2}'),
(1.0, 'CreatedBy', '{NP:1} {BE} made by {NP:2}'),
(1.0, 'CreatedBy', '{NP:1} {BE} created with {NP:2}'),
(1.0, 'CreatedBy', '{NP:1} {BE} made with {NP:2}'),
(1.0, 'CapableOf', "{NP:1} ca {n't:a} {VP:2}"),
(0.01, 'CapableOf', '{NP:1} can {ADVP2:a} {VP:2}'),
(0.001, 'CapableOf', '{NP:1} {ADVP1:a} {VP:2}'),
(1.0, 'UsedFor', 'you can use {NP:1} to {VP:2}'),
(1.0, 'AtLocation', 'something you might find {P} {NP:2} is {NP:1}'),
(1.0, 'AtLocation', 'something you find {P} {NP:2} is {NP:1}'),
(0.1, 'AtLocation', 'there are {NP:1} {P} {NP:2}'),
(1.0, 'HasA', '{NP:1} {ADVP:a} {HAVE} {NP:2}'),
(1.0, 'HasPrerequisite', '{NP:1} requires {NP:2}'),
(1.0, 'HasPrerequisite', 'if you want to {VP:1} then you should {VP:2}'),
(1.0, 'HasPrerequisite', '{VP:1} requires that you {VP:2}'),
(0.002, 'IsA', '{NP:1} {BE} {ADVP:a} {NP:2} {POST:0}'),
(0.02, 'IsA', 'to {VP:1} {BE} to {VP:2}'),
(0.3, 'HasProperty', '{NP:1} {BE} {ADVP:a} {AP:2}'),
(1.0, 'UsedFor', 'people use {NP:1} to {VP:2}'),
(1.0, 'UsedFor', '{NP:1} is for {XP:2}'),
(1.0, 'UsedFor', '{VP:1} is for {XP:2}'),
(1.0, 'junk', 'picture description : {XP:1}'),
(0.001, 'junk', '{NP:1}'),
(1.0, 'junk', 'things that are often found together are : {NP:1}'),
(1.0, 'junk', 'When you {VP:1} you do the following : 1'),
]

p1="""
NP -> N1 [0.1] | DT N1 [0.1] | AP N1 [0.1] | DT AP N1 [0.1]
NP -> Npr [0.1] | PRP [0.05] | WP [0.05] | NP PP [0.05] | NP join NP [0.05]
NP -> VG [0.05] | VG NP [0.05] | VG ADV [0.05] | VG NP P [0.04] | VG NP VP [0.01]
NP -> QUOT NP QUOT [0.05] | NP POS NP [0.05]
N1 -> NN [0.3] | NNS [0.3] | NN N1 [0.3] | NNS N1 [0.1]
Npr -> NNP [0.5] | NNP Npr [0.5]
join -> ',' [0.4] | 'and' [0.4] | 'or' [0.2]
AP -> JJ [0.1] | VBN [0.1] | PRPp [0.1] | JJR [0.1] | JJS [0.1] | CD [0.1]
AP -> AP join AP [0.1] | AP AP [0.2]
AP -> JJ PP [0.1]
P  -> IN [0.5] | TO [0.5]
PP -> P NP [0.5] | TO VP [0.5]
V  -> VB [0.3] | VBZ [0.3] | VBP [0.3] | VB V [0.05] | V RP [0.05]
VG -> VBG [0.8] | VB VBG [0.1] | VBG RP [0.1]
VP -> V [0.2] | V NP [0.15] | V PP [0.15] | V NP PP [0.1]
VP -> STATEVB NP [0.1] | STATEVB AP [0.1] | VP ADV [0.1]
VP -> ADVP V [0.02] | ADVP V NP [0.02] | ADVP V PP [0.02] | ADVP V NP PP [0.02]
VP -> ADVP STATEVB NP [0.01] | ADVP STATEVB AP [0.01]
STATEVB -> BE [0.5] | CHANGE [0.5]
POST -> [0.9] | VBN PP [0.03] | WDT VP [0.04] | WDT S [0.03]
S -> NP VP [1.0]
XP -> NP [0.4] | VP [0.3] | S [0.3]
PASV -> VBN [0.4] | PASV PP [0.3] | PASV ADV [0.3]
BEWORD -> 'be' [0.1] | 'is' [0.15] | 'are' [0.15] | 'was' [0.1] | 'being' [0.1]
BEWORD -> 'were' [0.1] | 'been' [0.1] | "'re" [0.1] | "'m" [0.1]
BE -> BEWORD [0.8] | MD BEWORD [0.1] | MD RB BEWORD [0.1]
MD -> 'can' [1.0]
HAVE -> 'has' [0.25] | 'have' [0.25] | 'contain' [0.25] | 'contains' [0.25]
DO -> 'do' [0.4] | 'does' [0.3] | 'did' [0.3]
CHANGE -> 'get' [0.25] | 'gets' [0.25] | 'become' [0.25] | 'becomes' [0.25]
ADV -> RB [0.5] | RP [0.3] | RBR [0.2]
ADVP -> [0.9] | RB [0.025] | RB ADVP [0.025] | MD ADVP [0.025] | DO ADVP [0.025]
ADVP1 -> RB [0.25] | RB ADVP [0.25] | MD ADVP [0.25] | DO ADVP [0.25]
ADVP2 -> [0.9] | RB [0.075] | RB ADVP2 [0.025]
"""

#thegrammar = nltk.data.load(p1,format='pcfg')
#thegrammar = nltk.data.load('file:/static/data/patterns.pcfg',format='pcfg')
#thegrammar = nltk.data.load('url:/static/data/patterns.pcfg',format='pcfg')


import os

#JC# treebank_brown = LazyCorpusLoader('treebank/combined', BracketParseCorpusReader, r'c.*\.mrg')
#JC# treebank_brown=None


def defaultunigram(smoothing):
    x = defaultdict(int)
    x['NN'] = smoothing * 0.9
    x['VB'] = smoothing * 0.1
    return x

def get_lexicon(filename='LEXICON.BROWN.AND.WSJ'):
    
    path=os.path.join(os.path.split(__file__)[0], '..\\static\\data\\corpora\\'+filename)
    filename=path
    f = open(filename)
    for line in f:
        parts = line.strip().split()
        if not parts: continue
        word = parts[0].lower()
        for tag in parts[1:]:
            # Pretend adverbs are a closed class. Otherwise lots of things
            # can inadvertently be adverbs.
            yield word, tag

class UnigramProbDist(object):
    #Sourced pcfgpattern.py
    def __init__(self, smoothing=0.01):
        self.counts = defaultdict(int)
        self.probs = defaultdict(lambda: defaultunigram(smoothing))
        self.smoothing = smoothing
        self.total = 0.0
    def inc(self, word, tag, closed_class=True):
        if word not in self.counts: self.total += self.smoothing
        if closed_class and tag in ['RB', 'MD', 'DO']: return
        self.probs[word][tag] += 1
        self.counts[word] += 1
        self.total += 1
    def probabilities(self, word):
        #count = float(self.counts[word]) + self.smoothing
        count = self.total
        if word != "'s":
            for tag, n in self.probs[word].items():
                yield tag, n/count
        else:
            yield "POS", 5000.0/count
    
    @classmethod
    def from_treebank(klass):
        from nltk.corpus import brown, treebank
        probdist = klass()
        logging.warning("JC removed treebank for now from here")
        for sent in treebank.tagged_sents():
            for word, tag in sent:
                probdist.inc(word.lower(), tag)
#JC#         for sent in treebank_brown.tagged_sents():
#JC#             for word, tag in sent:
#JC#                 probdist.inc(word.lower(), tag)
        for word, tag in get_lexicon():
            probdist.inc(word, tag, closed_class=False)
        for i in range(10): probdist.inc('can', 'VB')
        return probdist
    
    
        
thegrammar = nltk.data.load(p1,format='jc_direct')
theunigrams = UnigramProbDist.from_treebank()

def match_production(rhs, start, chart, tokens=None):
    if len(rhs) == 0:
        yield start, 1.0
        return
    symb = str(rhs[0])
    group = None
    if symb[0] == "{":
        parts = symb[1:-1].split(':')
        symb = parts[0]
        if len(parts) > 1: group = parts[1]
    for next, prob in chart[symb][start].items():
        for end, prob2 in match_production(rhs[1:], next, chart):
            yield end, prob*prob2

def match_pattern(rhs, start, chart, tokens):
    if len(rhs) == 0:
        yield start, 1.0, [], {}
        return
    symb = str(rhs[0])
    group = None
    if symb[0] == "{":
        parts = symb[1:-1].split(':')
        symb = parts[0]
        if len(parts) > 1: group = parts[1]
    for next, prob in chart[symb][start].items():
        for end, prob2, frame, matchdict in match_pattern(rhs[1:], next, chart, tokens):
            if group is not None:
                if group in string.digits:    
                    chunk = ["{%s}" % group]
                    groupn = int(group)
                    if groupn == 0: chunk = []
                else:
                    chunk = tokens[start:next]
                    groupn = group
                matchdict[groupn] = en_nl.untokenize(' '.join(tokens[start:next]))
            else: chunk = tokens[start:next]
            yield end, prob*prob2, chunk + frame, matchdict
            
            
def pattern_chart(tokens, grammar, unigrams, trace=0):
    # chart :: symbol -> start -> end -> prob
    chart = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
    for i, token in enumerate(tokens):
        token = token.lower()
        chart[token][i][i+1] = 1.0
        if trace > 0:
            print "%s\t%s\t%s" % (token, (i, i+1), 1.0)
        for tag, prob in unigrams.probabilities(token):
            if tag == "''" or tag == "``": tag = "QUOT"
            if tag == 'PRP$': tag = "PRPp"
            chart[tag][i][i+1] = prob
            if trace > 0:
                print "%s\t%s\t%s" % (tag, (i, i+1), prob)
    
    changed = True
    while changed:
        changed = False
        for prod in grammar.productions():
            lhs = str(prod.lhs())
            for start in range(len(tokens)+1):
                for end, prob in match_production(prod.rhs(), start, chart):
                    p = prob * prod.prob()
                    if p > 1e-55 and p > chart[lhs][start][end]:
                        changed = True
                        chart[lhs][start][end] = p
                        if trace > 0:
                            print "%s\t%s\t%s" % (lhs, (start, end), p)
    return chart

    
def pattern_parse(sentence, trace=0):
    tokens=en_nl.tokenize(sentence.split(". ")[0].strip("?!.")).split()
    chart = pattern_chart(tokens, thegrammar, theunigrams, trace)
    bestprob = 1e-60
    bestframe = None
    bestrel = None
    bestmatches = None
    for pprob, rel, pattern in patterns:
        ptok = pattern.split()
        for end, prob, frame, matchdict in match_pattern(ptok, 0, chart, tokens):
            prob *= pprob
            if end == len(tokens):
                if trace > 0:
                    print prob, pattern
                if prob > bestprob:
                    bestprob = prob
                    bestframe = en_nl.untokenize(' '.join(frame))
                    bestrel = rel
                    bestmatches = matchdict
    logging.info("BEST: "+str(bestprob)+","+str(bestframe)+","+str(bestrel)+","+str(bestmatches))

    return bestprob, bestframe, bestrel, bestmatches
        

tests = [
    ("If you want to impanel a jury then you should ask questions.",
     "HasPrerequisite(impanel a jury, ask questions)"),
    ('"Lucy in the Sky with Diamonds" was a famous Beatles song',
     'IsA("Lucy in the Sky with Diamonds", a famous Beatles song)'),
    ("sound can be recorded",
     "ReceivesAction(sound, recorded)"),
    ("sounds can be soothing",
     "HasProperty(sounds, soothing)"),
    ("music can be recorded with a recording device",
     "ReceivesAction(music, recorded with a recording device)"),
    ("The first thing you do when you buy a shirt is try it on",
     "HasFirstSubevent(buy a shirt, try it on)"),
    ("One of the things you do when you water a plant is pour",
     "HasSubevent(water a plant, pour)"),
    ("A small sister can bug an older brother",
     "CapableOf(A small sister, bug an older brother)"),
    ("McDonald's hamburgers contain mayonnaise",
     "HasA(McDonald's hamburgers, mayonnaise)"),
    ("If you want to stab to death then you should get a knife.",
     "HasPrerequisite(stab to death, get a knife)"),
    ("carbon can cake hard",
     "CapableOf(carbon, cake hard)"),
    ("You would take a walk because your housemates were having sex in your bed.",
     "MotivatedByGoal(take a walk, your housemates were having sex in your bed)"),
    ("police can tail a suspect",
     "CapableOf(police, tail a suspect)"),
    ("people can race horses",
     "CapableOf(people, race horses)"),
    ("computer can mine data",
     "CapableOf(computer, mine data)"),
    ("to use a phone you must dial numbers",
     "HasSubevent(use a phone, dial numbers)"),
    ("People who are depressed are more likely to kill themselves",
     "HasProperty(People who are depressed, more likely to kill themselves)"),
    ("Bird eggs are good with toast and jam",
     "HasProperty(Bird eggs, good with toast and jam)"),
    ("housewife can can fruit",
     "CapableOf(housewife, can fruit)"),
    ("pictures can be showing nudity",
     "CapableOf(pictures, be showing nudity)"),
    ("a large house where the president of the US resides",
     "junk(a large house where the president of the US resides, None)"),
    ("girls are cute when they eat",
     "HasProperty(girls, cute when they eat)"),
    ("When books are on a bookshelf, you see only their spines.",
     "HasSubevent(books are on a bookshelf, you see only their spines)"),
    ("The effect of taking a phone call is finding out who is calling",
     "Causes(taking a phone call, finding out who is calling)"),
    ("There are 60 seconds in a minute",
     "AtLocation(60 seconds, a minute)"),
    ("Two wrongs don't make a right.",
     "CapableOf(Two wrongs, make a right)"),
    ("Somewhere someone can be is an art gallery",
     "AtLocation(someone, an art gallery)"),
    ("A person doesn't want war",
     "Desires(A person, war)"),
    ("That's weird",
     "junk(That's weird, None)"),
]

def textrepr(rel, matchdict):
    if rel is None: return 'None'
    return "%s(%s, %s)" % (rel, matchdict.get(1), matchdict.get(2))

def run_tests():
    success = 0
    ntests = 0
    for testin, testout in tests:
        ntests += 1
        prob, frame, rel, matches = pattern_parse(testin)
        if textrepr(rel, matches) == testout:
            success += 1
            logging.info( "Success: "+ str(testin))
        else:
            logging.info( "Failed: "+ str(testin))
            logging.info( "Got: "+ str(textrepr(rel, matches)))
            logging.info( "Expected: "+ str(testout))
            pattern_parse(testin, 1)
            
    logging.info( "Tests complete: "+str(success)+" of "+str(ntests))

