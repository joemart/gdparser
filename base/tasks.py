from celery import task
from base.modules.glassdoor import glassdoor_parser
from base.modules.universal import universal_parser
from base.models import ParseSite, Task
from base.modules.status_updater import StatusUpdater
from base.modules.captcha_processor import resolve_captcha
from base.modules.logger import Logger
import logging
logger = logging.getLogger()
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from selenium.common.exceptions import NoSuchElementException
import time
from constance import config
from django.conf import settings
if settings.USE_VIRTUAL_DISPLAY:  # use virtual display only on production
    try:
        from pyvirtualdisplay import Display
    except:
        pass


@task
def parse_router(url_companies_list, topics_list, output_file_path, pre_output_file_path, task_id):
    need_driver = False
    logger.warning('Start task: %s links' % len(url_companies_list))
    time.sleep(2)  # for database sync
    su = StatusUpdater(task_id)
    file_logger = Logger(task_id)
    if len([value for value in url_companies_list if value[0].find('www.glassdoor.co.uk') != -1]):  # need driver
        su.set_text_status('Starting webdriver and try to login')
        need_driver = True
        if settings.USE_VIRTUAL_DISPLAY:
            display = Display(visible=0, size=(800, 600))
            display.start()
            su.set_text_status('Display driver started')
        su.set_text_status('Starting Firefox')
        try:
            log_file = open(settings.FIREFOX_LOG, 'a+b')
        except:
            log_file=None
        binary = FirefoxBinary(log_file=log_file)
        driver = webdriver.Firefox(firefox_binary=binary)
        su.set_text_status('Firefox started')
        driver.implicitly_wait(10)
        base_url = "https://www.glassdoor.co.uk"
        su.set_text_status('Login page Loaded. Now try Sign in.')
        driver.get(base_url + "/profile/login_input.htm")
        time.sleep(20)  # big pause for "A" captcha antidetector
        try:
            if driver.find_element_by_css_selector('#recaptcha_response_field').is_displayed():
                su.set_text_status('CAPTCHA detected. Try resolve.')
                if not resolve_captcha(driver, file_logger):
                    su.set_text_status('Can\'t CAPTCHA resolve. Login canceled.')
                    logger.error('Can\'t CAPTCHA resolve. Login canceled.')
                    raise Exception('Can\'t CAPTCHA resolve. Login canceled.')

        except NoSuchElementException:
            pass
        driver.find_element_by_name("username").send_keys(config.G_LOGIN)
        driver.find_element_by_css_selector(
            "span.block-signin > div.value.passWrapper > input[name=\"password\"]").send_keys(config.G_PASS)
        time.sleep(15)
        driver.find_element_by_id("signInBtn").click()
        time.sleep(10)
        try:
            driver.find_element_by_css_selector('#AccountMenu')
        except NoSuchElementException:
            su.set_text_status('Failed sign in. Check Login & Password')
            driver.quit()
            try:
                display.stop()  # ignore any output from this.
            except:
                pass
            logger.error('Failed sign in. Check Login & Password', exc_info=True)
            raise Exception('Failed sign in. Check Login & Password')

        su.set_text_status('Successfully signed in.')

    for i, url_and_company in enumerate(url_companies_list):
        company_name = None
        url = url_and_company[0]
        su.set_proc_status(i+1, len(url_companies_list), url)
        if len(url_and_company) > 1:
            company_name = url_and_company[1]
        if url.startswith('https://www.glassdoor.co.uk') or url.startswith('http://www.glassdoor.co.uk'):
            try:
                glassdoor_parser(driver, url, output_file_path, pre_output_file_path, topics_list, su, file_logger, company_name)
            except Exception as e:
                su.set_proc_text_status('Error. Page canceled. See Sentry for Details')
                file_logger.write('%s page return Exception %s' % (url, e.message))
                logger.warning('%s page return Exception %s' % (url, e.message), exc_info=True)

        else:
            domain = url.replace('http://', '').replace('https://', '')
            domain = domain[:domain.find('/') if domain.find('/') != -1 else None]
            site = ParseSite.objects.get(url=domain)
            try:
                universal_parser(url, output_file_path, pre_output_file_path, site.selector, topics_list, su, file_logger,
                                 company_name=company_name, mode=site.mode)
            except Exception as e:
                su.set_proc_text_status('Error. Task canceled. See Sentry for Details')
                file_logger.write('%s page return Exception %s' % (url, e.message))
                logger.warning('%s page return Exception %s' % (url, e.message), exc_info=True)

    if need_driver:
        driver.quit()
    if not settings.DEBUG:
        try:
            display.stop()  # ignore any output from this.
        except:
            pass

    Task.objects.filter(pk=task_id).update(status='complete')