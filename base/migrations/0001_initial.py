# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AlchemyApiKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('in_use', models.BooleanField(default=False, editable=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ParseSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('url', models.CharField(help_text=b'example.com', max_length=255)),
                ('selector', models.CharField(max_length=255)),
                ('mode', models.CharField(default=b'xpath', max_length=15, choices=[(b'css', b'css'), (b'xpath', b'xpath')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('url_list', models.FileField(upload_to=b'url_lists', verbose_name=b'urls list')),
                ('keyword_list', models.FileField(upload_to=b'keyword_lists', verbose_name=b'topics list')),
                ('status', models.CharField(default=b'waiting', max_length=255)),
                ('c_link', models.IntegerField(null=True, editable=False, blank=True)),
                ('c_page', models.IntegerField(null=True, editable=False, blank=True)),
                ('pre_result', models.FileField(help_text=b"Don't edit manually.                                   Parsed fields will be here when status become 'complete'.", null=True, upload_to=b'results', blank=True)),
                ('log_file', models.FileField(help_text=b"Don't edit manually.                                   Logs will be here when status become 'complete'.", null=True, upload_to=b'logs', blank=True)),
                ('result', models.FileField(help_text=b"Don't edit manually. Result will be here when status become 'complete'.", null=True, upload_to=b'results', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
