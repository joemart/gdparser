from django.contrib import admin
from models import Task, ParseSite, AlchemyApiKey
from django import forms


class TaskAdmin(admin.ModelAdmin):
    readonly_fields = ('status',)
    list_display = ('name', 'status')

# Register your models here.


admin.site.register(Task, TaskAdmin)
admin.site.register(ParseSite)
admin.site.register(AlchemyApiKey)
