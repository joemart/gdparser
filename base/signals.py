__author__ = 'orion'
from django.db.models.signals import pre_save, post_save, pre_delete, post_delete
from django.dispatch import receiver
from models import Task
from modules.glassdoor import glassdoor_parser
from tasks import parse_router
import random
import os
from django.conf import settings
from django.core.files.base import ContentFile


def generate_name(instance):
    filename = "".join([c for c in instance.name.strip().replace(' ', '_') if c.isalpha() or c.isdigit() or c == '_'])

    output_file_path = filename + '_' + ''.join(
        [random.choice('1234567890qwertyuiopasdfghjklzxcvbnm_') for i in range(15)]) + '.csv'
    return output_file_path


@receiver(post_save, sender=Task)
def start_task(sender, instance, created, **kwargs):
    if created:
        new_name = generate_name(instance)
        instance.result.save(new_name, ContentFile(''))
        instance.pre_result.save(new_name.replace('.csv', '_pre_values.csv'), ContentFile(''))
        instance.log_file.save(new_name.replace('.csv', '.log'), ContentFile(''))
        instance.save()
        url_list_file = instance.url_list
        url_list_file.open()
        topics_file = instance.keyword_list
        topics_file.open()
        topics_list = []
        url_companies_list = []
        for line in topics_file.read().splitlines():
            topics_list.append(line)
        for line in url_list_file.read().splitlines():
            url_companies_list.append(line.split(','))
        url_list_file.close()
        topics_file.close()
        parse_router.delay(url_companies_list, topics_list, instance.result.path, instance.pre_result.path, instance.pk)
    return 0


@receiver(pre_delete, sender=Task)
def delete_task(sender, instance, **kwargs):
    instance.keyword_list.delete()
    instance.url_list.delete()
    instance.result.delete()
    instance.log_file.delete()