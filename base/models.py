import os
from django.db import models
# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=255)
    url_list = models.FileField(upload_to='url_lists', verbose_name='urls list')
    keyword_list = models.FileField(upload_to='keyword_lists', verbose_name='topics list')

    status = models.CharField(max_length=255, default='waiting')
    c_link = models.IntegerField(blank=True, null=True, editable=False)
    c_page = models.IntegerField(blank=True, null=True, editable=False)

    pre_result = models.FileField(null=True, blank=True, upload_to='results',
                                  help_text='Don\'t edit manually.\
                                   Parsed fields will be here when status become \'complete\'.')
    log_file = models.FileField(null=True, blank=True, upload_to='logs',
                                help_text='Don\'t edit manually.\
                                   Logs will be here when status become \'complete\'.')
    result = models.FileField(null=True, blank=True, upload_to='results',
                              help_text='Don\'t edit manually. Result will be here when status become \'complete\'.')

    def __unicode__(self):
        return self.name


class ParseSite(models.Model):
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=255, help_text='example.com')
    selector = models.CharField(max_length=255)
    mode = models.CharField(max_length=15, choices=(('css', 'css'), ('xpath', 'xpath')), default='xpath')

    def __unicode__(self):
        return self.name


class AlchemyApiKey(models.Model):
    value = models.CharField(max_length=255)
    in_use = models.BooleanField(default=False, editable=False)

    def __unicode__(self):
        return self.value