$(function () {
    var options = {
        chart: {
            plotBorderWidth: 1,
            plotBorderColor: '#243C8C',
            zoomType: 'xy',
            resetZoomButton:{
                x:0,
                y: -30
            },
            style: {
                fontFamily: 'Open Sans',
                fontSize: '14px'
            }            
        },

        legend: {
            enabled: false
        },
        title : {
            text: ''
        },
        xAxis: {
            gridLineColor: '#E8EAF6',
            gridLineWidth: 1,
            tickColor: '#243C8C',
            lineColor: '#243C8C',
            labels:{
                style:{
                    fontSize: '16px',
                    color: '#292929'
                }
            },
            title: {
                style: {
                    fontSize: '16px',
                    color: '#292929'
                }
            }                         
        },
        yAxis: {
            gridLineColor: '#E8EAF6',
            tickColor: '#243C8C',
            startOnTick: false,
            endOnTick: false,
            labels:{
                style:{
                    fontSize: '16px',
                    color: '#292929'
                }
            },
            title: {
                style: {
                    fontSize: '16px',
                    color: '#292929'
                }
            }        
        }
    };

    Highcharts.setOptions(options);

    $('.buble-compare').highcharts({

        chart: {
            type: 'bubble',
        },

        xAxis: {
            title: {
                text: 'Daily fat intake'
            },
            labels: {
                format: '{value} gr'
            },
            plotLines: [{
                color: 'black',
                dashStyle: 'dot',
                width: 2,
                value: 65,
                label: {
                    align: 'middle',
                    rotation: 0,
                    y: 15,
                    style: {
                        fontStyle: 'italic'
                    },
                    text: 'Safe fat intake 65g/day'
                },
                zIndex: 3
            }]
        },

        yAxis: {
            title: {
                text: 'Daily sugar intake'
            },
            labels: {
                format: '{value} gr'
            },
            maxPadding: 0.2,
            plotLines: [{
                color: 'black',
                dashStyle: 'dot',
                width: 2,
                value: 50,
                label: {
                    align: 'right',
                    style: {
                        fontStyle: 'italic'
                    },
                    text: 'Safe sugar intake 50g/day',
                    x: -10
                },
                zIndex: 3
            }]
        },

        tooltip: {
            useHTML: true,
            headerFormat: '<table>',
            pointFormat: '<tr><th colspan="2"><h3>{point.country}</h3></th></tr>' +
                '<tr><th>Fat intake:</th><td>{point.x}g</td></tr>' +
                '<tr><th>Sugar intake:</th><td>{point.y}g</td></tr>' +
                '<tr><th>Obesity (adults):</th><td>{point.z}%</td></tr>',
            footerFormat: '</table>',
            followPointer: true
        },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },

        series: [{
            data: [
                {x: 95, y: 95, z: 13.8, name: 'BE', country: 'Belgium'},
                {x: 86.5, y: 102.9, z: 14.7, name: 'DE', country: 'Germany'},
                {x: 80.8, y: 91.5, z: 15.8, name: 'FI', country: 'Finland'},
                {x: 80.4, y: 102.5, z: 12, name: 'NL', country: 'Netherlands'},
                {x: 80.3, y: 86.1, z: 11.8, name: 'SE', country: 'Sweden'},
                {x: 78.4, y: 70.1, z: 16.6, name: 'ES', country: 'Spain'},
                {x: 74.2, y: 68.5, z: 14.5, name: 'FR', country: 'France'},
                {x: 73.5, y: 83.1, z: 10, name: 'NO', country: 'Norway'},
                {x: 71, y: 93.2, z: 24.7, name: 'UK', country: 'United Kingdom'},
                {x: 69.2, y: 57.6, z: 10.4, name: 'IT', country: 'Italy'},
                {x: 68.6, y: 20, z: 16, name: 'RU', country: 'Russia'},
                {x: 65.5, y: 126.4, z: 35.3, name: 'US', country: 'United States'},
                {x: 65.4, y: 50.8, z: 28.5, name: 'HU', country: 'Hungary'},
                {x: 63.4, y: 51.8, z: 15.4, name: 'PT', country: 'Portugal'},
                {x: 64, y: 82.9, z: 31.3, name: 'NZ', country: 'New Zealand'}
            ]
        }]
    });

$('.buble-analysis').highcharts({

        chart: {
            type: 'bubble'
        },
        legend: {
            enabled: false
        },
        series: [{
            data: [
                [9, 81, 63],
                [98, 5, 89],
                [51, 50, 73],
                [41, 22, 14],
                [58, 24, 20],
                [78, 37, 34],
                [55, 56, 53],
                [18, 45, 70],
                [42, 44, 28],
                [3, 52, 59],
                [31, 18, 97],
                [79, 91, 63],
                [93, 23, 23],
                [44, 83, 22]
            ],
            marker: {
                fillColor: {
                    radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                    stops: [
                        [0, 'rgba(255,255,255,0.5)'],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.5).get('rgba')]
                    ]
                }
            }
        }, {
            data: [
                [42, 38, 20],
                [6, 18, 1],
                [1, 93, 55],
                [57, 2, 90],
                [80, 76, 22],
                [11, 74, 96],
                [88, 56, 10],
                [30, 47, 49],
                [57, 62, 98],
                [4, 16, 16],
                [46, 10, 11],
                [22, 87, 89],
                [57, 91, 82],
                [45, 15, 98]
            ],
            marker: {
                fillColor: {
                    radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                    stops: [
                        [0, 'rgba(255,255,255,0.5)'],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.5).get('rgba')]
                    ]
                }
            }
        }]

    });
    
    $('.column-rank').each(function() {
        var chart = new Highcharts.Chart({
            chart: {
                type: 'bar',
                renderTo : this,
                
                marginBottom: 0,
                plotBorderWidth: 0,
                borderWidth: 0
            },
            colors: ['#2bc7dd','#2a80b9'],
            xAxis: {
                categories: ['Company Name','Company Name','Company Name','Company Name','Company Name',
                                'Company Name','Company Name','Company Name'
                            ],
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                gridLineColor: 'transparent',
                minorTickLength: 0,
                labels: {
                    style: {
                        color: '#292929',
                        fontSize:'18px',
                        fontFamily: 'Open Sans'
                    }
                },
                tickLength: 0            
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    enabled: false   
                },
                title: {
                    text: null  
                },            
                gridLineWidth: 0,
                minorGridLineWidth: 0     
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                bar:{
                    maxPointWidth: 100,
                    minPointWidth:20,
                    borderWidth: 0
                },
                series: {
                    stacking: 'percent',

                }
            },
            series: [{
                name: 'remain',
                data: [20,10,40,23,50, 30, 30, 30]
            }, {
                name: 'achieved',
                data: [80,90,60,77,50,70,70,70]
            }],

            title:{text: null}
        
        });
    });
    
    $('.column-patent-sector').each(function() {
        var chart = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: this
            },
            legend: {
                enabled: true
            },
            xAxis: {
                gridLineWidth: 0,
                categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                shared: true
            },
            plotOptions: {
                column: {
                    stacking: 'percent'
                }
            },
            series: [{
                name: 'John',
                data: [5, 3, 4, 7, 2]
            }, {
                name: 'Jane',
                data: [2, 2, 3, 2, 1]
            }, {
                name: 'Joe',
                data: [3, 4, 4, 2, 5]
            }]

        });
    });
    
    $('.line-return-sector').highcharts({
        chart: {
            type: 'spline'
        },
        plotOptions: {
            series: {
                color: '#3562d5',
                lineWidth: 2
            }
        },        
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']               
        },
        yAxis: {
            title: {
                text: ''
            }             
        },
        tooltip: {
            valueSuffix: ''
        },

        series: [{
            name: 'Tokyo',
            data: [20, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }]
    });

    $('.line-investment-sector').highcharts({
        chart: {
            type: 'line'
        },
        xAxis: {
            gridLineWidth: 0,
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
            }
        },        
        series: [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: 'London',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
    });
    $('.line-investment-sector-2').highcharts({

        xAxis: {
            tickInterval: 1
        },

        yAxis: {
            type: 'logarithmic',
            minorTickInterval: 0.1
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br />',
            pointFormat: 'x = {point.x}, y = {point.y}'
        },

        series: [{
            data: [1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
            pointStart: 1
        }]
    });

    $('.line-company-analysis').highcharts({

    chart: {
        
        "borderColor": "#ffffff",
        "zoomType": "xy"
    },
    yAxis: {
        "labels": {
            "format": "{value}%"
        },
        "title": {
            "text": "Return on Investment  (earnings relative to value of R&D Asset)"
        }
    },
    xAxis: {
        gridLineWidth: 0,
        title: {
            "text":"Investment (value of capitalised R&D relative to sales)"
        },
        labels: {
            format: "{value}%"
        }
    },
    title: {
        text: null
    },
    legend: {
        enabled: true
    },
    tooltip: {
        enabled: false
    },
    plotOptions: {
        series: {
            // dataLabels: {
            //     enabled: true,
            //     format: "{key}",
            //     borderColor: '#f4a360',
            //     borderWidth:1,
            //     backgroundColor: '#fdd0af',
            //     borderRadius: 50,
            // },
            color: '#abf39d'
        },
        bubble: {
            minSize: 30,
            maxSize: 30
        }
    },
    series: [
        {   type: 'spline',
            index: 0,
            name: "Industrial",
            data: [
                {  
                    x:4,
                    y:40
                },
                {   
                   
                    x:5,
                    y:30
                },
                {  
                    x:4.9,
                    y:190
                },
                {  
                    x:5,
                    y:120
                },
                {  
                    x:4.8,
                    y:75
                },
                { 
                    x:5,
                    y:30
                },
                { 
                    x:14.7,
                    y:230
                },
                { 
                    x:6.6,
                    y:220
                },
                { 
                    x:9.3,
                    y:40
                },
                { 
                    x:8.3,
                    y:350
                },
                { 
                    x:7.7,
                    y:290
                },
                { 
                    x:7.2,
                    y:320
                }
            ]
        },
        {   type: 'bubble',
            name: "Industrial",
            color: "#f9d1ae",
            dataLabels: {
                enabled: true,
                formatter:function() {
                    return this.point.name;
                },
                style:{color:"black"}
            },   
            data: [
                {   name: '1999',
                    x:4,
                    y:40
                },
                {   
                    name: '1984',
                    x:5,
                    y:30
                },
                {   name: '1994',
                    x:4.9,
                    y:190
                },
                {   name: '1886',
                    x:5,
                    y:120
                },
                {   name: '1992',
                    x:4.8,
                    y:75
                },
                {   name:'1784',
                    x:5,
                    y:30
                },
                {   name:'2001',
                    x:14.7,
                    y:230
                },
                {   name:'1884',
                    x:6.6,
                    y:220
                },
                {   name:'1903',
                    x:9.3,
                    y:40
                },
                {   name:'2005',
                    x:8.3,
                    y:350
                },
                {   name:'1882',
                    x:7.7,
                    y:290
                },
                {   name:'2011',
                    x:7.2,
                    y:320
                }
            ]
        },
    ]

    });

    $('.line-people-company').highcharts({
        chart: {
            type: 'spline'
        },
        plotOptions: {
            series: {
                color: '#3562d5',
                lineWidth: 2

            }
        },        
        xAxis: {
            gridLineWidth: 0,
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']               
        },
        yAxis: {
            title: {
                text: ''
            }             
        },
        tooltip: {
            valueSuffix: ''
        },

        series: [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }]
    });

    $('.column-people-company').highcharts({
            chart: {
                type: 'bar',
                renderTo : this,
                marginBottom: 0,
                plotBorderWidth: 0,
                borderWidth: 0,
                height: 200
            },
            // colors: ['#2980b9', '#00e676'],
            xAxis: {
                categories: ['Company','Sector'],
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                gridLineColor: 'transparent',
                minorTickLength: 0,
                labels: {
                    style: {
                        color: '#292929',
                        fontSize:'18px',
                        fontFamily: 'Open Sans'
                    } 
                },
                tickLength: 0          
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    enabled: false   
                },
                title: {
                    text: null  
                },            
                gridLineWidth: 0,
                minorGridLineWidth: 0     
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series:{
                    pointWidth: 50
                },
                bar:{
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        inside: true,
                        style: {
                            fontSize:'16px'
                        },                         
                        formatter: function() {
                            return this.y + '%';
                        }
                    }                    
                },
                // series: {
                //     stacking: 'percent',
                // }
            },
            series: [{
                data: [{y:98, color:'#2980b9' }, {y:75, color: '#00e676'}]
            }], 

            title:{text: null}
        
        });    
    $('.line-investment-company').highcharts({
        chart: {
            type: 'line',
            borderWidth:0,
            plotBorderWidth: 0,

        },
        xAxis: {
            gridLineWidth: 0,
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            }

        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                }
            },
            series: {
                lineWidth: 3
            }
        },        
        series: [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: 'London',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
    });
    
    $('.column-patent-company').highcharts({
        chart: {
            type: 'column',
            borderWidth:0,
            plotBorderWidth: 0,            
        },
        title: {
            text: 'Stacked column chart'
        },
        legend:{
            enabled: true
        },
        xAxis: {
            gridLineWidth: 0,
            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total fruit consumption'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        series: [{
            name: 'John',
            data: [5, 3, 4, 7, 2, 4, 2,]
        }, {
            name: 'Jane',
            data: [2, 2, 3, 2, 1,3,6]
        }, {
            name: 'Joe',
            data: [3, 4, 4, 2, 5, 2]
        }]
    });
$('.line-customer-company').highcharts({
        chart: {
            type: 'spline'           
        },    
        plotOptions: {
            series: {
                color: '#3562d5',
                lineWidth: 2

            }
        },        
        xAxis: {
            gridLineWidth: 0,
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']               
        },
        yAxis: {

            title: {
                text: ''
            }             
        },
        tooltip: {
            valueSuffix: ''
        },

        series: [{
            name: 'Tokyo',
            data: [12, 8, 7.0, 6.9, 9.5, 3.5, 7.2, 11.5, 25.2, 10.5, 12.3, 8.3]
        }]
    });

    $('.line-return-company').highcharts({
        chart: {
            type: 'spline'           
        },    

        plotOptions: {
            series: {
                lineWidth: 2
            }
        },        
        xAxis: {
            gridLineWidth: 0,
            categories: ['1985', '1986', '1987', '1988', '1989', '1990',
                '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998','1999', 2000]            
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return this.value + '%';
                }
            },            

        },
        tooltip: {
            valueSuffix: ''
        },

        series: [{
            name: 'Data',
            data: [1700, 500, 800 , 500, 600, 500, 400, 450, 350, 500  , 600, 1000, 800, 8000, 600, 400]
        }]
    });
});