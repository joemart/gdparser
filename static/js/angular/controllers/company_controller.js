
financial_app.controller('CompanyCtrl', [
    '$scope','$preloaded', function ($scope, $preloaded) {
        $scope.summary = $preloaded.summary;


        var investment_series_company_bubbles = [];
        var investment_series_company_line = [];
        var investment_series_sector_bubbles = [];
        var investment_series_sector_line = [];

        var glassdoor_series_sector_line = [];
        var glassdoor_categories_line = [];
        var glassdoor_series_company_line = [];

        var amazon_series_sector_line = [];
        var amazon_categories_line = [];
        var amazon_series_company_line = [];

        var rnd_series_sector_line = [];
        var rnd_categories_line = [];
        var rnd_series_company_line = [];

        var patents_series_sector_line = [];
        var patents_categories_line = [];
        var patents_series_company_line = [];

        var ret_rnd_series_sector_line = [];
        var ret_rnd_categories_line = [];
        var ret_rnd_series_company_line = [];

        var patents_by_categories_series = [];
        var patents_by_categories_categories = [];

        angular.forEach($preloaded.investment.company, function (value, year) {
            investment_series_company_bubbles.push([value.cap_rnd * 100, value.return_on_rnd * 100, year]);
            investment_series_company_line.push([value.cap_rnd * 100, value.return_on_rnd * 100]);
        });
        angular.forEach($preloaded.investment.sector, function (value, year) {
            investment_series_sector_bubbles.push([value.cap_rnd * 100, value.return_on_rnd * 100, year]);
            investment_series_sector_line.push([value.cap_rnd * 100, value.return_on_rnd * 100]);
        });


        angular.forEach($preloaded.glassdoor.sector, function (value, year) {
            glassdoor_categories_line.push(year);
            glassdoor_series_sector_line.push(value);
        });
        angular.forEach($preloaded.glassdoor.company, function (value, year) {
            glassdoor_series_company_line.push(value);
        });

        angular.forEach($preloaded.amazon.sector, function (value, year) {
            amazon_categories_line.push(year);
            amazon_series_sector_line.push(value);
        });
        angular.forEach($preloaded.amazon.company, function (value, year) {
            amazon_series_company_line.push(value);
        });


        angular.forEach($preloaded.rnd_by_sales.sector, function (value, year) {
            rnd_categories_line.push(year);
            rnd_series_sector_line.push(value*100);
        });
        angular.forEach($preloaded.rnd_by_sales.company, function (value, year) {
            rnd_series_company_line.push(value.rnd_by_sales*100);
        });

        angular.forEach($preloaded.patents.sector, function (value, year) {
            patents_categories_line.push(year);
            patents_series_sector_line.push(value);
        });
        angular.forEach($preloaded.patents.company, function (value, year) {
            patents_series_company_line.push(value);
        });

        angular.forEach($preloaded.return_on_rnd.sector, function (value, year) {
            ret_rnd_categories_line.push(year);
            ret_rnd_series_sector_line.push(value.return_on_rnd*100);
        });
        angular.forEach($preloaded.return_on_rnd.company, function (value, year) {
            ret_rnd_series_company_line.push(value*100);
        });

        var LEGEND_PATENTS = {
            'A': 'HUMAN NECESSITIES',
            'B': 'PERFORMING OPERATIONS; TRANSPORTING',
            'C': 'CHEMISTRY; METALLURGY',
            'D': 'TEXTILES; PAPER',
            'E': 'FIXED CONSTRUCTIONS',
            'F': 'MECHANICAL ENGINEERING; LIGHTING; HEATING; WEAPONS; BLASTING',
            'G': 'PHYSICS',
            'H': 'ELECTRICITY'
        };

        angular.forEach($preloaded.patents_by_category, function (value, category) {

            var data = [];
            angular.forEach(value, function (val, year) {
                if(patents_by_categories_categories.indexOf(year)==-1){
                    patents_by_categories_categories.push(year);
                }
                data.push(val*100);

            });
            patents_by_categories_series.push({
                name:LEGEND_PATENTS[category],
                data:data,
                events:{'click':function(series){console.log(series.currentTarget.name);}}
            });

        });

        $scope.investmentChartConfig = {
            "options": {
                "chart": {
                    zoomType: 'xy'
                }
                ,
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    series: {},
                    bubble: {
                        minSize: 25,
                        maxSize: 25,
                        dataLabels: {
                            align: 'center',
                            enabled: true,
                            format: "{point.z}"
                        }
                    },
                    spline: {
                        lineWidth: 1,
                        tooltip: {
                            pointFormat: ''
                        }
                    }
                }
            },
            "series": [{
                name: $preloaded.name.company,
                type: 'bubble',
                data: investment_series_company_bubbles,
                tooltip: {
                    enabled: true,
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Investment: <b>{point.x:,.1f} %</b><br/>Return on Investment: <b>{point.y:,.1f} %</b><br/>Year: <b>{point.z}</b>'
                }
            },
                {
                    name: $preloaded.name.company,
                    type: 'spline',
                    data: investment_series_company_line

                },
                {
                    name: $preloaded.name.sector,
                    type: 'spline',
                    data: investment_series_sector_line

                }, {
                name: $preloaded.name.sector,
                type: 'bubble',
                data: investment_series_sector_bubbles,
                tooltip: {
                    enabled: true,
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Investment: <b>{point.x:,.1f} %</b><br/>Return on Investment: <b>{point.y:,.1f} %</b><br/>Year: <b>{point.z}</b>'
                }
            }],

            "title": {"text": ""},
            xAxis: {
                title: {text: 'Investment (value of capitalised R&D relative to sales)'},
                labels: {
                    format: "{value}%"
                }
            },
            yAxis: {
                title: {text: 'Return on Investment (earnings uplift relative to value of R&D Asset)'},
                labels: {
                    format: "{value}%"
                }
            },
            size: {
                width: 800,
                height: 500
            }
        };

        $scope.glassdoorChartConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Average review rate: <b>{point.y:,.1f}</b>'
                },
                legend:{
                    verticalAlign: "top",
                    align: "right"
                }
            },
            title: {
                text: 'Employer satisfaction.',
                align: "left"
            },
            series: [{
                data: glassdoor_series_company_line,
                name: $preloaded.name.company
            },
            {
                data: glassdoor_series_sector_line,
                name: $preloaded.name.sector
            }],
            xAxis: {
                categories: glassdoor_categories_line,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}"
                },
                title: {text: ''}
            }
        };

        $scope.amazonChartConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Average review rate: <b>{point.y:,.1f}</b>'
                },
                legend:{
                    verticalAlign: "top",
                    align: "right"
                }
            },
            title: {
                text: 'Customer reviews.',
                align: "left"
            },
            series: [{
                data: amazon_series_company_line,
                name: $preloaded.name.company
            },
            {
                data: amazon_series_sector_line,
                name: $preloaded.name.sector
            }],
            xAxis: {
                categories: amazon_categories_line,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}"
                },
                title: {text: ''}
            }
        };

        $scope.rndBySalesChartConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'R&D to sales: <b>{point.y:,.1f}%</b>'
                },
                legend: {
                    verticalAlign: "top",
                    align: "right"
                }
            },
            title: {
                text: 'R&D to sales intensity.',
                align: "left"
            },
            series: [{
                data: rnd_series_company_line,
                name: $preloaded.name.company
            },
                {
                    data: rnd_series_sector_line,
                    name: $preloaded.name.sector
                }],
            xAxis: {
                categories: rnd_categories_line,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}%"
                },
                title: {text: ''}
            }
        };

        $scope.patentsBySalesChartConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Patents per $bn sales: <b>{point.y:,.1f}</b>'
                },
                legend: {
                    verticalAlign: "top",
                    align: "right"
                }
            },
            title: {
                text: 'Patents registered per $bn sales.',
                align: "left"
            },
            series: [{
                data: patents_series_company_line,
                name: $preloaded.name.company
            },
                {
                    data: patents_series_sector_line,
                    name: $preloaded.name.sector
                }],
            xAxis: {
                categories: patents_categories_line,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}"
                },
                title: {text: ''}
            }
        };

        $scope.returnOnRnDConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Return on R&D: <b>{point.y:,.1f} %</b>'
                },
                legend: {
                    verticalAlign: "top",
                    align: "right"
                }
            },
            title: {
                text: 'Return on R&D.',
                align: "left"
            },
            series: [{
                data: ret_rnd_series_company_line,
                name: $preloaded.name.company
            },
                {
                    data: ret_rnd_series_sector_line,
                    name: $preloaded.name.sector
                }],
            xAxis: {
                categories: ret_rnd_categories_line,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}%"
                },
                title: {text: ''}
            }
        };

        $scope.patentsByCategoriesChartConfig = {
            options: {
                chart: {
                    type: 'column'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    //format: "{value}%"
                    pointFormat: '{series.name}: <b>{point.y:,.1f}%</b>'
                },
                legend:{
                    align: 'right',
                    x: 10,
                    verticalAlign: 'top',
                    //y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false
                        }
                    }
                }

            },
            title: {
                text: '',
                align: "left"
            },
            series: patents_by_categories_series,
            xAxis: {
                categories: patents_by_categories_categories//,
                //title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}%"
                },
                title: {text: ''}
            },
            size: {
                width: 700,
                height: 500
            }
        };

    }]);