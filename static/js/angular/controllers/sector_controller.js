
financial_app.controller('ChartsCtrl', [
    '$scope','$preloaded', function ($scope, $preloaded) {

        var LEGEND_PATENTS = {
            'A': 'HUMAN NECESSITIES',
            'B': 'PERFORMING OPERATIONS; TRANSPORTING',
            'C': 'CHEMISTRY; METALLURGY',
            'D': 'TEXTILES; PAPER',
            'E': 'FIXED CONSTRUCTIONS',
            'F': 'MECHANICAL ENGINEERING; LIGHTING; HEATING; WEAPONS; BLASTING',
            'G': 'PHYSICS',
            'H': 'ELECTRICITY'
        };

        var overview_chart_series = [];
        var rnd_by_sales_series = [];
        var rnd_by_sales_categories = [];
        var patents_by_sales_series = [];
        var patents_by_sales_categories = [];
        var patents_by_categories_series = [];
        var patents_by_categories_categories = [];
        var return_on_rnd_series = [];
        var return_on_rnd_categories = [];
        angular.forEach($preloaded.overview_chart_data, function (value, sector) {
            overview_chart_series.push({
                name: sector,
                data: [[value.cap_rnd_by_sales*100, value.roi*100, value.rnd_adj/1000]],
                sizeBy: 'width'
            });
        });
        angular.forEach($preloaded.rnd_by_sales, function (value, year) {

            rnd_by_sales_categories.push(year);
            rnd_by_sales_series.push(value.rnd_by_sales*100);
        });

        angular.forEach($preloaded.patents_by_sales, function (value, year) {
            patents_by_sales_categories.push(year);
            patents_by_sales_series.push(value);
        });

        angular.forEach($preloaded.patents_by_category, function (value, category) {

            var data = [];
            angular.forEach(value, function (val, year) {
                if(patents_by_categories_categories.indexOf(year)==-1){
                    patents_by_categories_categories.push(year);
                }
                data.push(val*100);

            });
            patents_by_categories_series.push({
                name:LEGEND_PATENTS[category],
                data:data
            });

        });

        angular.forEach($preloaded.return_on_rnd, function (value, year) {
            return_on_rnd_categories.push(year);
            return_on_rnd_series.push(value.return_on_rnd*100);
        });

        $scope.overviewChartConfig = {
            "options": {
                chart: {
                    type: 'bubble',
                    zoomType: 'xy'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            align: 'center',
                            enabled: true,
                            format: "{series.name}"
                        }
                    },
                    bubble: {
                        minSize: 25,
                        maxSize: 60
                    }
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Investment: <b>{point.x:,.1f} %</b><br/>Return on Investment: <b>{point.y:,.1f} %</b><br/>Total R&D: <b>{point.z:,.2f} b</b>'
                }
            },
            "series": overview_chart_series,
            xAxis: {
                title: {text: 'Investment (value of capitalised R&D relative to sales)'},
                labels: {
                    format: "{value}%"
                }
            },
            yAxis: {
                title: {text: 'Return on Investment (earnings uplift relative to value of R&D Asset)'},
                labels: {
                    format: "{value}%"
                }
            },
            "title": {"text": ""},
            size: {
            //    width: 600,
                height: 600
            }
        };

        $scope.rndBySalesChartConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'R&D to sales: <b>{point.y:,.1f} %</b>'
                },
                legend:{
                    verticalAlign: "top",
                    align: "right"
                }

            },
            title: {
                text: 'RnD to Sales intensity.',
                align: "left"
            },
            series: [{
                data: rnd_by_sales_series,
                name: $preloaded.sector_name.name
            }],
            xAxis: {
                categories:rnd_by_sales_categories,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}%"
                },
                title: {text: ''}
            }
        };

        $scope.patentsBySalesChartConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Patents per $bn sales: <b>{point.y:,.1f}</b>'
                },
                legend:{
                    verticalAlign: "top",
                    align: "right"
                }

            },
            title: {
                text: 'Patents registered per $bn sales.',
                align: "left"
            },
            series: [{
                data: patents_by_sales_series,
                name: $preloaded.sector_name.name
            }],
            xAxis: {
                categories: patents_by_sales_categories,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}"
                },
                title: {text: ''}
            }
        };

        $scope.patentsByCategoriesChartConfig = {
            options: {
                chart: {
                    type: 'column'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    //format: "{value}%"
                    pointFormat: '{series.name}: <b>{point.y:,.1f}%</b>'
                },
                legend:{
                    align: 'right',
                    //x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false
                        }
                    }
                }

            },
            title: {
                text: 'Patents approvals by category.',
                align: "left"
            },
            series: patents_by_categories_series,
            xAxis: {
                categories: patents_by_categories_categories//,
                //title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}%"
                },
                title: {text: ''}
            }
        };

        $scope.returnOnRnDConfig = {
            options: {
                chart: {
                    type: 'spline'
                },
                tooltip: {
                    style: {
                        padding: 10
                        //fontWeight: 'bold'
                    },
                    pointFormat: 'Return on R&D: <b>{point.y:,.1f} %</b>'
                },
                legend:{
                    verticalAlign: "top",
                    align: "right"
                }

            },
            title: {
                text: 'Return on R&D.',
                align: "left"
            },
            series: [{
                data: return_on_rnd_series,
                name: $preloaded.sector_name.name
            }],
            xAxis: {
                categories:return_on_rnd_categories,
                title: {text: ''}
            },
            yAxis: {
                labels: {
                    format: "{value}%"
                },
                title: {text: ''}
            }
        };

    }]);