
financial_app.controller('OverviewChartCtrl', [
    '$scope','$preloaded', function ($scope, $preloaded) {

        var series = [{data:[]}];
        angular.forEach($preloaded.overview_chart_data, function (value, sector) {
            series[0].data.push({x: value.cap_rnd_by_sales*100, y: value.roi*100, z: value.rnd_adj/1000, name: sector});
        });

        $scope.chartConfig = {
            "options": {
                chart: {
                    type: 'bubble',
                    plotBorderWidth: 1,
                    plotBorderColor: '#243C8C',
                    zoomType: 'xy',
                    resetZoomButton: {
                        x: 0,
                        y: -30
                    },
                    style: {
                        fontFamily: 'Open Sans',
                        fontSize: '14px'
                    }
                    //zoomType: 'xy'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            align: 'center',
                            enabled: true,
                            format: "{point.name}"
                        }
                    }
                },
                //tooltip: {
                //    style: {
                //        padding: 10
                //        //fontWeight: 'bold'
                //    },
                //    pointFormat: 'Investment: <b>{point.x:,.1f} %</b><br/>Return on Investment: <b>{point.y:,.1f} %</b><br/>Total R&D: <b>{point.z:,.2f} b</b>'
                //},
                tooltip: {
                    useHTML: true,
                    headerFormat: '<table>',
                    pointFormat: '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +
                    '<tr><th>Investment:</th><td>{point.x:,.1f} %</td></tr>' +
                    '<tr><th>Return on Investment: </th><td>{point.y:,.1f} %</td></tr>' +
                    '<tr><th>Total R&D: </th><td>{point.z:,.2f} b</td></tr>',
                    footerFormat: '</table>',
                    followPointer: true
                },
                legend: {
                    enabled: false
                }
            },
            "series": series,
            xAxis: {
                gridLineColor: '#E8EAF6',
                gridLineWidth: 1,
                tickColor: '#243C8C',
                lineColor: '#243C8C',

                title: {
                    text: 'Investment (value of capitalised R&D relative to sales)', style: {
                        fontSize: '16px',
                        color: '#292929'
                    }
                },
                labels: {
                    format: "{value}%",
                    style: {
                        fontSize: '16px',
                        color: '#292929'
                    }
                }
            },
            yAxis: {
                gridLineColor: '#E8EAF6',
                tickColor: '#243C8C',
                startOnTick: false,
                endOnTick: false,
                title: {
                    text: 'Return on Investment (earnings uplift relative to value of R&D Asset)',
                    style: {
                        fontSize: '16px',
                        color: '#292929'
                    }
                },
                labels: {
                    format: "{value}%",
                    style: {
                        fontSize: '16px',
                        color: '#292929'
                    }
                }
            },
            "title": {"text": ""}
        };

    }]);