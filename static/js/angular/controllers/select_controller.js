/**
 * Created by orion on 25.08.15.
 */



financial_app.controller('SelectCtrl', [
    '$scope', '$rootScope', 'SectorsList', 'CompaniesList','$window','$timeout', function ($scope, $rootScope, SectorsList, CompaniesList,$window,$timeout) {
        SectorsList.query().$promise.then(function (data) {
            $scope.sectors = data;
        }, function (err) {
        });

        var get_companies = function (sector) {
            var data = {};
            if (sector) {
                data['sector'] = sector.yahoo_industry;
            }
            CompaniesList.get(data).$promise.then(function (data) {
                $scope.companies = data;
            }, function (err) {
            });
        };

        $timeout(get_companies);

        $scope.$watch('current_sector', function (n, o) {
            if (n) {
                get_companies(n);
            }
        });

        $scope.load_company = function () {
            $window.location.href = '/company/'+$scope.current_company.pk+'/';
            //$rootScope.$broadcast('loadRequest', {mode:'company', key: $scope.current_company.pk});
        };

        $scope.load_sector = function () {
            $window.location.href = '/sector/'+$scope.current_sector.yahoo_industry+'/';
            //document.location=
            //$rootScope.$broadcast('loadRequest', {mode:'sector', key: $scope.current_sector.yahoo_industry});
        };


    }
]);


