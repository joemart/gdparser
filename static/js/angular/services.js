var servicesModule = angular.module('financialServices', ['ngResource']);

servicesModule.factory('SectorsList', ['$resource',
    function ($resource) {
        return $resource('/api/sectors_list/', {}, {
            query: {method: 'GET', params: {}, isArray:true}
        });
}]);

servicesModule.factory('CompaniesList', ['$resource',
    function ($resource) {
        return $resource('/api/companies_list/', {}, {
            get: {method: 'GET', params: {}, isArray:true}
        });
}]);




servicesModule.factory('CompanyPatentInfo', ['$resource',
    function ($resource) {
        return $resource('/api/company/patents/:pk/', {pk:'@pk'}, {
            get: {method: 'GET', params: {}}
        });
}]);

servicesModule.factory('SectorPatentInfo', ['$resource',
    function ($resource) {
        return $resource('/api/sector/patents/', {}, {
            get: {method: 'GET', params: {}, isArray:true}
        });
}]);

servicesModule.factory('CompanyRnDInfo', ['$resource',
    function ($resource) {
        return $resource('/api/company/rnd/:pk/', {pk:'@pk'}, {
            get: {method: 'GET', params: {}}
        });
}]);

servicesModule.factory('SectorRnDInfo', ['$resource',
    function ($resource) {
        return $resource('/api/sector/rnd/', {}, {
            get: {method: 'GET', params: {}, isArray:true}
        });
}]);




servicesModule.factory('TabManager', ['$http', function ($http) {

    return {
        students: students,
        save:     save
    };
}]);
