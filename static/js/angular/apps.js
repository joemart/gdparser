function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined
}
var financial_app = angular.module('financial', ['ngRoute','ngCookies','financialServices',  'angular-bootstrap-select',"highcharts-ng", "gs.preloaded"]). //'ui.bootstrap',
    config(function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('((');
    $interpolateProvider.endSymbol('))');

    $httpProvider.defaults.headers.common['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
});

financial_app.config(function ($httpProvider) {
    $httpProvider.defaults.transformRequest = function(data, headersGetter){
        var headers = headersGetter();
        headers["X-CSRFToken"]= getCookie('csrftoken');
        return JSON.stringify(data);
    }
});