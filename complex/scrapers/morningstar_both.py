from complex.models import CompanyOutput, CompanyOutputFinancial

__author__ = 'orion'
from morningstar import morning_base_parse as tools
from morningstar2 import morning_base_parse as financials


def clear_none(company_fin_outputs_fin):
    CLEAR_PARAMS = (
    'sales', 'research_and_development', 'operating_income', 'net_income', 'depreciation_and_amortisation',
    'net_ppe',
    'intangible_assets', 'total_assets', 'total_stockholder_equity')
    for y in company_fin_outputs_fin.iterkeys():
        for param in CLEAR_PARAMS:
            if not company_fin_outputs_fin[y][param] or company_fin_outputs_fin[y][param] == '-' or company_fin_outputs_fin[y][param].strip() == '':
                company_fin_outputs_fin[y][param] = None
            else:
                company_fin_outputs_fin[y][param] = float(
                    company_fin_outputs_fin[y][param].replace(',', '').replace(' ', ''))
    return company_fin_outputs_fin


def morning_base_parse(url_fin, url_tools, company_output_id=None):
    NECESSARY_FIELDS = ('research_and_development','operating_income', 'sales', 'total_assets')
    if url_fin:
        company_output_pre_res_fin, company_fin_outputs_fin = financials(url_fin, company_output_id)
    elif url_tools:
        company_output_pre_res_fin, company_fin_outputs_fin = tools(url_fin, company_output_id)
    else:
        raise Exception('At least one of urls should be set')
    company_fin_outputs_fin = clear_none(company_fin_outputs_fin)
    all_years_present = True
    if url_fin and url_tools:
        for y in company_fin_outputs_fin.iterkeys():
            for field in NECESSARY_FIELDS:
                if not company_fin_outputs_fin[y][field]:
                    all_years_present = False
    if not all_years_present:
        company_output_pre_res_tools, company_fin_outputs_tools = tools(url_tools, company_output_id)
        company_fin_outputs_tools = clear_none(company_fin_outputs_tools)
        for y in company_fin_outputs_tools.iterkeys():
            if company_fin_outputs_tools[y]['operating_income'] and company_fin_outputs_tools[y]['depreciation_and_amortisation']:
                company_fin_outputs_tools[y]['operating_income'] += company_fin_outputs_tools[y]['depreciation_and_amortisation']
        for y in company_fin_outputs_fin.iterkeys():
            for param in company_fin_outputs_fin[y].iterkeys():
                company_fin_outputs_fin[y][param] = company_fin_outputs_fin[y][param] or company_fin_outputs_tools[y][param]
    company_output = CompanyOutput.objects.get(pk=company_output_id)
    for param in company_output_pre_res_fin:
        setattr(company_output, param, company_output_pre_res_fin[param])
    company_output.save()

    for param in company_output_pre_res_fin:
        setattr(company_output, param, company_output_pre_res_fin[param])
    company_output.save()

    for y in company_fin_outputs_fin:
        company_output_fin, created = CompanyOutputFinancial.objects.get_or_create(company=company_output.company,
                                                                                   year=y)
        for param in company_fin_outputs_fin[y]:
            setattr(company_output_fin, param, company_fin_outputs_fin[y][param])
        company_output_fin.save()