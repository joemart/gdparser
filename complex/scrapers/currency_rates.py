__author__ = 'orion'
from complex.models import Company, CompanyOutput, ExchangeRates, CompanyOutputFinancial
import datetime
import cookielib
import urllib2
from StringIO import StringIO
import gzip
import json

cj_rates = cookielib.CookieJar()
opener_rates = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj_rates))
opener_rates.addheaders = [
    ('Accept', 'text/javascript, text/html, application/xml, text/xml, */*'),
    ('Accept-Encoding', 'gzip, deflate, sdch'),
    ('Accept-Language', 'en-US,en;q=0.8'),
    ('Connection', 'keep-alive'),
    ('Host', 'www.oanda.com'),
    ('Referer', 'http://www.oanda.com/lang/ru/currency/historical-rates/'),
    ('User-Agent',
     'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
    ('X-Prototype-Version', '1.7'),
    ('X-Requested-With', 'XMLHttpRequest')
]


def get_rates(currency, year):
        if currency == "USD":
            return 1
        ex_rate, created = ExchangeRates.objects.get_or_create(currency=currency, year=year)
        now = datetime.datetime.now()
        if created or now.year == year and ex_rate.last_updated.month < now.month:
            start_date = '%s-1-1' % year
            end_date = '%s-12-31' % year
            page = opener_rates.open('http://www.oanda.com/currency/historical-rates/')  # Refresh opener cookies
            page = opener_rates.open(
                'http://www.oanda.com/currency/historical-rates/update?quote_currency=USD&end_date=%s&start_date=%s&period=monthly&display=absolute&rate=0&data_range=c&price=bid&view=table&base_currency_0=%s&base_currency_1=&base_currency_2=&base_currency_3=&base_currency_4=&' % (
                    end_date, start_date, currency))
            if page.info().get('Content-Encoding') == 'gzip':
                buf = StringIO(page.read())
                f = gzip.GzipFile(fileobj=buf)
                data = f.read()
                buf.close()
            else:
                data = page.read()
            data = json.loads(data)
            rates = [i[1] for i in data['widget'][0]['data']]
            if len(rates) == 0:
                ex_rate.value = 0
            else:
                ex_rate.value = sum(rates) / len(rates)
            ex_rate.save()
        return ex_rate.value