import datetime
from lxml import html as html
from lxml.etree import tostring
from selenium.common.exceptions import NoSuchElementException
from base.modules.captcha_processor import resolve_captcha
from complex.models import PatentOutput, PatentOutputApplicant
import urllib2
import cookielib
from time import sleep
from celery import signature
import logging
from selenium import webdriver
from complex.scrapers.patents import get_page_code_gzip
from django.conf import settings
if settings.USE_VIRTUAL_DISPLAY:  # use virtual display only on production
    try:
        from pyvirtualdisplay import Display
    except:
        pass
logger = logging.getLogger()

cj_patents = cookielib.CookieJar()
opener_patents = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj_patents))

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.151 Safari/535.19"


def get_headers():
    return [
        ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
        ('Accept-Encoding', 'gzip, deflate'),
        ('Accept-Language', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'),
        ('Cache-Control', 'max-age=0'),
        ('Connection', 'keep-alive'),
        ('Content-Type', 'application/x-www-form-urlencoded'),
        ('Host', 'patentscope.wipo.int'),
        ('Origin', 'https://patentscope.wipo.int'),
        ('Referer', 'https://patentscope.wipo.int/search/en/structuredSearch.jsf'),
        ('User-Agent', USER_AGENT)
    ]


opener_patents.addheaders = get_headers()


def alternate_detail_parsing(company_id):
    errors_count = 0
    uncomplete_patents = PatentOutput.objects.filter(company_id=company_id, details_parsed=False)
    if not uncomplete_patents.exists():
        return
    driver = webdriver.Firefox()
    driver.implicitly_wait(10)
    for patent in uncomplete_patents:
        driver.get(patent.publication_link)
        try:
            code = driver.find_element_by_id('detailMainForm').get_attribute('innerHTML')
            page = html.fromstring(code)
            pct = False

            try:
                cont = page.get_element_by_id('detailMainForm:NationalBiblio:content')
            except:
                cont = page.get_element_by_id('detailPCTtableHeader')
                pct = True
            if pct:
                app_date = None
                grant_date = None
                pub_date = cont.get_element_by_id('detailPCTtablePubDate').text_content()
                app_cont = cont.get_element_by_id('PCTapplicants')
                applicants = [i.text_content() for i in app_cont.cssselect('b')]
            else:
                app_date = cont.xpath('//*/span[text()="Application Date: "]')[0].getparent().getnext().text_content()
                grant_date_cont = cont.xpath('//*/span[text()="Grant Date: "]')
                grant_date = grant_date_cont[0].getparent().getnext().text_content() if len(grant_date_cont) else None
                pub_date = cont.xpath('//*/span[text()="Publication Date: "]')[0].getparent().getnext().text_content()
                a = tostring(cont.get_element_by_id('detailMainForm:NPapplicants'))

                applicants = [i for i in a[a.find('>') + 1:a.rfind('<')].split('<br/>') if i != '']
            patent.application_date = datetime.datetime.strptime(app_date, "%d.%m.%Y").date() if app_date else None
            patent.publication_date = datetime.datetime.strptime(pub_date, "%d.%m.%Y").date()
            if grant_date:
                patent.grant_date = datetime.datetime.strptime(grant_date, "%d.%m.%Y").date()
            patent.details_parsed = True

            if len(applicants) == 1:
                patent.applicant = applicants[0]
                patent.save()
            elif len(applicants) > 1:
                patent.many_applicants = True
                patent.save()
                for applicant in applicants:
                    PatentOutputApplicant.objects.get_or_create(name=applicant, patent=patent)


        except NoSuchElementException as e:
            logger.warning('parse except', exc_info=True)
            try:
                if driver.find_element_by_css_selector('#capForm\:fpSearchTabPanel img').is_displayed():
                    logger.warning('captcha - try resolve', exc_info=True)
                    errors_count += 1
                    # su.set_text_status('CAPTCHA detected. Try resolve.')
                    if not resolve_captcha(driver, time_sleep=1, captcha_selector='#capForm\:fpSearchTabPanel img',
                                           captcha_input_selector="#capForm\:jcaptcha"):
                        # su.set_text_status('Can\'t CAPTCHA resolve. Login canceled.')
                        # logger.error('Can\'t CAPTCHA resolve. Login canceled.')
                        raise Exception('Can\'t CAPTCHA resolve. patent details canceled.')
                    else:
                        pass
            except NoSuchElementException:
                logger.warning('no captcha', exc_info=True)

    driver.close()
    if errors_count:
        alternate_detail_parsing(company_id)


def parse_detail_patent_page(patent_id, browser_try_count=1, try_count=1, pct=False):
    patent = PatentOutput.objects.get(pk=patent_id)
    code = get_page_code_gzip(opener_patents.open(patent.publication_link))
    page = html.fromstring(code)

    try:
        if not pct:
            cont = page.get_element_by_id('detailMainForm:NationalBiblio:content')
        else:
            cont = page.get_element_by_id('detailPCTtableHeader')

    except Exception as e:
        if not pct:
            try:
                page.get_element_by_id('detailPCTtableHeader')
                return parse_detail_patent_page(patent_id, try_count=1, pct=True)
            except:
                pass
        if try_count < 3:
            sleep(5)
            return parse_detail_patent_page(patent_id, try_count=try_count + 1)
        if settings.USE_VIRTUAL_DISPLAY:
            display = Display(visible=0, size=(800, 600))
            display.start()
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override", USER_AGENT)
        driver = webdriver.Firefox(profile)
        for cookie in cj_patents:
            if cookie.name == "JSESSIONID":
                driver.add_cookie({"JSESSIONID": cookie.value})
        driver.implicitly_wait(10)
        try:

            driver.get(patent.publication_link)
            if driver.find_element_by_css_selector('#capForm\:fpSearchTabPanel img').is_displayed():
                if not resolve_captcha(driver, time_sleep=1, captcha_selector='#capForm\:fpSearchTabPanel img',
                                       captcha_input_selector="#capForm\:jcaptcha"):
                    raise Exception('Can\'t CAPTCHA resolve. patent details canceled.')
                else:
                    ck = cookielib.Cookie(version=0, name='JSESSIONID', value=driver.get_cookie('JSESSIONID')['value'],
                                          port=None, port_specified=False, domain='patentscope.wipo.int',
                                          domain_specified=False, domain_initial_dot=False, path='/',
                                          path_specified=True, secure=False, expires=None, discard=True, comment=None,
                                          comment_url=None, rest={'HttpOnly': None}, rfc2109=False)
                    cj_patents.clear()
                    cj_patents.set_cookie(ck)

        except NoSuchElementException:
            pass
        finally:
            driver.close()
        if browser_try_count <= 3:
            return parse_detail_patent_page(patent_id,browser_try_count+1)
        else:
            raise Exception('Cant process page %s with 3 tries' % patent.publication_link)

    if not pct:
        app_date = cont.xpath('//*/span[text()="Application Date: "]')[0].getparent().getnext().text_content()
        grant_date_cont = cont.xpath('//*/span[text()="Grant Date: "]')
        grant_date = grant_date_cont[0].getparent().getnext().text_content() if len(grant_date_cont) else None
    else:
        app_date = None
        grant_date = None
    if not pct:
        pub_date = cont.xpath('//*/span[text()="Publication Date: "]')[0].getparent().getnext().text_content()
    else:
        pub_date = cont.get_element_by_id('detailPCTtablePubDate').text_content()

    if not pct:
        a = tostring(cont.get_element_by_id('detailMainForm:NPapplicants'))
        applicants = [i for i in a[a.find('>') + 1:a.rfind('<')].split('<br/>') if i != '']
    else:
        app_cont = cont.get_element_by_id('PCTapplicants')
        applicants = [i.text_content() for i in app_cont.cssselect('b')]

    patent.application_date = datetime.datetime.strptime(app_date, "%d.%m.%Y").date() if app_date else None
    patent.publication_date = datetime.datetime.strptime(pub_date, "%d.%m.%Y").date()
    if grant_date:
        patent.grant_date = datetime.datetime.strptime(grant_date, "%d.%m.%Y").date()
    patent.details_parsed = True

    if len(applicants) == 1:
        patent.applicant = applicants[0]
        patent.save()
    elif len(applicants) > 1:
        patent.save()
        for applicant in applicants:
            PatentOutputApplicant.objects.get_or_create(name=applicant, patent=patent)