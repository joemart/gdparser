from complex.scrapers.currency_rates import get_rates
from complex.models import Company, CompanyOutput, ExchangeRates, CompanyOutputFinancial, FadeRates
import urllib2
from urlparse import urlparse
from lxml.html.clean import clean_html
import lxml.html as html
import cookielib

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
opener.addheaders = [
    ('User-Agent',
     'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
    ('Host', 'tools.morningstar.co.uk'),
    ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
]


def _morningstar_parse_table_by_header(table, header):
    return [i.text_content() for i in table.xpath('//*/th[text()="%s"]' % header)[0].getparent().getchildren()[1:]]


def _morningstar_page_load(url):
    code = opener.open(url).read()
    return html.fromstring(clean_html(code.replace('</html>', '').replace('</body>', '') + '</body></html>'))


def morning_base_parse(url, company_output_id=None):
    company_output = CompanyOutput.objects.get(pk=company_output_id)
    company_output_pre_res = {}
    page = _morningstar_page_load(url)
    company_name = page.find_class('securityName')[0].text_content()
    base_url = urlparse(url)
    base_url = "%s://%s%s" % (base_url.scheme, base_url.hostname, base_url.path)

    profile = page.get_element_by_id('CompanyProfile')
    sector = profile.xpath('//*/h3[text()="Sector"]')[0].getparent().text_content().replace("Sector", "")
    industry = profile.xpath('//*/h3[text()="Industry"]')[0].getparent().text_content().replace("Industry", "")
    if not company_output.company.sector:
        company_output.company.sector = industry
        company_output.company.save()
        FadeRates.objects.get_or_create(yahoo_industry=sector)
    isin = page.get_element_by_id('Col0Isin').text_content()

    page.make_links_absolute(base_url)
    fin_lnk_is = page.get_element_by_id('LnkPage10').attrib['href']
    fin_lnk_cf = fin_lnk_is.replace('vw=is', 'vw=cf')
    fin_lnk_bs = fin_lnk_is.replace('vw=is', 'vw=bs')

    page = _morningstar_page_load(fin_lnk_is)
    table = page.get_element_by_id('FinancialsIncomeStatement')
    years = [y.text_content() for y in table.cssselect('thead th')][1:]

    sales = _morningstar_parse_table_by_header(table, 'Revenue')
    research_and_development = _morningstar_parse_table_by_header(table, 'Research and development')
    operating_income = _morningstar_parse_table_by_header(table, 'Operating income before interest and taxes')
    net_income = _morningstar_parse_table_by_header(table, 'Net Income')
    depreciation_and_amortisation = _morningstar_parse_table_by_header(table, 'Depreciation and amortization')

    page = _morningstar_page_load(fin_lnk_bs)
    table = page.get_element_by_id('FinancialsBalanceSheet')

    net_ppe = [i.text_content() for i in table.xpath('//*/abbr[text()="%s"]' % 'Not property, plant and equipment')[
                                             0].getparent().getparent().getchildren()[1:]]
    intangible_assets = _morningstar_parse_table_by_header(table, 'Intangibles')
    total_assets = _morningstar_parse_table_by_header(table, 'Total Assets')
    total_stockholder_equity = _morningstar_parse_table_by_header(table, 'Total stockholders\' equity')

    currency_list = page.find_class('disclaimer')[0].text_content().replace('.', '').replace('\n', '').replace('\t',
                                                                                                               '').split(
        ' ')

    result_per_years = {yi: {
        'sales': sales_i,
        'research_and_development': research_and_development_i,
        'operating_income': operating_income_i,
        'net_income': net_income_i,
        'depreciation_and_amortisation': depreciation_and_amortisation_i,
        'net_ppe': net_ppe_i,
        'intangible_assets': intangible_assets_i,
        'total_assets': total_assets_i,
        'total_stockholder_equity': total_stockholder_equity_i,
        'morningstar_currency': currency_list[5],
        'morningstar_currency_units': currency_list[2],
    } for (
                            yi,
                            sales_i,
                            research_and_development_i,
                            operating_income_i,
                            net_income_i,
                            depreciation_and_amortisation_i,
                            net_ppe_i,
                            intangible_assets_i,
                            total_assets_i,
                            total_stockholder_equity_i,
                        ) in map(lambda yi,
                                        sales_i,
                                        research_and_development_i,
                                        operating_income_i,
                                        net_income_i,
                                        depreciation_and_amortisation_i,
                                        net_ppe_i,
                                        intangible_assets_i,
                                        total_assets_i,
                                        total_stockholder_equity_i,: (yi,
                                                                      sales_i,
                                                                      research_and_development_i,
                                                                      operating_income_i,
                                                                      net_income_i,
                                                                      depreciation_and_amortisation_i,
                                                                      net_ppe_i,
                                                                      intangible_assets_i,
                                                                      total_assets_i,
                                                                      total_stockholder_equity_i,), years,
                                 sales,
                                 research_and_development,
                                 operating_income,
                                 net_income,
                                 depreciation_and_amortisation,
                                 net_ppe,
                                 intangible_assets,
                                 total_assets,
                                 total_stockholder_equity, )}
    company_output_pre_res = {
        'morningstar_name': company_name,
        'morningstar_website': None,
        'morningstar_sector': sector,
        'morningstar_industry': industry,
        'ISIN': isin
    }
    company_fin_outputs = {}

    for year, data in result_per_years.iteritems():
        year = int(year)
        company_fin_outputs[year]={}
        company_fin_outputs[year]['rate']=get_rates(currency_list[5], year)
        company_fin_outputs[year]['com_id'] = company_output.company.companyid
        for attr, value in data.iteritems():
            company_fin_outputs[year][attr] = value
    return company_output_pre_res, company_fin_outputs