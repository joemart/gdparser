from django.conf import settings
import logging
import datetime
import time
from selenium import webdriver
from base.modules.captcha_processor import resolve_captcha
from selenium.common.exceptions import NoSuchElementException
from complex.models import PatentCounter

if settings.USE_VIRTUAL_DISPLAY:  # use virtual display only on production
    try:
        from pyvirtualdisplay import Display
    except:
        pass
logger = logging.getLogger()

CATEGORIES = ['', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
START_YEAR = 2000


def patent_count_parse(task):
    url = "https://patentscope.wipo.int/search/en/structuredSearch.jsf"
    if settings.USE_VIRTUAL_DISPLAY:
        display = Display(visible=0, size=(800, 600))
        display.start()
    driver = webdriver.Firefox()
    driver.implicitly_wait(10)
    driver.get(url)
    try:
        if driver.find_element_by_css_selector('#capForm\:fpSearchTabPanel img').is_displayed():
            task.set_status('counter - CAPTCHA detected. Try resolve.')
            if not resolve_captcha(driver, time_sleep=1, captcha_selector='#capForm\:fpSearchTabPanel img', captcha_input_selector="#capForm\:jcaptcha"):
                task.set_status('counter - Can\'t CAPTCHA resolve. Login canceled.')
                # logger.error('Can\'t CAPTCHA resolve. Login canceled.')
                raise Exception('counter - Can\'t CAPTCHA resolve. Login canceled.')
            driver.get(url)

    except NoSuchElementException:
        pass

    driver.find_element_by_id('structuredSearchForm:stucturedSearch6').send_keys(task.value)
    driver.find_element_by_id('structuredSearchForm:stucturedSearch9').send_keys('US')
    start_year = START_YEAR
    if task.last_counters_refresh:
        start_year = task.last_counters_refresh.year
    end_year = datetime.date.today().year
    for year in xrange(start_year,end_year+1):
        driver.find_element_by_id('structuredSearchForm:stucturedSearch3').clear()
        driver.find_element_by_id('structuredSearchForm:stucturedSearch3').send_keys('[01.01.%s TO 01.01.%s]' % (year, year+1))
        for category in CATEGORIES:
            driver.find_element_by_id('structuredSearchForm:stucturedSearch7').clear()
            driver.find_element_by_id('structuredSearchForm:stucturedSearch7').send_keys(category)
            time.sleep(3)
            count = int(driver.find_element_by_id('structuredSearchForm:outputCount').text)
            counter, created = PatentCounter.objects.get_or_create(company=task.company, task=task, year=year, category=category)
            counter.count = count
            counter.save()
    task.last_counters_refresh = datetime.date.today()
    task.save()
    driver.close()
