from datetime import datetime
from celery import signature
from base.modules.captcha_processor import resolve_captcha
from django.conf import settings
import time
import lxml.html as html
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from constance import config
import logging
from base.modules.glassdoor import check_login, process_review
from complex.models import GlassdoorPageRawResult

logger = logging.getLogger()
if settings.USE_VIRTUAL_DISPLAY:  # use virtual display only on production
    try:
        from pyvirtualdisplay import Display
    except:
        pass


def check_captcha(driver):
    try:
        if driver.find_element_by_css_selector('#recaptcha_response_field').is_displayed():
            if not resolve_captcha(driver):
                raise Exception('Can\'t CAPTCHA resolve. Login canceled.')

    except NoSuchElementException:
        pass


def glassdor_scraper(pages, for_update=False):
    pages.update(status='queued')
    if settings.USE_VIRTUAL_DISPLAY:
        display = Display(visible=0, size=(800, 600))
        display.start()
    driver = webdriver.Firefox()
    driver.implicitly_wait(10)
    base_url = "https://www.glassdoor.co.uk"
    driver.get(base_url + "/profile/login_input.htm")
    time.sleep(10)
    check_captcha(driver)
    driver.find_element_by_name("username").send_keys(config.G_LOGIN)
    driver.find_element_by_css_selector(
        "span.block-signin > div.value.passWrapper > input[name=\"password\"]").send_keys(config.G_PASS)
    time.sleep(15)
    driver.find_element_by_id("signInBtn").click()
    time.sleep(10)
    check_captcha(driver)
    check_login(driver, base_url)
    try:
        driver.find_element_by_css_selector('#AccountMenu')
    except NoSuchElementException:
        driver.close()
        logger.error('Failed sign in. Check Login & Password', exc_info=True)
        raise Exception('Failed sign in. Check Login & Password')
    for page in pages:
        page.status = 'process'
        page.save()

        max_page = page.link.replace(".htm", "_P10000.htm")
        driver.get(max_page)
        try:
            max_page_num = int(driver.find_element_by_css_selector('li.page.last').text)
        except NoSuchElementException:
            max_page_num = 1
        company_review_count = 0
        for i in xrange(1, max_page_num + 1):
            page.status = 'proc (%s/%s)' % (i, max_page_num)
            page_url = page.link.replace(".htm", "_P%s.htm" % i)
            if not max_page_num == 1:
                driver.get(page_url)
            html_tree = html.fromstring(driver.find_element_by_css_selector('body').get_attribute('innerHTML'))
            reviews = html_tree.find_class('hreview')
            if not len(reviews):
                check_captcha(driver)
                check_login(driver, page_url)
                html_tree = html.fromstring(driver.find_element_by_css_selector('body').get_attribute('innerHTML'))
                reviews = html_tree.find_class('hreview')
            gtdate_count = 0
            for review in reviews:
                res, stars = process_review(review)
                rewiev_time = datetime.strptime(res['time'], '%d %b %Y').date() if res['time'] != 'Featured Review' else None
                if for_update and rewiev_time and page.last_updated:
                    if rewiev_time < page.last_updated:
                        if gtdate_count == 0:
                            gtdate_count += 1
                        else:
                            page.status = 'complete'
                            page.last_updated = datetime.today()
                            page.completed = True
                            page.save()
                            signature('complex.tasks.page_extract_task').delay(page.pk, company_review_count)
                            driver.close()
                            return


                GlassdoorPageRawResult.objects.create(com_id=page.company.companyid,
                                                      page=page,
                                                      employee_review_date=rewiev_time,
                                                      employee_review_rating=stars,
                                                      employee_review_title=res['summary'],
                                                      employee_review_pros=res['pros'],
                                                      employee_review_cons=res['cons'],
                                                      employee_review_advice=res['advice'],
                )
                company_review_count += 1
        page.status = 'complete'
        page.last_updated = datetime.today()
        page.completed = True
        page.save()
        signature('complex.tasks.page_extract_task').delay(page.pk, company_review_count)
        #start analize

    driver.close()