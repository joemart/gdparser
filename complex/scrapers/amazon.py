if __name__ == '__main__':
    import os,sys
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "scraper.settings")
    import django
    django.setup()
import datetime
from lxml import html as html
import urllib2
import urllib
import cookielib
import logging
from selenium.common.exceptions import NoSuchElementException
from base.modules.captcha_processor import resolve_captcha
from selenium import webdriver
from django.conf import settings
from constance import config
if __name__ != '__main__' and settings.USE_VIRTUAL_DISPLAY:  # use virtual display only on production
    try:
        from pyvirtualdisplay import Display
    except:
        pass
if __name__ != '__main__':
    from complex.models import AmazonBrandRawOutput

logger = logging.getLogger()

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.151 Safari/535.19"


def check_captcha(page, url):
    try:
            page.get_element_by_id('captchacharacters')
            logger.warning('captcha')
            profile = webdriver.FirefoxProfile()
            profile.set_preference("general.useragent.override", USER_AGENT)
            if settings.USE_VIRTUAL_DISPLAY:
                display = Display(visible=0, size=(800, 600))
                display.start()
            driver = webdriver.Firefox(profile)
            logger.warning('ff started')
            for cookie in cj:
                driver.add_cookie({cookie.name: cookie.value})
            driver.implicitly_wait(10)
            try:
                driver.get(url)
                if driver.find_element_by_id('captchacharacters').is_displayed():
                    if not resolve_captcha(driver, time_sleep=1,captcha_selector='form .a-row.a-text-center img',
                                       captcha_input_selector="#captchacharacters"):
                        raise Exception('Can\'t CAPTCHA resolve. Amazon page.')
                    else:
                        logger.warning('captcha resolved')
                        cj.clear()
                        for cookie in driver.get_cookies():
                            ck = cookielib.Cookie(version=0, name=cookie['name'], value=cookie['value'],
                                          port=None, port_specified=False, domain='.amazon.com',
                                          domain_specified=False, domain_initial_dot=False, path='/',
                                          path_specified=True, secure=False, expires=None, discard=True, comment=None,
                                          comment_url=None, rest={'HttpOnly': None}, rfc2109=False)
                            cj.set_cookie(ck)
                        return 1
            except NoSuchElementException:
                logger.warning('no captcha for browser')
                return 0
            finally:
                driver.close()

    except:
        return None

def get_headers():
    return [
        ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
        ('Accept-Encoding', 'deflate'),
        ('Accept-Language', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'),
        ('Cache-Control', 'max-age=0'),
        ('Connection', 'keep-alive'),
        ('Content-Type', 'application/x-www-form-urlencoded'),
        ('Host', 'patentscope.wipo.int'),
        ('Origin', 'https://patentscope.wipo.int'),
        ('Referer', 'https://patentscope.wipo.int/search/en/structuredSearch.jsf'),
        ('User-Agent', USER_AGENT)
    ]


opener.addheaders = get_headers()


def get_rewiews_from_details(url):
    reviews = []
    max_page_num = 1

    for i in xrange(config.MAX_AMAZON_REVIEWS_PAGES):

        lnk = url[:url.find('ref=')].replace('/dp/', '/product-reviews/') + 'ref=cm_cr_pr_viewopt_sr?ie=UTF8&showViewpoints=1&sortBy=helpful&reviewerType=all_reviews&filterByStar=all_stars&pageNumber=%s' % i
        code = opener.open(lnk).read()
        page = html.fromstring(code)
        if check_captcha(page, lnk):
            page = html.fromstring(opener.open(lnk).read())  # reload
        if i == 1:
            try:
                max_page_num = int(page.cssselect('.a-disabled.page-ellipsis')[0].getnext().getchildren()[0].text.split(',')[0])
            except IndexError:
                pass
        stars = [int(round(float(e.text[:3]))) for e in page.cssselect('.a-icon-alt')]
        ids = [e.attrib['id'] for e in page.cssselect('.a-section.review')]
        dates = [datetime.datetime.strptime(e.text,'on %B %d, %Y').date() for e in page.cssselect('.review-date')]
        reviews.extend(zip(stars, dates, ids))
        if i == max_page_num:
            break
    return reviews


def parse_amazon(brand, task, for_update=False):
    # lnk = 'http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias'+'%3D'+'aps&%s' % urllib.urlencode({'field-keywords':brand})
    lnk="http://www.amazon.com/s/ref=sr_nr_p_89_0?fst=as%3Aoff&rh=i%3Aaps%2Ck%3A"+urllib.quote_plus(brand.lower())+"%2Cp_89%3A"+urllib.quote_plus(brand)+"&keywords="+urllib.quote_plus(brand)+"&ie=UTF8"
    print lnk
    code = opener.open(lnk).read()
    page = html.fromstring(code)
    if check_captcha(page, lnk):
        page = html.fromstring(opener.open(lnk).read())  # reload
    next_page_link = ''
    pages_counter = 0
    while True:
        page = html.fromstring(code)
        check_captcha(page, next_page_link)
        details = page.cssselect('a.s-access-detail-page')
        details_links = [(lnk.attrib['href'], lnk.attrib['title'],) for lnk in details if lnk.attrib['href'].startswith('http://www.amazon.com')]
        for lnk, title in details_links:
            for group in get_rewiews_from_details(lnk):
                if __name__ != '__main__':
                    if for_update and AmazonBrandRawOutput.objects.filter(amazon_id=group[2]).exists():
                        continue
                    AmazonBrandRawOutput.objects.create(task=task,
                                                        amazon_brand=brand,
                                                        amazon_product_name=title,
                                                        amazon_review_date=group[1],
                                                        amazon_review_stars=group[0],
                                                        amazon_id=group[2]
                    )
                else:
                    print brand, title, group[1], group[0], group[2]
        page.make_links_absolute('http://www.amazon.com')
        try:
            next_page_link = page.cssselect('.pagnCur')[0].getnext().getchildren()[0].attrib['href']
        except :
            break
        code = opener.open(next_page_link).read()
        pages_counter += 1
        if pages_counter >= config.MAX_AMAZON_SEARCH_PAGES:
            break
    task.completed = True
    task.last_updated = datetime.datetime.today()
    task.save()

if __name__ == '__main__':
    parse_amazon('Apple', None)