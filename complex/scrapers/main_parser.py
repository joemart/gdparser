from complex.tasks import complex_parse, patent_parsing, glassdoor_scrape, amazon_parse, universal_parse, \
    patent_count_parsing


class Parser():
    def __init__(self, company):
        self.company = company
        self.company_output = company.companyoutput

    def start(self, update_financial_only=False):
        complex_parse.delay(self.company.yahoo_page, self.company.morningstar_page, self.company.morningstar_tools_page, self.company_output.pk, self.company.pk)
        if not update_financial_only:
            patent_parsing.delay(self.company.pk)
            glassdoor_scrape.delay(self.company.pk)
            amazon_parse.delay(self.company.pk)
            universal_parse.delay(self.company.pk)
            patent_count_parsing.delay(self.company.pk)

    def update(self):
        complex_parse.delay(self.company.yahoo_page, self.company.morningstar_page, self.company_output.pk, self.company.pk)
        glassdoor_scrape.delay(self.company.pk, for_update=True)
        amazon_parse.delay(self.company.pk, for_update=True)
        universal_parse.delay(self.company.pk, for_update=True)
        patent_count_parsing.delay(self.company.pk)