# -*- encoding: utf8 -*-
import requests
import json
from lxml import html
from lxml.html.clean import clean_html
import logging
from complex.models import CompanyOutput, FadeRates, CompanyOutputFinancial
from complex.scrapers.currency_rates import get_rates

logger = logging.getLogger()

n_url = "http://www.morningstar.com/stocks/XNAS/MSON/quote.html"


def _get_val_by_cont(page, container_id):
    try:
        return [str(page.cssselect('#%s #%s' % (container_id, y_id))[0].text_content().replace('(', '-').replace(')',
                                                                                                                 '').replace(
            u'—', '-')) for y_id in
                ('Y_1', 'Y_2', 'Y_3', 'Y_4', 'Y_5')]
    except:
        return [None for i in xrange(5)]


def morning_base_parse(url, company_output_id=None):
    res = requests.get(url)
    if res.status_code == 200:
        start_p = res.text.find('{"securityName"')
        stop_p = res.text.find('}', start_p) + 1
        params = json.loads(res.text[start_p:stop_p])
        exchangeId = params['exchangeId']
        name = params['securityName']
        ISIN = params['ISIN']
        ticker = params['ticker']
        regionId = params['regionId']
    else:
        logger.error('fail when loading Morningstar api')
        return
    profile_url = \
        'http://quotes.morningstar.com/stock/c-company-profile?&t=%s:%s&region=%s&culture=%s&version=RET&cur=&test=QuoteiFrame' % \
        (exchangeId, ticker, regionId.lower(), 'en-US')

    res = requests.get(profile_url)
    if res.status_code == 200:
        page = html.fromstring(clean_html(res.text))
        data = page.cssselect('.gr_text7')
        industry = data[1].text.strip()  # 0-sector
        sector = data[0].text.strip()
        employers = data[3].text.strip()
    else:
        logger.error('fail when loading Morningstar api')
        return
    site_url = 'http://financials.morningstar.com/company-profile/component.action?component=ContactInfo&t=%s:%s&region=%s&culture=%s' % \
               (exchangeId, ticker, regionId.lower(), 'en-US')
    res = requests.get(site_url)
    link = None
    if res.status_code == 200:
        try:
            page = html.fromstring(res.text)
            lnk = page.cssselect('a')[-1].attrib['href']
            if lnk.startswith('http'):
                link = lnk
        except:
            pass
    is_url = 'http://financials.morningstar.com/ajax/ReportProcess4HtmlAjax.html?&t=%s:%s&region=%s&culture=%s&cur=&reportType=%s&period=12&dataType=A&order=asc&columnYear=5&curYearPart=1st5year&rounding=3&view=raw&r=838210&callback=' % \
             (exchangeId, ticker, regionId.lower(), 'en-US', 'is')  # is bs cf
    bs_url = 'http://financials.morningstar.com/ajax/ReportProcess4HtmlAjax.html?&t=%s:%s&region=%s&culture=%s&cur=&reportType=%s&period=12&dataType=A&order=asc&columnYear=5&curYearPart=1st5year&rounding=3&view=raw&r=838210&callback=' % \
             (exchangeId, ticker, regionId.lower(), 'en-US', 'bs')  # is bs cf
    cf_url = 'http://financials.morningstar.com/ajax/ReportProcess4HtmlAjax.html?&t=%s:%s&region=%s&culture=%s&cur=&reportType=%s&period=12&dataType=A&order=asc&columnYear=5&curYearPart=1st5year&rounding=3&view=raw&r=838210&callback=' % \
             (exchangeId, ticker, regionId.lower(), 'en-US', 'cf')  # is bs cf

    res = requests.get(is_url)
    if res.status_code == 200:
        page = html.fromstring(res.json()['result'])
        units = page.get_element_by_id('unitsAndFiscalYear')
        currensy_units = str(res.json()['i18nWords']['localChartUnit'])
        currensy = units.attrib['currency']
        years = _get_val_by_cont(page, 'Year')
        sales = _get_val_by_cont(page, 'data_i1')
        net_income = _get_val_by_cont(page, 'data_i80')
        ebitda = _get_val_by_cont(page, 'data_i90')
        rnd = _get_val_by_cont(page, 'data_i11')
    else:
        logger.error('fail when loading Morningstar api')
        return
    res = requests.get(bs_url)
    if res.status_code == 200:
        page = html.fromstring(res.json()['result'])
        total_assets = _get_val_by_cont(page, 'data_tts1')
        intangible_assets = _get_val_by_cont(page, 'data_i13')
        total_stockholder_equity = _get_val_by_cont(page, 'data_ttg8')
    else:
        logger.error('fail when loading Morningstar api')
        return
    res = requests.get(cf_url)
    if res.status_code == 200:
        page = html.fromstring(res.json()['result'])
        depreciation_and_amortisation = _get_val_by_cont(page, 'data_i2')
        net_ppe = _get_val_by_cont(page, 'data_i32')
    else:
        logger.error('fail when loading Morningstar api')
        return

    years = [int(y.split('-')[0]) for y in years]
    res = {y: v for y, v in zip(years, zip(sales, rnd, ebitda, net_income, depreciation_and_amortisation, net_ppe,
                                           intangible_assets, total_assets, total_stockholder_equity))}
    if not company_output_id:
        print(currensy)
        return {'res': res, 'currensy': currensy, 'currensy_units': currensy_units, 'ISIN': ISIN, 'name': name,
                'sector': sector, 'employers': employers, 'industry': industry}

    company_output = CompanyOutput.objects.get(pk=company_output_id)
    company_output.company.sector = industry
    company_output.company.save()
    FadeRates.objects.get_or_create(yahoo_industry=industry)

    company_output_pre_res = {
        'morningstar_name': name,
        'morningstar_website': link,
        'morningstar_sector': sector,
        'morningstar_industry': industry,
        'ISIN': ISIN
    }
    company_fin_outputs = {}

    for y in res:
        company_fin_outputs[y] = {}
        company_fin_outputs[y]['rate'] = get_rates(currensy, y)
        company_fin_outputs[y]['com_id'] = company_output.company.companyid
        for i, param in enumerate(
                ['sales', 'research_and_development', 'operating_income', 'net_income', 'depreciation_and_amortisation',
                 'net_ppe',
                 'intangible_assets', 'total_assets', 'total_stockholder_equity']):
            company_fin_outputs[y][param] = res[y][i]
        company_fin_outputs[y]['morningstar_currency'] = currensy
        company_fin_outputs[y]['morningstar_currency_units'] = currensy_units
    return company_output_pre_res, company_fin_outputs


if __name__ == '__main__':
    print morning_base_parse(n_url)
