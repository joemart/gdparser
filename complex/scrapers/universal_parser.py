import datetime
from base.modules.extraction.alchemy_interface import AlchemyAPI
from complex.models import Keyword, UniversalPageCalcResult

__author__ = 'orion'
import cookielib
import urllib2
from lxml import html
from base.modules.extracktor import extract
import logging
logger = logging.getLogger()

def extract_kw(big_blob, the_topics):
    if len(big_blob) > 10:  # at less few words
        try:
            keywords_list = extract(big_blob, the_topics)
        except Exception as e:
            logger.info('Keywords extraction error', exc_info=True)
            raise e
        return keywords_list
    return []

cj_patents = cookielib.CookieJar()

opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj_patents))

opener.addheaders = [
    ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
    ('Accept-Encoding', 'deflate'),
    ('Accept-Language', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'),
    ('Cache-Control', 'max-age=0'),
    ('Connection', 'keep-alive'),
    ('Content-Type', 'application/x-www-form-urlencoded'),
    ('Host', 'patentscope.wipo.int'),
    ('Origin', 'https://patentscope.wipo.int'),
    ('Referer', 'https://patentscope.wipo.int/search/en/structuredSearch.jsf'),
    ('User-Agent',
     'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
]


def u_parse(task, comid, url_mask, link_xpath, content_xpath, header_path=None, max_page=100, date_path=None,
            for_update=False):
    current_page_num = 0
    parsed_pages_links = []

    while True:
        task.status = 'parsing page %s' % current_page_num
        task.save()
        current_page_num += 1
        page = html.fromstring(opener.open(url_mask.replace('PAGENUM', str(current_page_num))).read())

        article_links = page.xpath(link_xpath)
        for article_link in article_links:
            if article_link.attrib['href'] in parsed_pages_links:
                continue
            else:
                parsed_pages_links.append(article_link.attrib['href'])
            article_link.make_links_absolute(url_mask)
            if for_update and UniversalPageCalcResult.objects.filter(page_url=article_link.attrib['href']).exists():
                continue
            try:
                article_page = html.fromstring(opener.open(article_link.attrib['href']).read())
            except:
                continue
            text_blob = ' '.join([i.text_content() for i in article_page.xpath(content_xpath)])
            header = None
            date = None
            if header_path:
                try:
                    header = article_page.xpath(header_path)[0].text_content()
                except IndexError:
                    pass
            if date_path:
                try:
                    date = article_page.xpath(date_path)[0].text_content()
                except IndexError:
                    pass
            if len(text_blob) < 100:
                continue
            else:
                alchemy = AlchemyAPI()
                sentiment = alchemy.get_sentiment(text_blob.encode('utf8', 'ignore'))
                UniversalPageCalcResult.objects.create(
                        company=task.company,
                        com_id=comid,
                        header=header.strip()[:255],
                        date=date,
                        page_url=article_link.attrib['href'],
                        # keyword=keyword,
                        # value=value,
                        sentiment_type=sentiment['type'],
                        sentiment_score=float(sentiment['score']) if 'score' in sentiment else None
                    )
        if len(article_links) == 0 or (max_page and current_page_num >= max_page):
            break
    task.status = 'complete'
    task.last_updated = datetime.datetime.today()
    task.save()

