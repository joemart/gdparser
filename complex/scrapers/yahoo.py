from complex.scrapers.currency_rates import get_rates

__author__ = 'orion'
from complex.models import Company, CompanyOutput, ExchangeRates, CompanyOutputFinancial, FadeRates
import urllib2
from urlparse import urlparse
from lxml.html.clean import clean_html
import lxml.html as html


def _get_page_by_url(url):
    return html.fromstring(clean_html(urllib2.urlopen(url).read()))


def _separate_currensy(currensy):
    return ''.join([l for l in currensy if (l in [str(i) for i in xrange(10)] or l == '.')]), ''.join(
        [l for l in currensy if (l not in [str(i) for i in xrange(10)] and l != '.')])


def yahoo_parse(url, company_output_id):
    company_output = CompanyOutput.objects.get(pk=company_output_id)
    base_url = urlparse(url)
    base_url = "%s://%s/" % (base_url.scheme, base_url.hostname)
    page = urllib2.urlopen(url)
    code = clean_html(page.read())
    pos1 = code.find('Market Cap:') + 16
    market_cap = _separate_currensy(
        html.fromstring(code[pos1:pos1 + code[pos1:].find('</td>') + 6]).text_content())
    page = html.fromstring(code)
    page.make_links_absolute(base_url)
    page.get_element_by_id('yfi_key_stats')
    forward_pe = page.get_element_by_id('yfi_key_stats').find_class('yfnc_tabledata1')[0].text_content()
    try:
        estimates_url = page.xpath('//*/a[text()="Analyst Estimates"]')[0].attrib['href']
    except IndexError:
        estimates_url = None
    profile_url = page.xpath('//*/a[text()="Profile"]')[0].attrib['href']
    if estimates_url:
        page = _get_page_by_url(estimates_url)
        currensy = page.get_element_by_id('yfncsumtab').getchildren()[-1].text_content().replace("Currency in ",
                                                                                                 "").replace(".", "")

        header_row = page.xpath('//*/strong[text()="Revenue Est"]')[0].getparent().getparent()
        years = [el.text_content()[-4:] for el in header_row.getchildren()[-2:]]
        year_sales = [el.text_content() for el in header_row.getnext().getchildren()[-2:]]
        sales = map(lambda x, y: (x, _separate_currensy(y)), years, year_sales)
    page = _get_page_by_url(profile_url)
    yahoo_name = page.cssselect('.yfnc_modtitlew1 b')[0].text_content()
    yahoo_website = ''
    for a in page.cssselect('.yfnc_modtitlew1 a'):
        if not 'yahoo.com' in a.attrib['href']:
            yahoo_website = a.attrib['href']
    table_els = page.cssselect('.yfnc_tablehead1')
    details = {}
    for el in table_els:
        if el.text_content() in ['Sector:', 'Industry:', 'Full Time Employees:']:
            details[el.text_content()] = el.getnext().text_content()
    summary = page.xpath('//*/span[text()="Business Summary"]')[
        0].getparent().getparent().getparent().getnext().text_content()
    company_output.yahoo_name = yahoo_name
    company_output.yahoo_website = yahoo_website
    company_output.yahoo_sector = details['Sector:']

    company_output.yahoo_industry = details['Industry:']
    # if not company_output.company.sector:
    #     company_output.company.sector = company_output.yahoo_industry
    #     company_output.company.save()
    # FadeRates.objects.get_or_create(yahoo_industry=company_output.yahoo_industry)

    company_output.employees = details['Full Time Employees:']
    company_output.business_description = summary
    if estimates_url:
        company_output.market_cap_currency = currensy

    company_output.forward_pe = forward_pe
    company_output.market_cap = market_cap[0]
    company_output.market_cap_units = market_cap[1]
    company_output.save()
    if estimates_url:
        for sale in sales:
            company_output_fin, created = CompanyOutputFinancial.objects.get_or_create(company=company_output.company,
                                                                                       year=sale[0])
            if not company_output_fin.sales:
                company_output_fin.sales = sale[1][0]
            company_output_fin.yahoo_currency = currensy
            company_output_fin.yahoo_currency_units = sale[1][1]
            company_output_fin.com_id = company_output.company.companyid
            company_output_fin.rate = get_rates(currensy, sale[0])
            company_output_fin.save()