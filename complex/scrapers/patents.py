import cookielib
import urllib2
from time import sleep
import re
from StringIO import StringIO
import gzip
from constance import config
from datetime import datetime

from selenium import webdriver
import lxml.html as html
from selenium.common.exceptions import NoSuchElementException
from base.modules.captcha_processor import resolve_captcha

from complex.models import PatentOutput
from celery import signature
import logging
from django.conf import settings
if settings.USE_VIRTUAL_DISPLAY:  # use virtual display only on production
    try:
        from pyvirtualdisplay import Display
    except:
        pass

logger = logging.getLogger()

cj_patents = cookielib.CookieJar()

opener_patents = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj_patents))

opener_patents.addheaders = [
    ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
    ('Accept-Encoding', 'gzip, deflate'),
    ('Accept-Language', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'),
    ('Cache-Control', 'max-age=0'),
    ('Connection', 'keep-alive'),
    ('Content-Type', 'application/x-www-form-urlencoded'),
    ('Host', 'patentscope.wipo.int'),
    ('Origin', 'https://patentscope.wipo.int'),
    ('Referer', 'https://patentscope.wipo.int/search/en/structuredSearch.jsf'),
    ('User-Agent',
     'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
]


def get_page_code_gzip(page):
    if page.info().get('Content-Encoding') == 'gzip':
        buf = StringIO(page.read())
        f = gzip.GzipFile(fileobj=buf)
        data = f.read()
        buf.close()
    else:
        data = page.read()
    return data


def _parse_main_patent_page(page, company):

    page.make_links_absolute('https://patentscope.wipo.int/search/en/')
    table_body = page.get_element_by_id('resultTable:tb')
    trs = table_body.cssselect('.rf-dt-r')
    total_res = len(trs) / 3
    i = 0

    while i < total_res:
        lnk = table_body.get_element_by_id('resultTable:%s:resultListTableColumnLink' % i)
        href = lnk.attrib['href']
        pub_num = lnk.cssselect('span')[0].text_content().strip()
        try:
            title = \
                table_body.get_element_by_id('resultTable:%s:resultListTableColumnTitle' % i).cssselect('.trans-section')[
                    0].text_content().strip()[:255]
        except:
            title = ''
        IPC = table_body.get_element_by_id('resultTable:%s:resultListTableColumnIPC' % i).cssselect('span')[
            0].text_content().strip()
        try:
            summary = \
            table_body.get_element_by_id('resultTable:%s:resultListTableColumnAbstract' % i).cssselect(
                '.trans-section')[
                0].text_content().strip()
        except:
            summary = ''
        i += 1
        patent, created = PatentOutput.objects.get_or_create(publication_number=pub_num, company=company)
        if created:
            patent.company = company
            patent.ticker = company.companyid
            patent.publication_link = href
            patent.title = title
            patent.summary = summary
            l_list = IPC.split(' ')
            patent.l1 = l_list[0][0]
            patent.l2 = l_list[0][1:2]
            patent.l3 = l_list[0][3]
            patent.l4 = l_list[1].split('/')[0]
            patent.l5 = l_list[1].split('/')[1]
            patent.save()
            signature('complex.tasks.patent_detail_parse').delay(patent.pk)


def patent_parse(task, company):
    url = "https://patentscope.wipo.int/search/en/structuredSearch.jsf"
    if settings.USE_VIRTUAL_DISPLAY:
        display = Display(visible=0, size=(800, 600))
        display.start()
    task.set_status('starting ff')
    driver = webdriver.Firefox()
    driver.implicitly_wait(10)
    task.set_status('ff started')
    driver.get(url)
    task.set_status('main page loaded')
    try:
        if driver.find_element_by_css_selector('#capForm\:fpSearchTabPanel img').is_displayed():
            task.set_status('CAPTCHA detected. Try resolve.')
            if not resolve_captcha(driver, time_sleep=1, captcha_selector='#capForm\:fpSearchTabPanel img', captcha_input_selector="#capForm\:jcaptcha"):
                task.set_status('Can\'t CAPTCHA resolve. Login canceled.')
                # logger.error('Can\'t CAPTCHA resolve. Login canceled.')
                raise Exception('Can\'t CAPTCHA resolve. Login canceled.')
            driver.get(url)

    except NoSuchElementException:
        pass
    driver.find_element_by_id('structuredSearchForm:stucturedSearch3').send_keys('[01.01.2000 TO 01.01.2025]')
    driver.find_element_by_id('structuredSearchForm:stucturedSearch6').send_keys(task.value)
    driver.find_element_by_id('structuredSearchForm:stucturedSearch9').send_keys('US')
    driver.find_element_by_id('structuredSearchForm:commandStructuredSearch').click()
    sleep(5)
    task.set_status('search page loaded')
    select = driver.find_element_by_id('resultListTableForm:lengthOption')
    all_options = select.find_elements_by_tag_name("option")
    for option in all_options:
        if option.get_attribute("value") == '200':
            option.click()
            break
    sleep(5)
    task.set_status('results count set to 200')
    res_count = int(driver.find_element_by_id('resultListForm:resultPanel0').find_elements_by_css_selector('b')[1].text.replace(',',''))
    task.results_count = res_count
    task.save()

    while True:
        current_url = driver.current_url
        task.set_status('parsing %s'%current_url)
        page = html.fromstring(driver.find_element_by_css_selector('body').get_attribute('innerHTML'))
        try:
            pages_counter = page.get_element_by_id('resultListForm:goToPage')
        except:
            try:
                if driver.find_element_by_css_selector('#capForm\:fpSearchTabPanel img').is_displayed():
                    task.set_status('CAPTCHA detected. Try resolve.')
                    if not resolve_captcha(driver, time_sleep=1, captcha_selector='#capForm\:fpSearchTabPanel img', captcha_input_selector="#capForm\:jcaptcha"):
                        task.set_status('Can\'t CAPTCHA resolve. Login canceled.')
                        # logger.error('Can\'t CAPTCHA resolve. Login canceled.')
                        raise Exception('Can\'t CAPTCHA resolve. Login canceled.')

                    driver.get(current_url)

            except NoSuchElementException:
                pass
            sleep(30)
            driver.get(current_url)
            continue
        current_page = int(pages_counter.attrib['value'])
        task.set_status('parse page %s'%current_page)
        total_pages = int(re.search('\d+', pages_counter.getparent().text_content()).group(0))
        try:
            _parse_main_patent_page(page, company)
        except Exception as e:
            logger.warning('Failed parse patents list page', exc_info=True)

        if current_page == total_pages or current_page == config.MAX_PATENTS_PAGES:
            task.set_status('all pages scraped %s %s'%(current_page, total_pages))
            task.last_updated = datetime.today()
            task.save()
            break
        else:
            next_btn = driver.find_elements_by_css_selector('.dr-dscr-button.rich-datascr-button')[1]
            next_btn.click()

    driver.close()
    # signature('complex.tasks.alternate_detail_parsing_task').delay(company.pk)
    # alternate_detail_parsing_task(company.pk)
