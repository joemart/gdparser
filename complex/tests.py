from django.test import TestCase
from scrapers.morningstar2 import morning_base_parse
# Create your tests here.


class AnimalTestCase(TestCase):

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        url = 'http://www.morningstar.com/stocks/XSWX/ROG/quote.html'
        res = morning_base_parse(url)

        self.assertEqual(len(res['res']), 5)
        self.assertEqual(cat.speak(), 'The cat says "meow"')