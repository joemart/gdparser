import datetime
from calculations.glassdoor_frequency_calculator import calculate_freq
from complex.keywords_extracktor import page_extract
from complex.models import PatentTask, Company, GlassdoorPage, AmazonBrandTask, AmazonBrandRawOutput, UniversalPageTask
from complex.scrapers.amazon import parse_amazon
from complex.scrapers.glassdoor import glassdor_scraper
from complex.scrapers.morningstar_both import morning_base_parse
from complex.scrapers.patents import patent_parse
from complex.scrapers.patent_detail import parse_detail_patent_page, alternate_detail_parsing
from complex.scrapers.patents_count import patent_count_parse
from complex.scrapers.universal_parser import u_parse
from complex.scrapers.yahoo import yahoo_parse
from celery import task
from celery.signals import worker_process_init
import logging

logger = logging.getLogger()

@worker_process_init.connect
def configure_workers(*args, **kwargs):
    import django
    django.setup()


@task
def complex_parse(yahoo_page, morningstar_page, morningstar_tools_page, company_output_id, company_id):
    if yahoo_page:
        try:
            yahoo_parse(yahoo_page, company_output_id)  # for celery serialization
        except Exception as e:
            logger.warning('Yahoo data missed', exc_info=True)
    morning_base_parse(morningstar_page, morningstar_tools_page, company_output_id)


@task
def patent_parsing(company_id, patent_id=None):
    company = Company.objects.get(pk=company_id)
    patents_tasks = PatentTask.objects.filter(pk=patent_id) if patent_id else PatentTask.objects.filter(company=company)
    for tsk in patents_tasks:
        patent_parse(tsk, company)


@task
def patent_count_parsing(company_id, patent_id=None):
    print('staaaart')
    company = Company.objects.get(pk=company_id)
    patents_tasks = PatentTask.objects.filter(pk=patent_id) if patent_id else PatentTask.objects.filter(company=company)
    for tsk in patents_tasks:
        patent_count_parse(tsk)


@task#(rate_limit='25/m')
def patent_detail_parse(patent_id):
    parse_detail_patent_page(patent_id)


@task
def glassdoor_scrape(company_id, page_id=None, for_update=False):
    company = Company.objects.get(pk=company_id)
    pages = GlassdoorPage.objects.filter(pk=page_id) if page_id else GlassdoorPage.objects.filter(company=company)
    if pages.exists():
        glassdor_scraper(pages, for_update)


@task
def page_extract_task(page_id, comments_count):
    page_extract(page_id, comments_count)


@task
def amazon_parse(company_id, a_task_id=None, for_update=False):
    company = Company.objects.get(pk=company_id)
    a_tasks = AmazonBrandTask.objects.filter(pk=a_task_id) if a_task_id else AmazonBrandTask.objects.filter(company=company)
    for task in a_tasks:
        if for_update:
            task.completed = False
            task.save()
        parse_amazon(task.value, task, for_update)
        task.completed = True
        task.last_updated = datetime.datetime.today()
        task.save()


@task
def universal_parse(company_id, u_task_id=None, for_update=False):
    company = Company.objects.get(pk=company_id)
    u_tasks = UniversalPageTask.objects.filter(pk=u_task_id) if u_task_id else UniversalPageTask.objects.filter(company=company)
    if not for_update:
        u_tasks = u_tasks.filter(status='wait')
    for tsk in u_tasks:
        u_parse(tsk,
                company.companyid,
                tsk.pagination_mask,
                tsk.rule.article_link_xpath,
                tsk.rule.text_extract_xpath,
                tsk.rule.article_header_xpath,
                tsk.max_pages_count,
                tsk.rule.article_date_xpath,
                for_update
                )

@task#(rate_limit='25/m')
def glassdor_freq(company_id):
    company = Company.objects.get(pk=company_id)
    calculate_freq(company)