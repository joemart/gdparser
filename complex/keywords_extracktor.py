from calculations.glassdoor_frequency_calculator import calculate_freq
from complex.models import GlassdoorPage, GlassdoorPageRawResult, Keyword, GlassdoorPageCalcResult
import logging
logger = logging.getLogger()
from base.modules.extracktor import extract


def extract_kw(big_blob, the_topics):
    if len(big_blob) > 10:  # at less few words
        try:
            keywords_list = extract(big_blob, the_topics)
        except Exception as e:
            logger.error('Keywords extraction error', exc_info=True)
            raise e
        return keywords_list
    return []


def page_extract(page_id, comments_count):
    page = GlassdoorPage.objects.get(pk=page_id)
    comments = GlassdoorPageRawResult.objects.filter(page=page)
    keywords = [kw.value for kw in Keyword.objects.all()]
    if not len(keywords):
        raise Exception('No keywords set')
    big_blob = ''.join(
        [','.join([r.employee_review_title, r.employee_review_pros, r.employee_review_cons, r.employee_review_advice])
         for r in comments])
    for keyword, value in zip(*(iter(extract_kw(big_blob, keywords)),) * 2):
        gp, created = GlassdoorPageCalcResult.objects.get_or_create(page=page, com_id=page.company.companyid, keyword=keyword)
        gp.value = value
        gp.comments_count = GlassdoorPageRawResult.objects.filter(page=page, com_id=page.company.companyid).count()
        gp.save()
    calculate_freq(page.company)