__author__ = 'orion'
from django.db.models.signals import pre_save, post_save, pre_delete, post_delete
from django.dispatch import receiver
from models import Company

@receiver(post_save, sender=Company)
def start_company_parse(sender, instance, created, **kwargs):
    return 0