# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0004_auto_20150708_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='universalpagecalcresult',
            name='page_url',
            field=models.TextField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='universalpagetask',
            name='status',
            field=models.CharField(default=b'wait', max_length=63),
            preserve_default=True,
        ),
    ]
