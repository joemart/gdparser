# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0005_auto_20150708_1657'),
    ]

    operations = [
        migrations.AddField(
            model_name='universalpagetask',
            name='last_updated',
            field=models.DateField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='amazonbrandrawoutput',
            name='amazon_brand',
            field=models.CharField(help_text=b'Amazon brand name. Important! case-sensitive! Example: "LG" not "Lg" or "lg"', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='universalpagetask',
            name='status',
            field=models.CharField(default=b'wait', max_length=63, editable=False),
            preserve_default=True,
        ),
    ]
