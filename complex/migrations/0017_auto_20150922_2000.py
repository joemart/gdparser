# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0016_glassdorkeyworddistanceinput_glassdorkeywordoutput_glassdorkeywordstopinput'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='glassdorkeywordoutput',
            options={'ordering': ('-count',)},
        ),
        migrations.AlterField(
            model_name='glassdorkeyworddistanceinput',
            name='keyword_target',
            field=models.CharField(max_length=31, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='glassdorkeywordoutput',
            name='value',
            field=models.CharField(max_length=31, db_index=True),
            preserve_default=True,
        ),
    ]
