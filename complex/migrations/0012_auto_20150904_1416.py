# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0011_patentcounter_task'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patenttask',
            name='last_counters_refresh',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
