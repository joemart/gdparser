# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0007_auto_20150709_2014'),
    ]

    operations = [
        migrations.AddField(
            model_name='patenttask',
            name='last_updated',
            field=models.DateField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='patenttask',
            name='results_count',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
