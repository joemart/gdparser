# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0017_auto_20150922_2000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faderates',
            name='earnings_fade_period',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='faderates',
            name='randd_lag',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='faderates',
            name='randd_life',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
