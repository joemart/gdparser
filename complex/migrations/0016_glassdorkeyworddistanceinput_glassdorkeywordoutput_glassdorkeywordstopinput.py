# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0015_auto_20150918_1219'),
    ]

    operations = [
        migrations.CreateModel(
            name='GlassdorKeywordDistanceInput',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('keyword_target', models.CharField(max_length=31)),
                ('value', models.CharField(max_length=31)),
                ('distance', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GlassdorKeywordOutput',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=31)),
                ('count', models.IntegerField()),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GlassdorKeywordStopInput',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=31)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
