# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0002_patenttask_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='universalpagecalcresult',
            name='sentiment_score',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='universalpagecalcresult',
            name='sentiment_type',
            field=models.CharField(max_length=63, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='status',
            field=models.CharField(default=b'creating', max_length=255, editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='companyoutput',
            name='status',
            field=models.CharField(max_length=255, null=True, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='universalpagetask',
            name='status',
            field=models.CharField(default=b'wait', max_length=63, editable=False),
            preserve_default=True,
        ),
    ]
