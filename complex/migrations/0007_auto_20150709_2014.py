# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0006_auto_20150709_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='amazonbrandrawoutput',
            name='amazon_id',
            field=models.CharField(max_length=63, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='amazonbrandtask',
            name='last_updated',
            field=models.DateField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
