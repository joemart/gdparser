# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0009_glassdoorpage_last_updated'),
    ]

    operations = [
        migrations.CreateModel(
            name='PatentCounter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField()),
                ('category', models.CharField(max_length=15)),
                ('count', models.IntegerField()),
                ('updated', models.DateField(auto_now=True)),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='patenttask',
            name='last_counters_refresh',
            field=models.DateField(auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='glassdoorpage',
            name='last_updated',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
