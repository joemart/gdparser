# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0003_auto_20150708_0823'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='universalpagecalcresult',
            name='keyword',
        ),
        migrations.RemoveField(
            model_name='universalpagecalcresult',
            name='value',
        ),
    ]
