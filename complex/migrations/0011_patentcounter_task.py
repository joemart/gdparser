# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0010_auto_20150904_1220'),
    ]

    operations = [
        migrations.AddField(
            model_name='patentcounter',
            name='task',
            field=models.ForeignKey(to='complex.PatentTask', null=True),
            preserve_default=True,
        ),
    ]
