# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0012_auto_20150904_1416'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patentcounter',
            name='count',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
