# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='patenttask',
            name='status',
            field=models.CharField(default=b'wait', max_length=127, null=True, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
