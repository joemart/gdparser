# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0019_auto_20150927_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='morningstar_tools_page',
            field=models.CharField(help_text=b'http://tools.morningstar.co.uk/uk/stockreport/default.aspx?SecurityToken=0P0000A5WF]3]0]E0WWE$$ALL', max_length=255, null=True, verbose_name=b'Morningstar tools link', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='morningstar_page',
            field=models.CharField(help_text=b'http://www.morningstar.com/stocks/XNAS/MSON/quote.html', max_length=255, null=True, verbose_name=b'Morningstar financial link', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='sector',
            field=models.CharField(help_text=b'Will be filled auto. Edit manually if not data in morningstar.', max_length=63, null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
    ]
