# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AmazonBrandRawOutput',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amazon_brand', models.CharField(max_length=255)),
                ('amazon_product_name', models.TextField(null=True, blank=True)),
                ('amazon_review_date', models.DateField(null=True, blank=True)),
                ('amazon_review_stars', models.SmallIntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AmazonBrandTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('completed', models.BooleanField(default=False, editable=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('companyid', models.CharField(unique=True, max_length=127)),
                ('company_name', models.CharField(max_length=127)),
                ('morningstar_page', models.CharField(max_length=255)),
                ('yahoo_page', models.CharField(max_length=255)),
                ('status', models.CharField(default=b'creating', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CompanyOutput',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('com_id', models.CharField(unique=True, max_length=127)),
                ('name', models.CharField(max_length=255)),
                ('yahoo_sector', models.CharField(max_length=63, null=True, blank=True)),
                ('yahoo_industry', models.CharField(max_length=63, null=True, blank=True)),
                ('yahoo_name', models.CharField(max_length=255, null=True, blank=True)),
                ('yahoo_website', models.CharField(max_length=255, null=True, blank=True)),
                ('employees', models.CharField(max_length=63, null=True, blank=True)),
                ('business_description', models.TextField(null=True, blank=True)),
                ('forward_pe', models.CharField(max_length=63, null=True, blank=True)),
                ('market_cap', models.CharField(max_length=63, null=True, blank=True)),
                ('market_cap_currency', models.CharField(max_length=15, null=True, blank=True)),
                ('market_cap_units', models.CharField(max_length=15, null=True, blank=True)),
                ('morningstar_name', models.CharField(max_length=255, null=True, blank=True)),
                ('morningstar_website', models.CharField(max_length=255, null=True, blank=True)),
                ('morningstar_sector', models.CharField(max_length=63, null=True, blank=True)),
                ('morningstar_industry', models.CharField(max_length=63, null=True, blank=True)),
                ('ISIN', models.CharField(max_length=63, null=True, blank=True)),
                ('status', models.CharField(max_length=255, null=True, blank=True)),
                ('company', models.OneToOneField(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CompanyOutputFinancial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rate', models.FloatField(null=True, blank=True)),
                ('com_id', models.CharField(max_length=127, null=True, blank=True)),
                ('year', models.IntegerField(null=True, blank=True)),
                ('morningstar_currency', models.CharField(max_length=7, null=True, blank=True)),
                ('morningstar_currency_units', models.CharField(max_length=63, null=True, blank=True)),
                ('yahoo_currency', models.CharField(max_length=7, null=True, blank=True)),
                ('yahoo_currency_units', models.CharField(max_length=63, null=True, blank=True)),
                ('sales', models.CharField(max_length=127, null=True, blank=True)),
                ('research_and_development', models.CharField(max_length=127, null=True, blank=True)),
                ('operating_income', models.CharField(max_length=127, null=True, blank=True)),
                ('net_income', models.CharField(max_length=127, null=True, blank=True)),
                ('depreciation_and_amortisation', models.CharField(max_length=127, null=True, blank=True)),
                ('net_ppe', models.CharField(max_length=127, null=True, blank=True)),
                ('intangible_assets', models.CharField(max_length=127, null=True, blank=True)),
                ('total_assets', models.CharField(max_length=127, null=True, blank=True)),
                ('total_stockholder_equity', models.CharField(max_length=127, null=True, blank=True)),
                ('roa', models.CharField(max_length=127, null=True, blank=True)),
                ('roe', models.CharField(max_length=127, null=True, blank=True)),
                ('actual', models.DateField(auto_now=True, null=True)),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExchangeRates',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('currency', models.CharField(max_length=7)),
                ('last_updated', models.DateField(auto_now=True)),
                ('value', models.FloatField(null=True, blank=True)),
                ('year', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FadeRates',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('yahoo_industry', models.CharField(unique=True, max_length=255)),
                ('randd_life', models.IntegerField(null=True, blank=True)),
                ('randd_lag', models.IntegerField(null=True, blank=True)),
                ('earnings_fade_period', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GlassdoorPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.CharField(max_length=255)),
                ('status', models.CharField(default=b'wait', max_length=127)),
                ('completed', models.BooleanField(default=False)),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GlassdoorPageCalcResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('com_id', models.CharField(max_length=127, null=True, blank=True)),
                ('comments_count', models.IntegerField(default=0, null=True, blank=True)),
                ('keyword', models.CharField(max_length=127, null=True, blank=True)),
                ('value', models.FloatField(null=True, blank=True)),
                ('page', models.ForeignKey(to='complex.GlassdoorPage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GlassdoorPageRawResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('com_id', models.CharField(max_length=127, null=True, blank=True)),
                ('employee_review_date', models.DateField(null=True, blank=True)),
                ('employee_review_rating', models.IntegerField(null=True, blank=True)),
                ('employee_review_title', models.TextField(null=True, blank=True)),
                ('employee_review_pros', models.TextField(null=True, blank=True)),
                ('employee_review_cons', models.TextField(null=True, blank=True)),
                ('employee_review_advice', models.TextField(null=True, blank=True)),
                ('page', models.ForeignKey(to='complex.GlassdoorPage')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=127, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PatentOutput',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ticker', models.CharField(max_length=127)),
                ('publication_link', models.CharField(max_length=255, null=True, blank=True)),
                ('publication_number', models.CharField(max_length=255)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('summary', models.TextField(null=True, blank=True)),
                ('application_date', models.DateField(null=True, blank=True)),
                ('publication_date', models.DateField(null=True, blank=True)),
                ('grant_date', models.DateField(null=True, blank=True)),
                ('applicant', models.CharField(max_length=255, null=True, blank=True)),
                ('many_applicants', models.BooleanField(default=False)),
                ('l1', models.CharField(max_length=7, null=True, blank=True)),
                ('l2', models.CharField(max_length=7, null=True, blank=True)),
                ('l3', models.CharField(max_length=7, null=True, blank=True)),
                ('l4', models.CharField(max_length=7, null=True, blank=True)),
                ('l5', models.CharField(max_length=7, null=True, blank=True)),
                ('details_parsed', models.BooleanField(default=False)),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PatentOutputApplicant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('patent', models.ForeignKey(to='complex.PatentOutput')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PatentTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=2550)),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UniversalPageCalcResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('com_id', models.CharField(max_length=127, null=True, blank=True)),
                ('header', models.TextField(null=True, blank=True)),
                ('date', models.CharField(max_length=255, null=True, blank=True)),
                ('page_url', models.CharField(max_length=255, null=True, blank=True)),
                ('keyword', models.CharField(max_length=127, null=True, blank=True)),
                ('value', models.FloatField(null=True, blank=True)),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UniversalPageTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pagination_mask', models.CharField(help_text=b'Replace number of page vith PAGENUM keyword. Example http://www.cnet.com/uk/search/?query=apple&fq=&sort=1&p=PAGENUM&typeName=&rpp=10', max_length=255)),
                ('max_pages_count', models.IntegerField(null=True, blank=True)),
                ('status', models.CharField(default=b'wait', max_length=63)),
                ('company', models.ForeignKey(to='complex.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UniversalScraperRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('domain', models.CharField(max_length=255)),
                ('article_link_xpath', models.CharField(help_text=b"Example: //a[starts-with(@href, '/uk/products/')]", max_length=255)),
                ('text_extract_xpath', models.CharField(help_text=b'Example: //p', max_length=255)),
                ('article_header_xpath', models.CharField(help_text=b"Example: //h1[@class='headline']", max_length=255, null=True, blank=True)),
                ('article_date_xpath', models.CharField(help_text=b'Example: //span[@itemprop="datePublished"]', max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='universalpagetask',
            name='rule',
            field=models.ForeignKey(to='complex.UniversalScraperRule'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='exchangerates',
            unique_together=set([('year', 'currency')]),
        ),
        migrations.AlterUniqueTogether(
            name='companyoutputfinancial',
            unique_together=set([('company', 'year')]),
        ),
        migrations.AddField(
            model_name='amazonbrandtask',
            name='company',
            field=models.ForeignKey(to='complex.Company'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='amazonbrandrawoutput',
            name='task',
            field=models.ForeignKey(to='complex.AmazonBrandTask'),
            preserve_default=True,
        ),
    ]
