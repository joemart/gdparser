# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0008_auto_20150709_2037'),
    ]

    operations = [
        migrations.AddField(
            model_name='glassdoorpage',
            name='last_updated',
            field=models.DateField(null=True, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
