# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0013_auto_20150904_1502'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='sector',
            field=models.CharField(max_length=63, null=True, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
