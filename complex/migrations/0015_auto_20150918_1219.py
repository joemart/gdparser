# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('complex', '0014_company_sector'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='sector',
            field=models.CharField(help_text=b'Will be filled auto. Edit manually if not data in yahoo site.', max_length=63, null=True, db_index=True, blank=True),
            preserve_default=True,
        ),
    ]
