from django.contrib import admin, messages
from calculations.glassdoor_frequency_calculator import calculate_freq
from complex.scrapers.main_parser import Parser
from tasks import amazon_parse, universal_parse, glassdoor_scrape, patent_parsing, patent_count_parsing, glassdor_freq
from models import *


# Register your models here.


class CompanyAdmin(admin.ModelAdmin):
    actions = ['start_parsing', 'update_financial', 'update_all', 'glassdoor_calculate']

    def start_parsing(self, request, queryset):
        companies_names = []
        for company in queryset:
            if not company.morningstar_page:
                self.message_user(request, 'Company %s can\'t be parsed - please input morningstar url.' % company,
                                  level=messages.WARNING)
                continue
            if not company.morningstar_page.startswith(
                    'http://www.morningstar.com/stocks/') or not company.morningstar_page.endswith('/quote.html'):
                self.message_user(request,
                                  'Company %s can\'t be parsed - incorrect morningstar url. Please use "http://www.morningstar.com/stocks/*/*/quote.html" ' % company,
                                  level=messages.WARNING)
                continue
            company.status = 'wait parsing'
            company.save()

            company_output, created = CompanyOutput.objects.get_or_create(company=company, com_id=company.companyid, name=company.company_name)
            if not created:
                self.message_user(request, 'Company %s already parsed. Can\'t start task' % company.company_name, level = messages.ERROR)
                continue
            else:
                parser = Parser(company)
                parser.start()
                companies_names.append(company.company_name)
        self.message_user(request, 'Company %s parsing started.' % ', '.join(companies_names), level=messages.INFO)

    def update_financial(self, request, queryset):
        companies_names = []
        for company in queryset:
            if not company.morningstar_page:
                self.message_user(request, 'Company %s can\'t be parsed - please input morningstar url.' % company,
                                  level=messages.WARNING)
                continue
            if not company.morningstar_page.startswith(
                    'http://www.morningstar.com/stocks/') or not company.morningstar_page.endswith('/quote.html'):
                self.message_user(request,
                                  'Company %s can\'t be parsed - incorrect morningstar url. Please use "http://www.morningstar.com/stocks/*/*/quote.html" ' % company,
                                  level=messages.WARNING)
                continue
            company_output, created = CompanyOutput.objects.get_or_create(company=company, com_id=company.companyid, name=company.company_name)
            parser = Parser(company)
            parser.start(update_financial_only=True)
            companies_names.append(company.company_name)

        self.message_user(request, 'Company %s fin data updating started.' % ', '.join(companies_names), level=messages.INFO)

    def update_all(self, request, queryset):
        companies_names = []
        for company in queryset:
            if not company.morningstar_page:
                self.message_user(request, 'Company %s can\'t be parsed - please input morningstar url.' % company,
                                  level=messages.WARNING)
                continue
            if not company.morningstar_page.startswith(
                    'http://www.morningstar.com/stocks/') or not company.morningstar_page.endswith('/quote.html'):
                self.message_user(request,
                                  'Company %s can\'t be parsed - incorrect morningstar url. Please use "http://www.morningstar.com/stocks/*/*/quote.html" ' % company,
                                  level=messages.WARNING)
                continue
            parser = Parser(company)
            parser.update()
            companies_names.append(company.company_name)
        self.message_user(request, 'Company %s all data updating started.' % ', '.join(companies_names), level=messages.INFO)

    def glassdoor_calculate(self, request, queryset):
        companies_names = []
        for company in queryset:
            glassdor_freq.delay(company.pk)
            companies_names.append(company.company_name)
        self.message_user(request, 'Glassdoor %s stats recalculation started.' % ', '.join(companies_names), level=messages.INFO)

    start_parsing.short_description = "Start company scraping"
    update_financial.short_description = "Update company financial and main data"
    glassdoor_calculate.short_description = "(Re)calculate glassdoor keywords frequency."
    update_all.short_description = "Update all data"
    list_display = ('company_name', 'companyid', 'sector')
    search_fields = ['company_name']


class ExchangeRatesAdmin(admin.ModelAdmin):
    list_filter = ('currency', 'year')
    search_fields = ['currency', 'year']
    list_display = ('currency', 'year', 'value')


class CompanyOutputAdmin(admin.ModelAdmin):
    list_display = (
    'com_id', 'name', 'patents_scraped', 'patents_detailed', 'financial_data_actual_at', 'glassdoor_tasks',
    'glassdoor_reviews_count', 'amazon_reviews_count', 'product_reviews_count')


class FadeRatesAdmin(admin.ModelAdmin):
    list_display = ('yahoo_industry', 'randd_life', 'randd_lag', 'earnings_fade_period',)


class CompanyOutputFinAdmin(admin.ModelAdmin):
    list_display = ('company', 'com_id', 'sales','operating_income','research_and_development', 'total_assets', 'year','morningstar_currency_units','morningstar_currency')
    list_filter = ('company__sector', 'company', 'year')


class PatentOutputAdmin(admin.ModelAdmin):
    list_display = ('company', 'title', 'details_parsed')
    list_filter = ('company',)


class PatentCounterAdmin(admin.ModelAdmin):
    list_display = ('company', 'year', 'category', 'count', 'updated')
    list_filter = ('company', 'year')


class PatentTaskAdmin(admin.ModelAdmin):
    actions = ['start_parsing', 'update_counters']
    list_display = ('company', 'value', 'results_count', 'status', 'last_updated', 'last_counters_refresh')
    list_filter = ('company',)

    def start_parsing(self, request, queryset):
        for task in queryset:
            if task.status == 'wait':
                patent_parsing.delay(task.company.pk, task.pk)
                self.message_user(request, 'Patent Task %s parsing started.' % task.value, level=messages.INFO)
            else:
                self.message_user(request, 'Patent Task %s already parsed.' % task.value, level=messages.ERROR)

    def update_counters(self, request, queryset):
        for task in queryset:
            patent_count_parsing.delay(task.company.pk, task.pk)
            self.message_user(request, 'Patent counter update task %s started.' % task.value, level=messages.INFO)

    start_parsing.short_description = "Start Patent scraping"
    update_counters.short_description = "Update Patent counters"


class PatentApplicantAdmin(admin.ModelAdmin):
    list_display = ('name', 'patent')
    list_filter = ('patent__company',)


class GlassdoorPageAdmin(admin.ModelAdmin):
    actions = ['start_parsing','update_results']
    list_display = ('company', 'link', 'status', 'completed', 'last_updated')
    list_filter = ('company',)

    def start_parsing(self, request, queryset):
        for task in queryset:
            if not task.completed:
                glassdoor_scrape.delay(task.company.pk, task.pk)
                self.message_user(request, 'Glassdoor page %s parsing started.' % task.link, level=messages.INFO)
            else:
                self.message_user(request, 'Glassdoor page %s already parsed.' % task.link, level=messages.ERROR)

    def update_results(self, request, queryset):
        for task in queryset:
            glassdoor_scrape.delay(task.company.pk, task.pk, True)
            self.message_user(request, 'Glassdoor page %s update started.' % task.link, level=messages.INFO)

    start_parsing.short_description = "Start Glassdoor pages scraping"
    update_results.short_description = "Update Glassdoor reviews"


class GlassdoorPageRawResultAdmin(admin.ModelAdmin):
    list_display = ('com_id', 'employee_review_date', 'employee_review_title', 'employee_review_rating')
    list_filter = ('com_id',)


class GlassdoorPageCalcResultAdmin(admin.ModelAdmin):
    list_display = ('com_id', 'page', 'comments_count', 'keyword', 'value')
    list_filter = ('com_id',)


class AmazonBrandTaskAdmin(admin.ModelAdmin):
    actions = ['start_parsing', 'update_results']
    list_display = ('company', 'value', 'completed', 'last_updated')
    list_filter = ('company',)

    def start_parsing(self, request, queryset):
        for task in queryset:
            if not task.completed:
                amazon_parse.delay(task.company.pk, task.pk)
                self.message_user(request, 'Amazon brand %s parsing started.' % task.value, level=messages.INFO)
            else:
                self.message_user(request, 'Amazon brand %s already parsed.' % task.value, level=messages.ERROR)

    def update_results(self, request, queryset):
        for task in queryset:
            amazon_parse.delay(task.company.pk, task.pk, True)
            self.message_user(request, 'Amazon brand %s update started.' % task.value, level=messages.INFO)

    start_parsing.short_description = "Start amazon brands scraping"
    update_results.short_description = "Update amazon brands scraping results"


class AmazonBrandRawOutputAdmin(admin.ModelAdmin):
    list_display = ('amazon_brand', 'amazon_product_name', 'amazon_review_date', 'amazon_review_stars')
    list_filter = ('task__company',)


class UniversalPageTaskAdmin(admin.ModelAdmin):
    actions = ['start_parsing','update_results']
    list_display = ('company', 'rule', 'status', 'last_updated')
    list_filter = ('company', 'rule')

    def start_parsing(self, request, queryset):
        for upage in queryset:
            if upage.status == 'wait':
                universal_parse.delay(upage.company.pk, upage.pk)
                self.message_user(request, 'Reviews from %s parsing started.' % upage.pagination_mask,
                                  level=messages.INFO)
            else:
                self.message_user(request, 'Amazon brand %s already parsed.' % upage.pagination_mask,
                                  level=messages.ERROR)

    def update_results(self, request, queryset):
        for upage in queryset:
            universal_parse.delay(upage.company.pk, upage.pk, True)
            self.message_user(request, 'Reviews from %s update started.' % upage.pagination_mask, level=messages.INFO)


    start_parsing.short_description = "Start product review scraping"
    update_results.short_description = "Update product reviews"


class UniversalPageCalcResultAdmin(admin.ModelAdmin):
    list_display = ('company', 'header', 'page_url', 'sentiment_type', 'sentiment_score')
    list_filter = ('company',)


class GlassdorKeywordOutputAdmin(admin.ModelAdmin):
    actions = ['clear_all']
    list_display = ('company', 'value', 'count', 'glassdoor_strength', 'glassdoor_reliability')
    list_filter = ('company',)
    search_fields = ('value',)

    def clear_all(self, request, queryset):
        queryset.delete()
        self.message_user(request, 'All outputs deleted.')

    clear_all.short_description = "Fast delete without(!!!) confirmation"

class GlassdorKeywordDistanceInputAdmin(admin.ModelAdmin):
    list_display = ('keyword_target', 'value', 'distance')
    list_filter = ('keyword_target',)



admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyOutput, CompanyOutputAdmin)
admin.site.register(CompanyOutputFinancial, CompanyOutputFinAdmin)
admin.site.register(ExchangeRates, ExchangeRatesAdmin)
admin.site.register(PatentTask, PatentTaskAdmin)
admin.site.register(FadeRates, FadeRatesAdmin)
admin.site.register(PatentOutput, PatentOutputAdmin)
admin.site.register(PatentCounter, PatentCounterAdmin)
admin.site.register(PatentOutputApplicant, PatentApplicantAdmin)

admin.site.register(GlassdoorPage, GlassdoorPageAdmin)
admin.site.register(GlassdoorPageRawResult, GlassdoorPageRawResultAdmin)
admin.site.register(GlassdorKeywordOutput,GlassdorKeywordOutputAdmin)
admin.site.register(GlassdorKeywordStopInput)
admin.site.register(GlassdorKeywordDistanceInput, GlassdorKeywordDistanceInputAdmin)
# admin.site.register(Keyword)
admin.site.register(GlassdoorPageCalcResult, GlassdoorPageCalcResultAdmin)

admin.site.register(AmazonBrandTask, AmazonBrandTaskAdmin)
admin.site.register(AmazonBrandRawOutput, AmazonBrandRawOutputAdmin)

admin.site.register(UniversalScraperRule)
admin.site.register(UniversalPageTask, UniversalPageTaskAdmin)
admin.site.register(UniversalPageCalcResult, UniversalPageCalcResultAdmin)