from django.db import models
from django.core.cache import cache
# Create your models here.
from django.db.models import Sum
from calculations.cache_maker import cache_dec


class Company(models.Model):
    companyid = models.CharField(max_length=127, unique=True)
    company_name = models.CharField(max_length=127)
    morningstar_page = models.CharField(max_length=255, null=True, blank=True,
                                        verbose_name='Morningstar financial link',
                                        help_text='http://www.morningstar.com/stocks/XNAS/MSON/quote.html')
    morningstar_tools_page = models.CharField(max_length=255, null=True, blank=True,
                                              verbose_name='Morningstar tools link',
                                              help_text='http://tools.morningstar.co.uk/uk/stockreport/default.aspx?SecurityToken=0P0000A5WF]3]0]E0WWE$$ALL')
    yahoo_page = models.CharField(max_length=255, null=True, blank=True)

    sector = models.CharField(null=True, blank=True, db_index=True, max_length=63,
                              help_text='Will be filled auto. Edit manually if not data in morningstar.')

    status = models.CharField(max_length=255, default='creating', editable=False)

    def __unicode__(self):
        return self.company_name


class PatentTask(models.Model):
    company = models.ForeignKey(Company)
    value = models.CharField(max_length=2550)
    results_count = models.IntegerField(null=True, blank=True)
    status = models.CharField(null=True, blank=True, max_length=127, default='wait', editable=False)
    last_updated = models.DateField(null=True, blank=True, editable=False)
    last_counters_refresh = models.DateField(null=True, blank=True)

    def set_status(self, status_text):
        self.status = status_text[:127]
        self.save()

    def __unicode__(self):
        return '%s : %s' % (self.company.companyid,self.value)


class PatentCounter(models.Model):
    company = models.ForeignKey(Company, db_index=True)
    task = models.ForeignKey(PatentTask, null=True)
    year = models.IntegerField()
    category = models.CharField(max_length=15)
    count = models.IntegerField(null=True)
    updated = models.DateField(auto_now=True)


class PatentOutput(models.Model):
    company = models.ForeignKey(Company)

    ticker = models.CharField(max_length=127)
    publication_link = models.CharField(max_length=255, null=True, blank=True)
    publication_number = models.CharField(max_length=255)
    title = models.CharField(max_length=255, null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    application_date = models.DateField(null=True, blank=True)
    publication_date = models.DateField(null=True, blank=True)
    grant_date = models.DateField(null=True, blank=True)

    applicant = models.CharField(max_length=255, null=True, blank=True)
    many_applicants = models.BooleanField(default=False)

    l1 = models.CharField(max_length=7, null=True, blank=True)
    l2 = models.CharField(max_length=7, null=True, blank=True)
    l3 = models.CharField(max_length=7, null=True, blank=True)
    l4 = models.CharField(max_length=7, null=True, blank=True)
    l5 = models.CharField(max_length=7, null=True, blank=True)

    details_parsed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title

        # waiting for detail


class PatentOutputApplicant(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    patent = models.ForeignKey(PatentOutput)

    def __unicode__(self):
        return self.name


class CompanyOutput(models.Model):
    company = models.OneToOneField(Company)

    com_id = models.CharField(max_length=127, unique=True)
    name = models.CharField(max_length=255)
    yahoo_sector = models.CharField(max_length=63, null=True, blank=True)
    yahoo_industry = models.CharField(max_length=63, null=True, blank=True)
    yahoo_name = models.CharField(max_length=255, null=True, blank=True)
    yahoo_website = models.CharField(max_length=255, null=True, blank=True)
    employees = models.CharField(max_length=63, null=True, blank=True)
    business_description = models.TextField(null=True, blank=True)
    forward_pe = models.CharField(max_length=63, null=True, blank=True)
    market_cap = models.CharField(max_length=63, null=True, blank=True)
    market_cap_currency = models.CharField(max_length=15, null=True, blank=True)
    market_cap_units = models.CharField(max_length=15, null=True, blank=True)
    morningstar_name = models.CharField(max_length=255, null=True, blank=True)
    morningstar_website = models.CharField(max_length=255, null=True, blank=True)
    morningstar_sector = models.CharField(max_length=63, null=True, blank=True)
    morningstar_industry = models.CharField(max_length=63, null=True, blank=True)
    ISIN = models.CharField(max_length=63, null=True, blank=True)

    status = models.CharField(max_length=255, null=True, blank=True, editable=False)

    def __unicode__(self):
        return self.name

    def patents_scraped(self):
        return PatentOutput.objects.filter(company=self.company).count()

    def patents_detailed(self):
        return PatentOutput.objects.filter(company=self.company, details_parsed=True).count()

    def financial_data_actual_at(self):
        fins = CompanyOutputFinancial.objects.filter(company=self.company).order_by('-actual')
        if len(fins):
            return fins[0].actual
        else:
            return '---'

    def glassdoor_tasks(self):
        return "%s/%s" % (GlassdoorPage.objects.filter(completed=True, company=self.company).count(),
                          GlassdoorPage.objects.filter(company=self.company).count())

    def glassdoor_reviews_count(self):
        return GlassdoorPageRawResult.objects.filter(page__company=self.company).count()

    def product_reviews_tasks(self):
        return "%s/%s" % (UniversalPageTask.objects.filter(status='complete', company=self.company).count(),
                          UniversalPageTask.objects.filter(company=self.company).count())

    def product_reviews_count(self):
        return UniversalPageCalcResult.objects.filter(company=self.company).count()

    def amazon_reviews_count(self):
        return "%s reviews from %s/%s tasks" % (AmazonBrandRawOutput.objects.filter(task__company=self.company).count(),
                                                AmazonBrandTask.objects.filter(company=self.company, completed=True).count(),
                                                AmazonBrandTask.objects.filter(company=self.company).count())


class CompanyOutputFinancial(models.Model):
    company = models.ForeignKey(Company, db_index=True)

    rate = models.FloatField(null=True, blank=True)
    com_id = models.CharField(max_length=127, null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    morningstar_currency = models.CharField(max_length=7, null=True, blank=True)
    morningstar_currency_units = models.CharField(max_length=63, null=True, blank=True)
    yahoo_currency = models.CharField(max_length=7, null=True, blank=True)
    yahoo_currency_units = models.CharField(max_length=63, null=True, blank=True)
    sales = models.CharField(max_length=127, null=True, blank=True)
    research_and_development = models.CharField(max_length=127, null=True, blank=True)
    operating_income = models.CharField(max_length=127, null=True, blank=True, verbose_name='EBITDA')
    net_income = models.CharField(max_length=127, null=True, blank=True)
    depreciation_and_amortisation = models.CharField(max_length=127, null=True, blank=True)
    net_ppe = models.CharField(max_length=127, null=True, blank=True)
    intangible_assets = models.CharField(max_length=127, null=True, blank=True)
    total_assets = models.CharField(max_length=127, null=True, blank=True)
    total_stockholder_equity = models.CharField(max_length=127, null=True, blank=True)
    roa = models.CharField(max_length=127, null=True, blank=True)
    roe = models.CharField(max_length=127, null=True, blank=True)

    actual = models.DateField(auto_now=True, null=True, blank=True)

    def __unicode__(self):
        return "%s - %s" % (self.company.company_name, self.year)

    class Meta:
        unique_together = ['company', 'year']


class FadeRates(models.Model):
    yahoo_industry = models.CharField(max_length=255, unique=True)
    randd_life = models.IntegerField(default=1)
    randd_lag = models.IntegerField(default=1)
    earnings_fade_period = models.IntegerField(default=1)


class ExchangeRates(models.Model):
    currency = models.CharField(max_length=7)
    last_updated = models.DateField(auto_now=True)
    value = models.FloatField(null=True, blank=True)
    year = models.IntegerField()

    class Meta:
        unique_together = ("year", "currency")


class GlassdoorPage(models.Model):
    company = models.ForeignKey(Company)
    link = models.CharField(max_length=255)

    status = models.CharField(default='wait', max_length=127)
    completed = models.BooleanField(default=False)
    last_updated = models.DateField(null=True, blank=True, editable=True)

    def __unicode__(self):
        return self.link


class GlassdoorPageRawResult(models.Model):
    page = models.ForeignKey(GlassdoorPage)

    com_id = models.CharField(max_length=127, null=True, blank=True)
    employee_review_date = models.DateField(null=True, blank=True)
    employee_review_rating = models.IntegerField(null=True, blank=True)
    employee_review_title = models.TextField(null=True, blank=True)
    employee_review_pros = models.TextField(null=True, blank=True)
    employee_review_cons = models.TextField(null=True, blank=True)
    employee_review_advice = models.TextField(null=True, blank=True)


class Keyword(models.Model):
    value = models.CharField(max_length=127, null=True, blank=True)

    def __unicode__(self):
        return self.value


class GlassdoorPageCalcResult(models.Model):
    page = models.ForeignKey(GlassdoorPage)
    com_id = models.CharField(max_length=127, null=True, blank=True)
    comments_count = models.IntegerField(null=True, blank=True, default=0)
    keyword = models.CharField(max_length=127, null=True, blank=True)
    value = models.FloatField(null=True, blank=True)


class UniversalScraperRule(models.Model):
    domain = models.CharField(max_length=255)
    article_link_xpath = models.CharField(max_length=255, help_text="Example: //a[starts-with(@href, '/uk/products/')]")
    text_extract_xpath = models.CharField(max_length=255, help_text="Example: //p")
    article_header_xpath = models.CharField(max_length=255, null=True, blank=True, help_text="Example: //h1[@class='headline']")
    article_date_xpath = models.CharField(max_length=255, null=True, blank=True, help_text="Example: //span[@itemprop=\"datePublished\"]")

    def __unicode__(self):
        return self.domain


class UniversalPageTask(models.Model):
    company = models.ForeignKey(Company)
    rule = models.ForeignKey(UniversalScraperRule)
    pagination_mask = models.CharField(max_length=255, help_text='Replace number of page vith PAGENUM keyword. Example http://www.cnet.com/uk/search/?query=apple&fq=&sort=1&p=PAGENUM&typeName=&rpp=10')
    max_pages_count = models.IntegerField(null=True, blank=True)
    status = models.CharField(max_length=63, default='wait', editable=False)
    last_updated = models.DateField(null=True, blank=True, editable=False)


class UniversalPageCalcResult(models.Model):
    company = models.ForeignKey(Company)
    com_id = models.CharField(max_length=127, null=True, blank=True)
    header = models.TextField(null=True, blank=True)
    date = models.CharField(max_length=255, null=True, blank=True)
    page_url = models.TextField(max_length=255, null=True, blank=True)
    # keyword = models.CharField(max_length=127, null=True, blank=True)
    # value = models.FloatField(null=True, blank=True)
    sentiment_type = models.CharField(null=True,blank=True,max_length=63)
    sentiment_score = models.FloatField(null=True, blank=True)


class AmazonBrandTask(models.Model):
    company = models.ForeignKey(Company)
    value = models.CharField(max_length=255)
    last_updated = models.DateField(null=True, blank=True, editable=False)
    completed = models.BooleanField(default=False, editable=False)


class AmazonBrandRawOutput(models.Model):
    task = models.ForeignKey(AmazonBrandTask)
    amazon_id = models.CharField(max_length=63, null=True, blank=True)
    amazon_brand = models.CharField(max_length=255, help_text='Amazon brand name. Important! case-sensitive! Example: "LG" not "Lg" or "lg"')
    amazon_product_name = models.TextField(null=True, blank=True)
    amazon_review_date = models.DateField(null=True, blank=True)
    amazon_review_stars = models.SmallIntegerField(null=True, blank=True)

@cache_dec()
def _get_keywords_total():
    return GlassdorKeywordOutput.objects.all().aggregate(sum=Sum('count'))['sum']

class GlassdorKeywordOutput(models.Model):
    company = models.ForeignKey(Company)
    value = models.CharField(max_length=31, db_index=True)
    count = models.IntegerField()

    class Meta:
        ordering = ('-count',)

    def get_freq_company(self):
        rate = cache.get("get_freq_company_%s" % self.pk)
        if rate:
            return rate
        sum_all = GlassdorKeywordOutput.objects.filter(company=self.company).aggregate(sum=Sum('count'))['sum']
        sum = self.count
        sim_terms = GlassdorKeywordDistanceInput.objects.filter(keyword_target=self.value)
        for term in sim_terms:
            sim_output = GlassdorKeywordOutput.objects.filter(company=self.company, value=term.value).first()
            if sim_output:
                sum += term.distance * sim_output.count
        res = (sum, sum / float(sum_all))
        cache.set("get_freq_company_%s" % self.pk, res, 10)
        return res

    def get_freq_total(self):
        sum_all = _get_keywords_total()
        sum = GlassdorKeywordOutput.objects.filter(value=self.value).aggregate(sum=Sum('count'))['sum']
        sim_terms = GlassdorKeywordDistanceInput.objects.filter(keyword_target=self.value)
        for term in sim_terms:
            sum_sim_output = GlassdorKeywordOutput.objects.filter(value=term.value).aggregate(sum=Sum('count'))['sum']
            if sum_sim_output:
                sum += term.distance * sum_sim_output
        return (sum, sum / float(sum_all))

    def glassdoor_strength(self):
        company = self.get_freq_company()
        total = self.get_freq_total()
        return "%.4f%%" % (total[1] - company[1])

    def glassdoor_reliability(self):
        company = self.get_freq_company()
        return "%s" % company[0]

class GlassdorKeywordStopInput(models.Model):
    value = models.CharField(max_length=31)

    def __unicode__(self):
        return self.value


class GlassdorKeywordDistanceInput(models.Model):
    keyword_target = models.CharField(max_length=31, db_index=True)
    value = models.CharField(max_length=31)
    distance = models.FloatField()


# class SentenceSample(models.Model):
#     company = models.ForeignKey(Company)
#     value = models.TextField()

