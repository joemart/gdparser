"""
Django settings for scraper project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
import djcelery
djcelery.setup_loader()


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@8kpsbq-=^@rx(*yo!p9340+k$wh3b5+ga%mwnkbn$j7%n^tu8'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
USE_VIRTUAL_DISPLAY = not DEBUG

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'base',
    'raven.contrib.django.raven_compat',
    'constance',
    'complex',

    'djangobower',
    'rest_framework',

    # 'debug_toolbar',
)

INSTALLED_APPS += ("djcelery", )

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'scraper.urls'

WSGI_APPLICATION = 'scraper.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases



CONSTANCE_CONFIG = {
    'G_LOGIN': ('', 'Glassdoor login'),
    'G_PASS': ('', 'Glassdoor password'),
    'CAPTCHA_LOGIN': ('', 'DeathByCaptcha login'),
    'CAPTCHA_PASS': ('', 'DeathByCaptcha password'),
    'CAPTCHA_TRY_COUNT': (3, 'Captcha resolve try count'),
    'MAX_PATENTS_PAGES': (3, 'Pages of patents(200 per page) to scrape.'),
    'MAX_AMAZON_SEARCH_PAGES': (3, 'Pages of amazon search results to scrape.'),
    'MAX_AMAZON_REVIEWS_PAGES': (5, 'Pages of amazon reviews to scrape.'),
    'INCLUSION_FLAG': (1, 'capitalised value of R&D must be more than X% of the value of the assets in the same year.'),
    'EXTREME_RND': (250, 'Max return on investment value in percent.'),
    'EXTREME_CAP_RND': (100, 'Max capitalized rnd in percent.')
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379/0'
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['pickle', 'json']
CELERYD_HIJACK_ROOT_LOGGER = False
# CELERYD_PREFETCH_MULTIPLIER = 1
CELERY_DEFAULT_QUEUE = "default"
CELERY_QUEUES = {
    "default": {
        "binding_key": "task.#",
    },
    "patents_main": {
        "binding_key": "patents.#",
    },
    "patents_detail": {
        "binding_key": "patents_detail.#",
    },
    "patents_count": {
        "binding_key": "patents_count.#",
    },
    "gd": {
        "binding_key": "gd.#",
    },
    "amazon": {
        "binding_key": "amazon.#",
    },
    "universal": {
        "binding_key": "universal.#",
    }
}

CELERY_DEFAULT_EXCHANGE = "tasks"
CELERY_DEFAULT_EXCHANGE_TYPE = "topic"
CELERY_DEFAULT_ROUTING_KEY = "task.default"

CELERY_ROUTES = {
    "complex.tasks.patent_detail_parse": {
        "queue": "patents_detail",
        "routing_key": "patents_detail.patent_detail_parse",
    },
    "complex.tasks.patent_parsing": {
        "queue": "patents_main",
        "routing_key": "patents.patent_parsing",
    },
    "complex.tasks.patent_count_parsing": {
        "queue": "patents_count",
        "routing_key": "patents_count.patent_count_parsing",
    },
    "complex.tasks.glassdor_freq": {
        "queue": "patents_count",
        "routing_key": "patents_count.glassdor_freq",
    },
    "complex.tasks.complex_parse": {
        "queue": "default",
        "routing_key": "task.complex_parse",
    },
    "base.tasks.parse_router": {
        "queue": "gd",
        "routing_key": "gd.parse_router_default",
    },
    "complex.tasks.glassdoor_scrape": {
        "queue": "gd",
        "routing_key": "gd.glassdor_scrape",
    },
    "complex.tasks.page_extract_task": {
        "queue": "default",
        "routing_key": "gd.page_extract_task",
    },
    "complex.tasks.amazon_parse": {
        "queue": "amazon",
        "routing_key": "amazon.amazon_parse",
    },
    "complex.tasks.universal_parse": {
        "queue": "universal",
        "routing_key": "universal.universal_parse",
    }
}

# ######### STATIC FILE CONFIGURATION

STATICFILES_FINDERS = ("django.contrib.staticfiles.finders.FileSystemFinder",
                       "django.contrib.staticfiles.finders.AppDirectoriesFinder",
                       'djangobower.finders.BowerFinder',)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'staticfiles')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

BOWER_COMPONENTS_ROOT = os.path.join(BASE_DIR, 'components')

BOWER_INSTALLED_APPS = (
    'jquery#<=2.0',
    'bootstrap#3.3.5',
    'angular#1.4.4',
    'angular-route#1.4.4',
    'angular-cookies#1.4.4',
    'angular-resource#1.4.4',
    # 'd3#>=3.5',
    # 'nvd3#>=1.8',
    # 'angular-nvd3#>=1.0',
    'bootstrap-select#1.5.4',
    'angular-bootstrap#0.13.4',
    'highcharts-release#4.1.8',
    'highcharts-ng#0.0.10',
    'angular-preloaded#0.1.0',
)

# ######### MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

try:
    from local_settings import RAVEN_CONFIG
except:
    RAVEN_CONFIG = {'dsn': ''}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,

    'formatters': {
        'console': {
            'format': '[%(asctime)s][%(levelname)s] %(name)s %(filename)s:%(funcName)s:%(lineno)d | %(message)s',
            'datefmt': '%H:%M:%S',
            },
        },

    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'console'
            },
        'sentry': {
            'level': 'WARNING',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': RAVEN_CONFIG['dsn'],
            },
        },

    'loggers': {
        '': {
            'handlers': ['console', 'sentry'],
            'level': 'DEBUG',
            'propagate': False,
            },
        'root': {
            'handlers': ['sentry'],
            'level': 'WARNING',
            'propagate': True,
        },
    }
}


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
        'MAX_ENTRIES': 10000,
        'KEY_PREFIX': 'gd_calc'

    }
}

# REST_FRAMEWORK = {
#     'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
#     'PAGE_SIZE': 50
# }

try:
    from local_settings import *
except:
    pass