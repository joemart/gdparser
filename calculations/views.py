from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404

# Create your views here.
from django.views.generic import TemplateView, FormView
from calculations.calculator import *
from calculations.calculator import _get_sectors_list, _check_sector_is_empty
from calculations.forms import CompaniesListForm
from calculations.importer import companies_list_csv_process, financial_list_csv_process
from complex.models import Company
import json
from django.contrib import messages
from django.core.cache import cache

def _invert_patents(patents):
    res = {}
    for year in sorted(patents.iterkeys()):
        for category in sorted(patents[year].iterkeys()):
            if not category in res:
                res[category] = {}
            res[category][year]=patents[year][category]
    return res


class ImportView(TemplateView):
    template_name = 'calculations/import.html'


class CompaniesListUploadView(FormView):
    template_name = 'calculations/import.html'
    form_class = CompaniesListForm
    success_url = reverse_lazy('import')

    def form_valid(self, form):
        processed_count, errors = companies_list_csv_process(self.request.FILES['companies'])
        if processed_count > 0:
            messages.success(self.request,'Successfully imported %s companies' % processed_count)
        else:
            messages.error(self.request,'No companies imported')
        for error in errors:
            messages.error(self.request, error)
        return super(CompaniesListUploadView, self).form_valid(form)


class FinancialListUploadView(FormView):
    template_name = 'calculations/import.html'
    form_class = CompaniesListForm
    success_url = reverse_lazy('import')

    def form_valid(self, form):
        processed_count, errors = financial_list_csv_process(self.request.FILES['companies'])
        if processed_count > 0:
            messages.success(self.request,'Successfully imported %s financial years' % processed_count)
        else:
            messages.error(self.request,'No financial years imported')
        for error in errors:
            messages.error(self.request, error)
        return super(FinancialListUploadView, self).form_valid(form)


class OverviewView(TemplateView):
    template_name = 'calculations/overview.html'

    def get_context_data(self, **kwargs):
        context = super(OverviewView, self).get_context_data(**kwargs)
        sectors = _get_sectors_list()
        res = {}
        overview_chart_data = None#cache.get("overview_chart_data")
        if not overview_chart_data:
            for sector in sectors:
                return_on_rnd = get_return_on_rnd_sector(sector)
                cap_rnd = get_cap_rnd_sector(sector)
                rnd_adj = get_sector_avg_rnd_sales(sector, r_n_d=True)
                years = [y for y in return_on_rnd.iterkeys() if y in rnd_adj.iterkeys() and y in cap_rnd.iterkeys()]
                if not len(years):
                    continue
                most_recent_year = max(years)
                res[sector] = {'roi': return_on_rnd[most_recent_year]['return_on_rnd'],
                                              'cap_rnd_by_sales': cap_rnd[most_recent_year]['cap_rnd_by_sales_avg'],
                                              'rnd_adj': rnd_adj[most_recent_year]['r_n_d']}
            overview_chart_data = json.dumps(res)
            cache.set("overview_chart_data", overview_chart_data, 60*60)

        context['overview_chart_data'] = overview_chart_data
        context['page'] = 'overview'
        return context


class SectorView(TemplateView):
    template_name = 'calculations/sector.html'

    def get_context_data(self, **kwargs):
        context = super(SectorView, self).get_context_data(**kwargs)
        sector_name=self.kwargs['sector_name']
        if _check_sector_is_empty(sector_name):
            context['is_empty'] = True
            return context

        context['sector_name'] = self.kwargs['sector_name']
        companies = Company.objects.filter(sector=sector_name)
        res = {}
        for company in companies:
            if not CompanyOutputFinancial.objects.filter(company=company).exists():
                continue
            return_on_rnd = get_return_on_rnd_company(company)
            cap_rnd = get_cap_rnd_company(company)
            rnd_adj = get_company_rnd_adj(company)
            years = [y for y in return_on_rnd.iterkeys() if y in rnd_adj['res'].iterkeys() and y in cap_rnd.iterkeys()]
            if not len(years):
                continue
            most_recent_year = max(years)
            res[company.company_name] = {'roi': return_on_rnd[most_recent_year],
                                         'cap_rnd_by_sales': cap_rnd[most_recent_year]['cap_rnd_by_sales'],
                                         'rnd_adj': rnd_adj['res'][most_recent_year]['r_n_d']}
        context['overview_chart_data'] = json.dumps(res)
        context['rnd_by_sales'] = json.dumps(get_sector_avg_rnd_sales(sector_name, r_n_d=True))
        context['patents_by_sales'] = json.dumps(get_patents_ratio_sector(sector_name))
        context['patents_by_category'] = json.dumps(_invert_patents(get_patents_per_category_sector(sector_name, percentage=True)))
        context['return_on_rnd'] = json.dumps(get_return_on_rnd_sector(sector_name))
        context['page'] = 'sector'

        return context


class CompanyView(TemplateView):
    template_name = 'calculations/company.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyView, self).get_context_data(**kwargs)
        company = get_object_or_404(Company, pk=self.kwargs['pk'])
        context['company'] = company
        if not CompanyOutputFinancial.objects.filter(company=company).exists():
            context['is_empty'] = True
            return context
        context['summary'] = json.dumps({})
        # context['summary'] = json.dumps({
        #     'sector': int(round(get_rnd_to_return_sector_relative(company.sector) * 100)),
        #     'company': int(round(get_rnd_to_return_company_relative(company) * 100))
        # })

        cap_rnd = get_cap_rnd_company(company)
        return_on_rnd = get_return_on_rnd_company(company)

        investment_company = {y: {'cap_rnd': cap_rnd[y]['cap_rnd_by_sales'], 'return_on_rnd': return_on_rnd[y]} for y
                              in return_on_rnd}
        cap_rnd = get_cap_rnd_sector(company.sector)
        return_on_rnd = get_return_on_rnd_sector(company.sector)
        investment_sector = {y: {'cap_rnd': cap_rnd[y]['cap_rnd_by_sales_avg'], 'return_on_rnd': return_on_rnd[y]['return_on_rnd']} for y
                              in return_on_rnd if 'return_on_rnd' in return_on_rnd[y]}


        context['investment'] = json.dumps({
            'sector': investment_sector,
            'company': investment_company
        })

        glassdor_rating_sector = get_glassdor_rating_sector(company.sector)
        glassdor_rating_company = get_glassdor_rating_company(company)
        if glassdor_rating_sector:
            context['glassdoor'] = json.dumps({
            'sector': {y: glassdor_rating_sector[y] for y in glassdor_rating_sector if
                       y in glassdor_rating_company},
            'company': glassdor_rating_company
        })
        else:
            context['glassdoor'] =  json.dumps({})

        amazon_rating_sector = get_amazon_rating_sector(company.sector)
        amazon_rating_company = get_amazon_rating_company(company)
        if amazon_rating_company:
            context['amazon'] = json.dumps({
                'sector': {y: amazon_rating_sector[y] for y in amazon_rating_sector if y in amazon_rating_company},
                'company': amazon_rating_company
            })
        else:
            context['amazon'] = json.dumps({})

        rnd_by_sales_sector = get_sector_avg_rnd_sales(company.sector, r_n_d=True)
        rnd_by_sales_company = get_company_rnd_sales(company, r_n_d=True)
        context['rnd_by_sales'] = json.dumps({
            'sector': {y:rnd_by_sales_sector[y]['rnd_by_sales'] for y in rnd_by_sales_sector if y in rnd_by_sales_company},
            'company': rnd_by_sales_company
        })
        patents_sector = get_patents_ratio_sector(company.sector)
        patents_company = get_patents_ratio_company(company)
        context['patents'] = json.dumps({
            'sector': {y:patents_sector[y] for y in patents_sector if y in patents_company},
            'company': patents_company
        })

        return_on_rnd_company = get_return_on_rnd_company(company)
        return_on_rnd_sector = get_return_on_rnd_sector(company.sector)

        context['return_on_rnd'] = json.dumps({
            'sector': {y:return_on_rnd_sector[y] for y in return_on_rnd_sector if y in return_on_rnd_company},
            'company': {y:return_on_rnd_company[y] for y in return_on_rnd_company if y in return_on_rnd_sector}
        })

        context['patents_by_category'] = json.dumps(
            _invert_patents(get_patents_per_category_company(company.pk, percentage=True)))

        context['page'] = 'company'
        return context
