# -*- encoding:utf8 -*-
from complex.models import GlassdoorPageRawResult, GlassdorKeywordStopInput
import nltk
import os
import datetime
from nltk.corpus import wordnet as wn
from django.conf import settings
nltk.data.path.append(os.path.join(settings.BASE_DIR, 'base', 'modules', 'extraction', 'nlt'))
from complex.models import GlassdorKeywordOutput

# nltk.data.path.append('/media/DATA/Programming/python/projects/git_it/gdparser/base/modules/extraction/nlt')
__author__ = 'orion'
LETTERS = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM '
Lemmatizer = nltk.stem.wordnet.WordNetLemmatizer()

word_detect = ['trainingshow','salariesslow','businessstrong']


def proc_lowCaps(st):
    r = ''
    for s in range(len(st) - 1):
        if st[s].islower() and st[s + 1].isupper():
            r += st[s] + ' '
        else:
            r += st[s]
    r += st[-1:]
    return r


def blob_anilize(blob, stop_words):
    clear_letters = ''.join([l for l in
                             blob.replace('-', ' ').replace('.', ' ').replace(',', ' ').replace('(', ' ').replace(')',
                                                                                                                  ' ').replace(
                                 '\n', ' ').replace('\0', ' ').replace('...', ' ').replace('/', ' ') if l in LETTERS])
    clear_words_list = proc_lowCaps(clear_letters.strip()).split(' ')
    res = []
    for word in clear_words_list:
        word = word.strip().lower()
        if 1 < len(word) < 32 and word not in stop_words:
            lword = Lemmatizer.lemmatize(word, 'v')
            if lword not in stop_words:
                res.append(lword)
    return res


def calculate_freq(company):
    raw_results = GlassdoorPageRawResult.objects.filter(page__company=company)

    words_list = []
    key_outputs = []
    cnt = raw_results.count()
    if not cnt:
        return None
    stop_words = [s.value for s in GlassdorKeywordStopInput.objects.all()]
    GlassdorKeywordOutput.objects.filter(company=company).delete() # delete old entries
    for i, result in enumerate(raw_results):
        txt_blob = ' '.join((result.employee_review_title, result.employee_review_pros, result.employee_review_cons,
                             result.employee_review_advice))
        words_list.extend(blob_anilize(txt_blob, stop_words))
    word_set = set(words_list)
    for word in word_set:
        key_outputs.append(GlassdorKeywordOutput(company=company, value=word, count=words_list.count(word)))
    GlassdorKeywordOutput.objects.bulk_create(key_outputs)
