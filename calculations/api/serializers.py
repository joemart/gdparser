from calculations.calculator import *

__author__ = 'orion'
from rest_framework import serializers
from complex.models import FadeRates, Company, CompanyOutput, CompanyOutputFinancial, ExchangeRates, PatentOutput, \
    PatentTask, PatentCounter
from django.db.models import Sum, Count


class SectorSerializer(serializers.ModelSerializer):
    class Meta:
        model = FadeRates
        fields = ('yahoo_industry',)


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name')


class CompanyPatentsSerializer(serializers.ModelSerializer):
    patents_per_year = serializers.SerializerMethodField()
    patents_ever = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'patents_ever', 'patents_per_year')

    def get_patents_per_year(self, company):
        return get_patents_per_category_company(company.pk, percentage=True)

    def get_patents_ever(self, company):
        patents = PatentTask.objects.filter(company=company, results_count__isnull=False).aggregate(
            Sum('results_count'))
        return patents['results_count__sum']


class CompanyPatentsBySalesSerializer(serializers.ModelSerializer):
    ratio_per_year = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'ratio_per_year')

    def get_ratio_per_year(self, company):
        return get_patents_ratio_company(company)




class CompanyRNDSerializer(serializers.ModelSerializer):
    rnd_per_year = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'rnd_per_year')

    def get_rnd_per_year(self, company):
        return get_company_rnd_sales(company, r_n_d=True)


class CompanyRNDAdjSerializer(serializers.ModelSerializer):
    rnd_adj_per_year = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'rnd_adj_per_year')

    def get_rnd_adj_per_year(self, company):
        return get_company_rnd_adj(company)


class CompanyReturnOnRNDSerializer(serializers.ModelSerializer):
    return_on_rnd = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'return_on_rnd')

    def get_return_on_rnd(self, company):
        return get_return_on_rnd_company(company)


class CompanyReturnOnRNDCapitalizedSerializer(serializers.ModelSerializer):
    rnd_cap = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'rnd_cap')

    def get_return_on_rnd_cap(self, company):
        return get_cap_rnd_company(company)


class CompanyOverviewSerializer(serializers.ModelSerializer):
    return_on_rnd = serializers.SerializerMethodField()
    rnd_cap = serializers.SerializerMethodField()
    rnd_adj = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'return_on_rnd', 'rnd_cap', 'rnd_adj')

    def get_return_on_rnd(self, company):
        return get_return_on_rnd_company(company)

    def get_rnd_cap(self, company):
        return get_cap_rnd_company(company)

    def get_rnd_adj(self, company):
        return get_company_rnd_adj(company)


class CompanyTestSerializer(serializers.ModelSerializer):
    test_field = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('pk', 'companyid', 'company_name', 'test_field')

    def get_test_field(self, company):
        return get_company_rnd_adj(company)



