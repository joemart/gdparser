from rest_framework import filters


class SectorFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """

    def filter_queryset(self, request, queryset, view):
        sector = request.GET.get('sector', None)
        if sector:
            return queryset.filter(yahoo_industry=sector)
        else:
            return queryset