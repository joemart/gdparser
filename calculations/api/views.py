from calculations.api.filters import SectorFilterBackend
from calculations.api.serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from calculations.calculator import *
from complex.models import CompanyOutput, FadeRates, Company, PatentOutput, CompanyOutputFinancial, ExchangeRates, \
    PatentCounter
from rest_framework import generics
from rest_framework import permissions
from rest_framework import filters
from django.db.models import Sum, Avg


class ListSectors(generics.ListAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = FadeRates.objects.all().exclude(yahoo_industry='N/A')
    serializer_class = SectorSerializer


class ListCompanies(generics.ListAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()

    def get_queryset(self):
        sector = self.request.GET.get('sector', None)

        queryset = super(ListCompanies, self).get_queryset()
        if sector:
            return queryset.filter(sector=sector).order_by('company_name')
        queryset = queryset.order_by('company_name')
        return queryset

# patents

class PatentViewCompany(generics.RetrieveAPIView):
    model = Company
    serializer_class = CompanyPatentsSerializer
    queryset = Company.objects.all()


class PatentSectorAverage(APIView):  # +++

    def get(self, request):
        return Response(get_patents_per_category_sector(request.GET.get('sector', None), percentage=True))

class PatentSectorBySales(APIView):  # +++

    def get(self, request):
        return Response(get_patents_ratio_sector(request.GET.get('sector', None)))


class PatentCompanySectorBySales(generics.RetrieveAPIView):
    model = Company
    serializer_class = CompanyPatentsBySalesSerializer
    queryset = Company.objects.all()

# end patents


class RnDViewCompany(generics.RetrieveAPIView):  # +++
    model = Company
    serializer_class = CompanyRNDSerializer
    queryset = Company.objects.all()


class RnDAdjViewCompany(generics.RetrieveAPIView):  # +++
    model = Company
    serializer_class = CompanyRNDAdjSerializer
    queryset = Company.objects.all()


class RnDViewSector(APIView):  # +++

    def get(self, request):
        return Response(get_sector_avg_rnd_sales(request.GET.get('sector', None), r_n_d=True))


class ReturnOnRnDViewCompany(generics.RetrieveAPIView):  # +++
    model = Company
    serializer_class = CompanyReturnOnRNDSerializer
    queryset = Company.objects.all()


class ReturnOnRnDCapitalizedViewCompany(generics.RetrieveAPIView):  # +++
    model = Company
    serializer_class = CompanyReturnOnRNDCapitalizedSerializer
    queryset = Company.objects.all()


class CapRnDViewSector(APIView):  # +++

    def get(self, request):
        return Response(get_cap_rnd_sector(request.GET.get('sector', None)))


class ReturnRnDViewSector(APIView):  # +++

    def get(self, request):
        return Response(get_return_on_rnd_sector(request.GET.get('sector', None)))

# Charts


class SectorsOverviewChart(APIView):
    def get(self, request):
        sectors = FadeRates.objects.all().exclude(yahoo_industry='N/A')
        res = {}
        for sector in sectors:
            return_on_rnd = get_return_on_rnd_sector(sector.yahoo_industry)
            cap_rnd = get_cap_rnd_sector(sector.yahoo_industry)
            rnd_adj = get_sector_avg_rnd_sales(sector.yahoo_industry, r_n_d=True)
            most_recent_year = max(
                [y for y in return_on_rnd.iterkeys() if y in rnd_adj.iterkeys() and y in cap_rnd.iterkeys()])
            res[sector.yahoo_industry] = {'roi': return_on_rnd[most_recent_year]['return_on_rnd'],
                                          'cap_rnd_by_sales': cap_rnd[most_recent_year]['cap_rnd_by_sales_avg'],
                                          'rnd_adj': rnd_adj[most_recent_year]['r_n_d']}

        return Response(res)


class CompaniesOverviewView(generics.ListAPIView):  # +++
    model = Company
    serializer_class = CompanyOverviewSerializer
    queryset = Company.objects.all()

    def get_queryset(self):
        qs = super(CompaniesOverviewView, self).get_queryset()
        qs = qs.filter(sector=self.kwargs['sector_name'])
        return qs



class TestViewSector(APIView):

    def get(self, request):
        return Response(get_return_on_rnd_sector(request.GET.get('sector', None)))


class TestViewCompany(generics.RetrieveAPIView):
    model = Company
    serializer_class = CompanyTestSerializer
    queryset = Company.objects.all()


class TestViewSectorAlt(generics.ListAPIView):  # +++
    model = Company
    serializer_class = CompanyOverviewSerializer
    queryset = Company.objects.all()