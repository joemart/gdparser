from django import forms
from complex.models import Company, CompanyOutputFinancial


class CompaniesListForm(forms.Form):
    companies = forms.FileField()


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company


class CompanyOutputFinancialForm(forms.ModelForm):
    class Meta:
        model = CompanyOutputFinancial