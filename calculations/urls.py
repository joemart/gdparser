from calculations.api.views import *

__author__ = 'orion'
from django.conf.urls import patterns, include, url
from views import ImportView, OverviewView, SectorView, CompanyView, CompaniesListUploadView, FinancialListUploadView
# from django.views.decorators.cache import cache_page

urlpatterns = patterns('',
                       url(r'^$', OverviewView.as_view(), name='overview'),
                       url(r'^sector/(?P<sector_name>[A-Za-z0-9 &+-.]+)/$', SectorView.as_view(), name='sector'),
                       url(r'^company/(?P<pk>[0-9]+)/$', CompanyView.as_view(), name='company'),
                       url(r'^import/$', ImportView.as_view(), name='import'),


                       url(r'^import_companies/$', CompaniesListUploadView.as_view(), name='import_companies'),
                       url(r'^import_financial/$', FinancialListUploadView.as_view(), name='import_financial'),

                       url(r'^api/sectors_list/$', ListSectors.as_view(), name='api_sectors'),
                       url(r'^api/companies_list/$', ListCompanies.as_view(), name='api_companies'),

                       # patents
                       url(r'^api/company/patents/category/(?P<pk>[0-9]+)/$', PatentViewCompany.as_view(),  #+
                           name='api_patents_company_view'),
                       url(r'^api/sector/patents/category/$', PatentSectorAverage.as_view(),  #+
                           name='api_patents_sector_avg_view'),

                       url(r'^api/company/patents/per_sales/(?P<pk>[0-9]+)/$', PatentCompanySectorBySales.as_view(),
                           name='api_patents_company_by_sales_view'),
                       url(r'^api/sector/patents/per_sales/$', PatentSectorBySales.as_view(),
                           name='api_patents_sector_by_sales_view'),
                       # / patents

                       # sales
                       url(r'^api/company/rnd/by_sales/(?P<pk>[0-9]+)/$', RnDViewCompany.as_view(),  #+
                           name='api_rnd_by_sales_company_view'),
                       url(r'^api/sector/rnd/by_sales/$', RnDViewSector.as_view(),
                           name='api_rnd_by_sales_sector_view'),
                       # / sales

                       # rnd
                       url(r'^api/company/rnd/adj/(?P<pk>[0-9]+)/$', RnDAdjViewCompany.as_view(),  #+
                           name='api_rnd_company_adj'),
                       url(r'^api/company/rnd/return/(?P<pk>[0-9]+)/$', ReturnOnRnDViewCompany.as_view(),  #+
                           name='api_rnd_company_return'),
                       url(r'^api/company/rnd/cap/(?P<pk>[0-9]+)/$', ReturnOnRnDCapitalizedViewCompany.as_view(),  #+
                           name='api_rnd_company_cap'),
                       # / rnd

                       # test
                       url(r'^api/company/test/(?P<pk>[0-9]+)/$', TestViewCompany.as_view(),  #+
                           name='api_patents_companweuewruewru'),
                       url(r'^api/sector/test/$', TestViewSector.as_view(),
                           name='api_rnd_by_sales_secwewewe'),
                       url(r'^api/sector/test/alt/$', TestViewSectorAlt.as_view(),
                           name='api_rnd_by_sales_secwewewe'),

                       # /test

                       # charts
                       url(r'^api/chart/sectors/overview/$', SectorsOverviewChart.as_view(),
                           name='api_chart_sectors_overview'),
                       url(r'^api/chart/sector/overview/(?P<sector_name>[A-Za-z0-9 ]+)/$', CompaniesOverviewView.as_view(),
                           name='api_chart_sectors_overview'),
                       # /charts

                       )
