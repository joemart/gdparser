from django.db.models import Sum, Avg, Count
from complex.models import PatentCounter, Company, CompanyOutputFinancial, ExchangeRates, FadeRates, \
    GlassdoorPageRawResult, AmazonBrandRawOutput
from constance import config
from django.core.cache import cache
import datetime
from cache_maker import cache_dec


class MoneyConverter:
    def __init__(self, currency, source, units, year):
        if units not in ['millions', 'b', 'thousands', 'th', 'm', 'Mil', 'k', ',k']:
            raise Exception('unknown units - %s' % units)
        self.currency = currency
        self.source = source
        self.units = units
        self.year = year
        if self.currency == 'USD':
            self.rate = 1.0
        else:
            rate = cache.get("%s_%s" % (year, currency))
            if not rate:
                self.rate = 1.0 if self.currency == 'USD' else float(
                    ExchangeRates.objects.get(year=self.year, currency=self.currency).value)
                cache.set("%s_%s" % (year, currency), self.rate,
                          604800 if year == datetime.datetime.now().year else None)
            else:
                self.rate = rate

    def convert(self, value):
        adapted_value = float(value.replace(',', '').replace(' ', ''))
        if self.units == 'b':
            adapted_value *= 1000
        if self.units in ('thousands', 'th', 'k', ',k'):
            adapted_value /= 1000
        return adapted_value / self.rate


# sales
@cache_dec()
def get_company_rnd_sales(company, r_n_d=False, r_n_d_null=False):
    financial_outputs = CompanyOutputFinancial.objects.filter(company=company,
                                                              sales__isnull=False).exclude(sales='').exclude(sales='-')
    if r_n_d and not r_n_d_null:
        financial_outputs = financial_outputs.filter(research_and_development__isnull=False).exclude(research_and_development='').exclude(research_and_development='-')
    financial_data = {}
    for output in financial_outputs:
        currency = output.morningstar_currency or output.yahoo_currency
        mc = MoneyConverter(currency, '', output.morningstar_currency_units or output.yahoo_currency_units, output.year)
        if r_n_d and output.research_and_development and output.research_and_development != '-' and mc.convert(output.research_and_development) > mc.convert(output.sales):
                    continue  # fix conversation error
        financial_data[output.year] = {'sales': mc.convert(output.sales)}
        if r_n_d:
            if output.research_and_development and output.research_and_development != '-':
                financial_data[output.year]['r_n_d'] = mc.convert(output.research_and_development)
                if financial_data[output.year]['sales']:
                    financial_data[output.year]['rnd_by_sales'] = financial_data[output.year]['r_n_d'] / \
                                                              financial_data[output.year]['sales']
            else:
                financial_data[output.year]['r_n_d'], financial_data[output.year]['rnd_by_sales'] = None, None
    return financial_data

@cache_dec()
def get_sector_avg_rnd_sales(sector_name, r_n_d=False):
    financial_outputs = CompanyOutputFinancial.objects.filter(company__sector=sector_name,
                                                              sales__isnull=False).exclude(sales='').exclude(sales='-')
    if r_n_d:
        financial_outputs = financial_outputs.filter(research_and_development__isnull=False).exclude(research_and_development='').exclude(research_and_development='-')

    financial_data = {}
    for output in financial_outputs:
        currency = output.morningstar_currency or output.yahoo_currency
        mc = MoneyConverter(currency, '', output.morningstar_currency_units or output.yahoo_currency_units, output.year)
        if r_n_d and output.research_and_development and output.research_and_development != '-' and mc.convert(output.research_and_development) > mc.convert(output.sales):
                    continue  # fix conversation error

        if not output.year in financial_data:
            financial_data[output.year] = {'sales': mc.convert(output.sales)}
            if r_n_d:
                financial_data[output.year]['r_n_d'] = mc.convert(output.research_and_development)
            financial_data[output.year]['count'] = 1
        else:
            financial_data[output.year]['count'] += 1
            financial_data[output.year]['sales'] += mc.convert(output.sales)
            if r_n_d:
                financial_data[output.year]['r_n_d'] += mc.convert(output.research_and_development)

    for key in financial_data.iterkeys():
        financial_data[key]['sales_avg'] = financial_data[key]['sales'] / float(financial_data[key]['count'])
        if r_n_d and financial_data[key]['sales_avg'] and financial_data[key]['count']:
            financial_data[key]['r_n_d_avg'] = financial_data[key]['r_n_d'] / float(financial_data[key]['count'])
            financial_data[key]['rnd_by_sales'] = financial_data[key]['r_n_d_avg'] / financial_data[key]['sales_avg']

    return financial_data


# end sales

# patents
@cache_dec()
def get_total_patents_sector(sector_name, as_sum=False):
    companies_with_patents_ids = set()
    for patent in PatentCounter.objects.distinct('company').select_related('company'):
        companies_with_patents_ids.add(patent.company.pk)  # only companies with parsed counters
    companies = Company.objects.filter(sector=sector_name, pk__in=companies_with_patents_ids)
    com_count = companies.count()
    res = {}
    if not as_sum:
        patent_counters = PatentCounter.objects.filter(company__in=companies, category='').values('year').annotate(
            acount=(Sum('count')))
    else:
        patent_counters = PatentCounter.objects.filter(company__in=companies).exclude(category='').values(
            'year').annotate(
            acount=(Sum('count')))
    patent_counters = map(lambda x: {'year': x['year'], 'avg': x['acount'] / float(com_count), 'count': com_count},
                          patent_counters)
    patent_counters = {x['year']: {'avg': x['avg'], 'count': com_count} for x in patent_counters}
    return patent_counters


@cache_dec()
def get_total_patents_company(company, as_sum=False):
    if not as_sum:
        patent_counters = PatentCounter.objects.filter(company=company, category='').values('year').annotate(
            count=(Sum('count')))
    else:
        patent_counters = PatentCounter.objects.filter(company=company).exclude(category='').values('year').annotate(
            count=(Sum('count')))
    patent_counters = map(lambda x: {'year': x['year'], 'count': x['count']},
                          patent_counters)
    patent_counters = {x['year']: {'count': x['count']} for x in patent_counters}
    return patent_counters


def get_patents_ratio_sector(sector_name):
    total_patents = get_total_patents_sector(sector_name)
    sales = get_sector_avg_rnd_sales(sector_name)
    res = {}
    for year in sales.iterkeys():
        if year in total_patents:
            res[year] = total_patents[year]['avg']/(sales[year]['sales']/1000.0) # to billions
    return res


def get_patents_ratio_company(company):
    total_patents = get_total_patents_company(company)
    sales = get_company_rnd_sales(company)
    res = {}
    for year in sales.iterkeys():
        if year in total_patents:
            res[year] = total_patents[year]['count']/(sales[year]['sales']/1000.0) # to billions
    return res

def get_patents_per_category_sector(sector_name, percentage=False):
    companies_with_patents_ids = set()
    for patent in PatentCounter.objects.distinct('company').select_related('company'):
        companies_with_patents_ids.add(patent.company.pk)  # only companies with parsed counters
    companies = Company.objects.filter(sector=sector_name, pk__in=companies_with_patents_ids)

    com_count = companies.count()
    res = {}
    patent_counters = PatentCounter.objects.filter(company__in=companies).exclude(category='').values('year', 'category').annotate(
        acount=(Sum('count')))
    if percentage:
        totals = get_total_patents_sector(sector_name,
                                          True)  # {y: a for y, a in map(lambda x: (x['year'], x['avg']), get_total_patents_sector(sector_name))}
    for patent_counter in patent_counters:
        if not patent_counter['year'] in res:
            res[patent_counter['year']] = {}
        res[patent_counter['year']][patent_counter['category']] = patent_counter['acount'] / float(com_count)
        if percentage and patent_counter['year'] in totals and totals[patent_counter['year']]['avg']:
            res[patent_counter['year']][patent_counter['category']] /= float(totals[patent_counter['year']]['avg'])
    return res


def get_patents_per_category_company(company_id, percentage=False):
    company = Company.objects.get(pk=company_id)
    res = {}
    patent_counters = PatentCounter.objects.filter(company=company).exclude(category='').values('year',
                                                                                                'category').annotate(
        acount=(Sum('count')))
    if percentage:
        totals = get_total_patents_company(company_id, True)
    for patent_counter in patent_counters:
        if not patent_counter['year'] in res:
            res[patent_counter['year']] = {}
        res[patent_counter['year']][patent_counter['category']] = patent_counter['acount']
        if percentage and patent_counter['year'] in totals and totals[patent_counter['year']]['count']:
            res[patent_counter['year']][patent_counter['category']] /= float(totals[patent_counter['year']]['count'])
    return res

    # end patents


# rnd
@cache_dec()
def get_company_rnd_adj(company):
    rnd_sales_company_reported = get_company_rnd_sales(company, r_n_d=True, r_n_d_null=True)
    rnd_sales_sector_avg = get_sector_avg_rnd_sales(company.sector, r_n_d=True)
    if not len(rnd_sales_company_reported):
        return {'max_year': 0, 'min_year': 0, 'res': {}}
    max_year = max(rnd_sales_company_reported.iterkeys())
    min_year = min(rnd_sales_company_reported.iterkeys())
    res = {}
    for year in xrange(max_year, min_year - 1, -1):
        if year in rnd_sales_company_reported and rnd_sales_company_reported[year]['sales']:
            if rnd_sales_company_reported[year][
                'r_n_d'] > 0:  # If it is not a positive number, it takes the sector average level of R&D/Sales (from the previous tab) and multiplies it by the sales the company reports in that year.
                res[year] = {'r_n_d': rnd_sales_company_reported[year]['r_n_d'],
                             'r_n_d_by_sales': rnd_sales_company_reported[year]['r_n_d'] /
                                               rnd_sales_company_reported[year]['sales']}
            else:
                if year in rnd_sales_sector_avg and rnd_sales_company_reported[year]['sales']:
                    res[year] = {
                        'r_n_d': rnd_sales_company_reported[year]['sales'] * rnd_sales_sector_avg[year]['rnd_by_sales']}
                    res[year]['r_n_d_by_sales'] = res[year]['r_n_d'] / rnd_sales_company_reported[year]['sales']
                elif year + 1 in res:
                    res[year] = {'r_n_d': res[year + 1]['r_n_d'] * 0.9, 'r_n_d_by_sales': res[year + 1][
                        'r_n_d_by_sales']}  # it takes the following years R&D spending multiplied by 0.9
        else:
            if year + 1 in res:
                res[year] = {'r_n_d': res[year + 1]['r_n_d'] * 0.9, 'r_n_d_by_sales': res[year + 1][
                        'r_n_d_by_sales']}
    if not len(res):
        return {'max_year': 0, 'min_year': 0, 'res': res}
    return {'max_year': max(res.iterkeys()), 'min_year': min_year, 'res': res}

@cache_dec()
def get_sector_rnd_adj(sector_name):
    return get_sector_avg_rnd_sales(sector_name, r_n_d=True)

@cache_dec()
def get_op_income_company(company):
    """get op income and op margin"""
    rnd = get_company_rnd_adj(company)['res']
    financial_outputs = CompanyOutputFinancial.objects.filter(company=company, operating_income__isnull=False, sales__isnull=False).exclude(
        operating_income='').exclude(
        operating_income='-').exclude(
        sales='-').exclude(
        sales='x').exclude(
        sales='')
    res = {}
    for output in financial_outputs:
        currency = output.morningstar_currency or output.yahoo_currency
        mc = MoneyConverter(currency, '', output.morningstar_currency_units or output.yahoo_currency_units, output.year)
        if output.year in rnd and mc.convert(output.sales):
            res[output.year] = {'op_income': mc.convert(output.operating_income) + rnd[output.year]["r_n_d"]}
            if 0 < res[output.year]['op_income'] / mc.convert(output.sales) < 1:
                res[output.year]['op_margin'] = res[output.year]['op_income'] / mc.convert(output.sales)
    return res

@cache_dec()
def get_op_income_sector(sector_name):
    """get op income and op margin"""
    rnd = get_sector_rnd_adj(sector_name)
    financial_outputs = CompanyOutputFinancial.objects.filter(company__sector=sector_name,
                                                              operating_income__isnull=False,
                                                              sales__isnull=False).exclude(
        operating_income='').exclude(sales='').exclude(
        operating_income='-').exclude(sales='-')
    res = {}
    for output in financial_outputs:
        currency = output.morningstar_currency or output.yahoo_currency
        mc = MoneyConverter(currency, '', output.morningstar_currency_units or output.yahoo_currency_units, output.year)
        if not output.year in res:
            res[output.year] = {'op_income': 0, 'sales_count': 0, 'op_income_count': 0, 'sales': 0}
        if output.year in rnd and "r_n_d_avg" in rnd[output.year]:
            if mc.convert(output.sales) and 0 < mc.convert(output.operating_income) / mc.convert(output.sales) < 1:  # filter extreme values
                res[output.year]['op_income'] += (mc.convert(output.operating_income) + rnd[output.year]["r_n_d_avg"])
                res[output.year]['op_income_count'] += 1
        res[output.year]['sales'] += mc.convert(output.sales)
        res[output.year]['sales_count'] += 1
    for year in res.iterkeys():
        if res[year]['sales_count'] != 0:
            res[year]['sales_avg'] = res[year]['sales'] / float(res[year]['sales_count'])
        if res[year]['op_income_count'] != 0:
            res[year]['op_income_avg'] = res[year]['op_income'] / float(res[year]['op_income_count'])

        if 'sales_avg' in res[year] and res[year]['sales_avg'] != 0 and 'op_income_avg' in res[year]:
            res[year]['op_margin'] = float(res[year]['op_income_avg']) / res[year]['sales_avg']
    return res


def forecast(x, known_y, known_x):
    """Excel FORECAST function"""
    if not len(known_x) or not len(known_y) or not len(known_x)==len(known_y):
        return None
    if len(known_y) == 1:
        return known_y[0]
    avgx = sum(known_x) / float(len(known_x))
    avgy = sum(known_y) / float(len(known_y))
    b = sum([(avgx - xi) * (avgy - yi) for xi, yi in zip(known_x, known_y)]) / sum([(avgx-xi) ** 2 for xi in known_x])
    a = avgy-b*avgx
    return a + x * b

@cache_dec()
def get_op_margin_trend_sector(sector_name):
    """get op margin and SctAdj"""
    op_margin = get_op_income_sector(sector_name)
    res = {}
    for year in op_margin:
        if not year in res:
            res[year]={}
        res[year]['op_margin_trend'] = forecast(year, [op_margin[y]['op_margin'] for y in op_margin if 'op_margin' in op_margin[y]],
                             [y for y in op_margin if 'op_margin' in op_margin[y]])
        if 'op_margin_trend' in res[year] and 'op_margin' in op_margin[year]:
            res[year]['op_margin_adj'] = op_margin[year]['op_margin']-res[year]['op_margin_trend']
    return res

@cache_dec()
def get_op_margin_adj_company(company):
    op_margin = get_op_income_company(company)
    op_margin_adj = get_op_margin_trend_sector(company.sector)
    res = {}
    for year in op_margin:
        if not year in res:
            res[year] = {}
        if year in op_margin_adj and 'op_margin' in op_margin[year] and 'op_margin_adj' in op_margin_adj[year]:
            res[year]['sct_adj'] = op_margin[year]['op_margin'] - op_margin_adj[year]['op_margin_adj']
    for year in res:
        res[year]['op_margin_trend'] = forecast(year,[res[y]['sct_adj'] for y in res if 'sct_adj' in res[y]],[y for y in res  if 'sct_adj' in res[y]])
    return res

@cache_dec()
def get_uplift_company(company, fade_rate = None):
    op_margin_trend = get_op_margin_adj_company(company)

    financial_outputs = CompanyOutputFinancial.objects.filter(company=company, total_assets__isnull=False,
                                                              sales__isnull=False).exclude(sales='').exclude(
        total_assets='').exclude(sales='-').exclude(
        total_assets='-').order_by('year')
    if not fade_rate:
        fade_rate = FadeRates.objects.get(yahoo_industry=company.sector)
    rnd_life = fade_rate.randd_life
    fade_period = fade_rate.earnings_fade_period
    res = {}
    for output in financial_outputs:
        old_output = None
        for o in financial_outputs:
            if o.year == output.year - rnd_life and o.total_assets and o.total_assets != '':
                old_output = o
        if not output.year in op_margin_trend or not output.year - rnd_life in op_margin_trend or not old_output:
            continue
        currency = output.morningstar_currency or output.yahoo_currency
        mc = MoneyConverter(currency, '', output.morningstar_currency_units or output.yahoo_currency_units, output.year)
        if 'op_margin_trend' in op_margin_trend[output.year] and op_margin_trend[output.year]['op_margin_trend'] and op_margin_trend[output.year - rnd_life]['op_margin_trend'] and mc.convert(old_output.total_assets) and mc.convert(old_output.sales):
            res[output.year] = (mc.convert(output.sales) * op_margin_trend[output.year]['op_margin_trend']) - (
            mc.convert(output.total_assets) / mc.convert(old_output.total_assets)) * (
        1 - rnd_life / float(fade_period)) * (mc.convert(old_output.sales) * op_margin_trend[output.year - rnd_life]['op_margin_trend'])
    return res

@cache_dec()
def get_cap_rnd_company(company, fade_rate=None):
    """returns capitilized rnd and capitalized rnd divided by sales"""
    sales = get_company_rnd_sales(company)
    rnd = get_company_rnd_adj(company)
    if not fade_rate:
        fade_rate = FadeRates.objects.get(yahoo_industry=company.sector)
    max_year = rnd['max_year']
    min_year = rnd['min_year']
    if max_year != min_year:
        for y in xrange(min_year, min_year - fade_rate.randd_life, -1):
            rnd['res'][y] = {}
            rnd['res'][y]['r_n_d'] = rnd['res'][y + 1]['r_n_d'] * 0.95
    res = {}
    for year in xrange(max_year, min_year, -1):
        if year == 0:
            continue
        if not year in res:
            res[year] = {'cap_rnd':0}
        for i, y in enumerate(xrange(year, year - fade_rate.randd_life, -1)):
            res[year]['cap_rnd'] += rnd['res'][y]['r_n_d'] * (1 - i / float(fade_rate.randd_life))
        if year in sales and sales[year]['sales']:
            res[year]['cap_rnd_by_sales'] = res[year]['cap_rnd']/sales[year]['sales']
            if res[year]['cap_rnd_by_sales'] > config.EXTREME_CAP_RND/100:
                res[year]['cap_rnd_by_sales'] = config.EXTREME_CAP_RND/100
    return res

@cache_dec()
def get_inclusion_flag(company,fade_rate=None):
    financial_outputs = CompanyOutputFinancial.objects.filter(company=company, total_assets__isnull=False).exclude(
        total_assets='').exclude(
        total_assets='-')
    cap_rnd = get_cap_rnd_company(company, fade_rate=fade_rate)
    res = {}
    for output in financial_outputs:
        currency = output.morningstar_currency or output.yahoo_currency
        mc = MoneyConverter(currency, '', output.morningstar_currency_units or output.yahoo_currency_units, output.year)
        if output.year in cap_rnd and mc.convert(output.total_assets) > cap_rnd[output.year]['cap_rnd']:
            res[output.year] = cap_rnd[output.year]['cap_rnd'] / mc.convert(output.total_assets)
        # if not output.year in cap_rnd or mc.convert(output.total_assets) == 0:
        #     res[output.year] = False
        # else:
        #
        #     res[output.year] = cap_rnd[output.year]['cap_rnd'] / mc.convert(output.total_assets) > config.INCLUSION_FLAG/100.0 and \
        #                    cap_rnd[output.year]['cap_rnd'] < mc.convert(output.total_assets)

    return (sum(res.itervalues())/len(res)>config.INCLUSION_FLAG/100.0) if len(res) else False


@cache_dec()
def get_return_on_rnd_company(company, fade_rate=None):
    if not fade_rate:
        fade_rate = FadeRates.objects.get(yahoo_industry=company.sector)
    op_uplift = get_uplift_company(company, fade_rate=fade_rate)
    cap_rnd = get_cap_rnd_company(company, fade_rate=fade_rate)
    flag = get_inclusion_flag(company)
    res = {}
    for year in op_uplift:
        if year - fade_rate.randd_lag in cap_rnd and flag:
            if op_uplift[year] > 0 and cap_rnd[year - fade_rate.randd_lag]['cap_rnd'] > 0:
                #and the current year need to be positive to calculate a value for that current year.  If it is not, the value should be left empt
                pre_res = op_uplift[year] / cap_rnd[year - fade_rate.randd_lag]['cap_rnd']
                if pre_res <= config.EXTREME_RND/100:
                    res[year] = pre_res
                else:
                    res[year] = config.EXTREME_RND/100
    return res


@cache_dec()
def get_cap_rnd_sector(sector_name, fade_rate=None):
    companies = Company.objects.filter(sector=sector_name)
    if not fade_rate:
        fade_rate = FadeRates.objects.get(yahoo_industry=sector_name)
    res = {}
    for company in companies:
        if not CompanyOutputFinancial.objects.filter(company=company).exists():
            continue
        cap_rnd = get_cap_rnd_company(company, fade_rate=fade_rate)
        inc_flag = get_inclusion_flag(company, fade_rate=fade_rate)
        for year in cap_rnd:
            if inc_flag:
                if not year in res:
                    res[year] = {"cap_rnd": 0.0, "cap_rnd_by_sales": 0.0, 'count': 0}
                res[year]["cap_rnd"] += cap_rnd[year]["cap_rnd"]
                if "cap_rnd_by_sales" in cap_rnd[year]:
                    res[year]["cap_rnd_by_sales"] += cap_rnd[year]["cap_rnd_by_sales"]
                res[year]["count"] += 1
    for year in res:
        res[year]["cap_rnd_avg"] = res[year]["cap_rnd"] / float(res[year]["count"])
        res[year]["cap_rnd_by_sales_avg"] = res[year]["cap_rnd_by_sales"] / float(res[year]["count"])
    return res


@cache_dec()
def get_op_uplift_sector(sector_name,fade_rate=None):
    if not fade_rate:
        fade_rate = FadeRates.objects.get(yahoo_industry=sector_name)
    companies = Company.objects.filter(sector=sector_name)
    res = {}
    for company in companies:
        if not CompanyOutputFinancial.objects.filter(company=company).exists():
            continue
        inc_flag = get_inclusion_flag(company, fade_rate=fade_rate)
        op_uplift_c = get_uplift_company(company, fade_rate=fade_rate)
        for year in op_uplift_c:
            if year in inc_flag:
                if not year in res:
                    res[year] = {"op_uplift": 0.0, 'count': 0}
                res[year]["op_uplift"] += op_uplift_c[year]
                res[year]["count"] += 1
    for year in res:
        res[year]["op_uplift_avg"] = res[year]["op_uplift"] / float(res[year]["count"])
    return res


def get_return_on_rnd_sector(sector_name):
    compaies_in_sector = [c for c in Company.objects.filter(sector=sector_name) if
                          CompanyOutputFinancial.objects.filter(
                              company=c).exists()]
    rnd_list = []
    for company in compaies_in_sector:
        rnd_company = get_return_on_rnd_company(company)
        rnd_list.append(rnd_company)
    res = {}
    for rnd_company in rnd_list:
        for y in rnd_company.iterkeys():
            if not y in res:
                res[y] = {'return_on_rnd': rnd_company[y], 'count': 1}
            else:
                res[y]['return_on_rnd'] += rnd_company[y]
                res[y]['count'] += 1
    for y in res.iterkeys():
        res[y]['return_on_rnd'] /= res[y]['count']
    return res
    # fade_rate = FadeRates.objects.get(yahoo_industry=sector_name)
    # op_uplift = get_op_uplift_sector(sector_name, fade_rate=fade_rate)
    # cap_rnd = get_cap_rnd_sector(sector_name, fade_rate=fade_rate)
    # rnd_lag = fade_rate.randd_lag
    # res = {}
    # for year in op_uplift:
    #     if not year in res and year-rnd_lag in cap_rnd:
    #         res[year] = {}
    #         res[year]['return_on_rnd']=op_uplift[year]['op_uplift_avg']/cap_rnd[year-rnd_lag]['cap_rnd_avg']
    # return res


@cache_dec()
def get_rnd_to_return_company_absolute(company):
    ret_rnd = get_return_on_rnd_company(company)
    rnd_adj = get_company_rnd_adj(company)['res']
    years = [y for y in ret_rnd.iterkeys() if y in rnd_adj.iterkeys()]
    if not len(years):
        return None
    most_recent_year = max(years)
    return rnd_adj[most_recent_year]['r_n_d_by_sales'] * ret_rnd[most_recent_year] * 0.9


@cache_dec()
def get_rnd_to_return_sector_absolute(sector_name):
    ret_rnd = get_return_on_rnd_sector(sector_name)
    rnd_adj = get_sector_rnd_adj(sector_name)
    years = [y for y in ret_rnd.iterkeys() if y in rnd_adj.iterkeys()]
    if not len(years):
        return None
    most_recent_year = max(years)
    return rnd_adj[most_recent_year]['r_n_d_avg'] * ret_rnd[most_recent_year]['return_on_rnd'] * 0.9


def get_rnd_to_return_company_relative(company):
    sector = company.sector
    compaies_in_sector = [c for c in Company.objects.filter(sector=sector) if
                          CompanyOutputFinancial.objects.filter(
                              company=c).exists()]

    max_value = max([get_rnd_to_return_company_absolute(c) for c in compaies_in_sector])
    rnd_to_ret = get_rnd_to_return_company_absolute(company)
    if max_value and rnd_to_ret:
        return rnd_to_ret / max_value
    return 0


def get_rnd_to_return_sector_relative(sector_name):
    sectors = _get_sectors_list()
    max_value = max([get_rnd_to_return_sector_absolute(s) for s in sectors])
    rnd_to_return_sector_absolute = get_rnd_to_return_sector_absolute(sector_name)
    if max_value and rnd_to_return_sector_absolute:
        return rnd_to_return_sector_absolute / max_value
    return 0


def get_glassdor_rating_company(company):
    reviews = GlassdoorPageRawResult.objects.filter(page__company=company).order_by('employee_review_date')
    if not reviews.count():
        return []
    first_year = reviews.first().employee_review_date.year
    last_year = reviews.last().employee_review_date.year
    res = {}
    for year in range(first_year, last_year + 1):
        rev_rating = reviews.filter(employee_review_date__year=year).aggregate(avg=Avg('employee_review_rating'),
                                                                               count=Count(
                                                                                   'employee_review_rating'))  # .filter(employee_review_date__year=year)
        # if rev_rating['count']>10:
        if rev_rating['count']>10:
            res[year] = rev_rating['avg']
    return res


def get_glassdor_rating_sector(sector_name):
    reviews = GlassdoorPageRawResult.objects.filter(page__company__sector=sector_name).exclude(employee_review_date__isnull=True).order_by('employee_review_date')
    if not reviews.count():
        return []
    first_year = reviews.first().employee_review_date.year
    last_year = reviews.last().employee_review_date.year
    res = {}
    for year in range(first_year, last_year + 1):
        rev_rating = reviews.filter(employee_review_date__year=year).aggregate(avg=Avg('employee_review_rating'),
                                                                               count=Count(
                                                                                   'employee_review_rating'))
        if rev_rating['count']>10:
            res[year] = rev_rating['avg']
    return res


def get_amazon_rating_company(company):
    reviews = AmazonBrandRawOutput.objects.filter(task__company=company).order_by('amazon_review_date')
    if not reviews.count():
        return None
    first_year = reviews.first().amazon_review_date.year
    last_year = reviews.last().amazon_review_date.year
    res = {}
    for year in range(first_year, last_year + 1):
        rev_rating = reviews.filter(amazon_review_date__year=year).aggregate(avg=Avg('amazon_review_stars'),
                                                                             count=Count(
                                                                                 'amazon_review_stars'))
        if rev_rating['count']>10:
            res[year] = rev_rating['avg']
    return res


def get_amazon_rating_sector(sector_name):
    reviews = AmazonBrandRawOutput.objects.filter(task__company__sector=sector_name).order_by('amazon_review_date')
    if not reviews.count():
        return None
    first_year = reviews.first().amazon_review_date.year
    last_year = reviews.last().amazon_review_date.year
    res = {}
    for year in range(first_year, last_year + 1):
        rev_rating = reviews.filter(amazon_review_date__year=year).aggregate(avg=Avg('amazon_review_stars'),
                                                                             count=Count(
                                                                                 'amazon_review_stars'))
        if rev_rating['count']>10:
            res[year] = rev_rating['avg']

    return res


def _get_sectors_list():
    sectors = FadeRates.objects.all().exclude(yahoo_industry='N/A')
    res = []
    for sector in sectors:
        if not _check_sector_is_empty(sector.yahoo_industry):
            res.append(sector.yahoo_industry)
    return res


def _check_sector_is_empty(sectorname):
    if CompanyOutputFinancial.objects.filter(company__sector=sectorname).exists():
            return False
    return True