import csv
from calculations.forms import CompanyForm, CompanyOutputFinancialForm
from complex.models import Company, FadeRates, CompanyOutputFinancial


def companies_list_csv_process(csvfile):
    reader = csv.reader(csvfile)
    processed_count = 0
    errors = []
    for line, row in enumerate(reader):
        if len(row) != 5:
            if len(row) > 1:
                errors.append('line %s %s(%s) - format error' % (line + 1, row[0], [row[1]]))
            else:
                errors.append('line %s - format error' % (line + 1))

        c_form = CompanyForm(
            {'companyid': row[0],
             'company_name': row[1],
             'morningstar_page': row[2],
             'morningstar_page_tools': row[3],
             'yahoo_page': row[4],
             'sector': row[5]}, instance=Company.objects.filter(companyid=row[0]).first())
        if c_form.is_valid():
            processed_count += 1
            c_form.save()
            FadeRates.objects.get_or_create(yahoo_industry=row[4])
        else:
            err_str = ','.join(['%s: %s' % (k, v) for k, v in c_form.errors.iteritems()])
            errors.append("%s(%s) - Import error - %s" % (row[1], row[0], err_str))
    return processed_count, errors


def financial_list_csv_process(csvfile):
    reader = csv.reader(csvfile)
    processed_count = 0
    errors = []
    output_list_for_create = []
    for line, row in enumerate(reader):
        if len(row) <= 1:
            continue
        if len(row) != 16:
            if len(row) > 2:
                errors.append('line %s (%s) - format error - inconsistent columns count - must be 16' % (line + 1, row[0]))
                continue
            else:
                errors.append('line %s - format error - inconsistent columns count - must be 16' % (line + 1))
                continue
        try:
            company = Company.objects.get(companyid=row[0])
        except Exception as e:
            errors.append('%s - %s' % (row[0], e.message))
            continue
        if not row[2]:
            errors.append('%s - year is required field' % row[0])
        output = CompanyOutputFinancial.objects.filter(company=company, year=row[2]).first()
        c_form = CompanyOutputFinancialForm(
            {'company': company.pk,
             'rate': row[1],
             'com_id': row[0],
             'year': row[2],
             'morningstar_currency': row[3],
             'morningstar_currency_units': row[4],
             'yahoo_currency': row[5],
             'yahoo_currency_units': row[6],
             'sales': row[7],
             'research_and_development': row[8],
             'operating_income': row[9],
             'net_income': row[10],
             'depreciation_and_amortisation': row[11],
             'net_ppe': row[12],
             'intangible_assets': row[13],
             'total_assets': row[14],
             'total_stockholder_equity': row[15]
             }, instance=output
        )
        if c_form.is_valid():
            processed_count += 1
            if output:
                c_form.save()
            else:
                output_list_for_create.append(c_form.save(commit=False))
        else:
            err_str = ','.join(['%s: %s' % (k, v) for k, v in c_form.errors.iteritems()])
            errors.append("%s(%s) - Import error - %s" % (row[1], row[0], err_str))
    CompanyOutputFinancial.objects.bulk_create(output_list_for_create)
    return processed_count, errors
